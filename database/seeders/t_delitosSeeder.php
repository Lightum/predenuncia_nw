<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class t_delitosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 1 ,'tipo_delitos' =>'Secuestro','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 2 ,'tipo_delitos' =>'Robo a casa habitacion','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 3 ,'tipo_delitos' =>'Robo en flagrancia', 'activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 4 ,'tipo_delitos' =>'Robo con violencia','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 5 ,'tipo_delitos' =>'Robo en transporte publico','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 6 ,'tipo_delitos' =>'Delitos sexuales', 'activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 7 ,'tipo_delitos' =>'Lesiones','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 8 ,'tipo_delitos' =>'Robo de vehiculos','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 9 ,'tipo_delitos' =>'Persona desaparecida','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 10 ,'tipo_delitos' =>'Extorsiones','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 11 ,'tipo_delitos' =>'Homicidio','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 12 ,'tipo_delitos' =>'Trata de personas','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 13 ,'tipo_delitos' =>'Tortura','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 14 ,'tipo_delitos' =>'Otras denuncias','activo' => 1]);
        DB::table('tipo_delitos')->insert(['id_tdelitos' => 15 ,'tipo_delitos' =>'Violencia Familiar','activo' => 1]);
    }
}
