<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Facades\DB;

class aseguradorasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('aseguradoras')->insert(['id_aseguradora' => 1 ,'aseguradora' =>'Aseguradora 1','activo' => 1]);
        // DB::table('aseguradoras')->insert(['id_aseguradora' => 2 ,'aseguradora' =>'Aseguradora 2','activo' => 1]);

        DB::table('aseguradoras')->insert(['id_aseguradora' =>1,'aseguradora' =>'PROLIBER','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>2,'aseguradora' =>'SEGUCEN','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>3,'aseguradora' =>'AGROASEMEX, S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>4,'aseguradora' =>'BANCOMER','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>5,'aseguradora' =>'LA PENINSULAR CIA. GRAL. SEG.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>6,'aseguradora' =>'METROPOLITANA CIA. DE SEG. S.A','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>7,'aseguradora' =>'SEGUROS EL POTOSI S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>8,'aseguradora' =>'ZURICH S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>9,'aseguradora' =>'CHUBB DE MEXICO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>10,'aseguradora' =>'GBM ATLANTICO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>11,'aseguradora' =>'INTERACCIONES S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>12,'aseguradora' =>'LA LATINOAMERICANA SEG. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>13,'aseguradora' =>'PRINCIPAL MEXICO, S.A. DE C.V.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>14,'aseguradora' =>'PROBURSA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>15,'aseguradora' =>'QUALITAS S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>16,'aseguradora' =>'SEGUROS ATLAS S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>17,'aseguradora' =>'SEGUROS DEL NOROESTE S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>18,'aseguradora' =>'SEGUROS SERFIN S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>19,'aseguradora' =>'ZURICH VIDA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>20,'aseguradora' =>'AIG MEXICO SEG. INTER. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>21,'aseguradora' =>'AZTECA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>22,'aseguradora' =>'BAJIO EL','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>23,'aseguradora' =>'C.B.I. SEG., S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>24,'aseguradora' =>'CIGNA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>25,'aseguradora' =>'GEO NEW YORK LIFE S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>26,'aseguradora' =>'ING SEG. S.A. DE C.V.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>27,'aseguradora' =>'INVERLINCOLN S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>28,'aseguradora' =>'METROPOLITANA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>29,'aseguradora' =>'MEXICANA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>30,'aseguradora' =>'MEXICANA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>31,'aseguradora' =>'SEG. BANAMEX AEGON S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>32,'aseguradora' =>'SEG. COMERCIAL AMERICA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>33,'aseguradora' =>'SEG. INBURSA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>34,'aseguradora' =>'SEG. LA TERRITORIAL S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>35,'aseguradora' =>'SEGUROS DEL CENTRO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>36,'aseguradora' =>'ANA CIA. DE SEG. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>37,'aseguradora' =>'BANORTE GENERAL','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>38,'aseguradora' =>'CIA. MEX. SEG. DE CREDITO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>39,'aseguradora' =>'CICA SEGUROS DE MEXICO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>40,'aseguradora' =>'MAYA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>41,'aseguradora' =>'PROT. PESQUERA SOC. MUT. SEG.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>42,'aseguradora' =>'SEGURO BITAL S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>43,'aseguradora' =>'SEGUROS CIGNA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>44,'aseguradora' =>'SEGUROS TEPEYAC S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>45,'aseguradora' =>'ABA SEGUROS, S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>46,'aseguradora' =>'ALLIANZ MEX., S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>47,'aseguradora' =>'CENTRO DEL','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>48,'aseguradora' =>'COLONIAL PENN DE MEX. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>49,'aseguradora' =>'GERLING DE MEX. SEG. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>50,'aseguradora' =>'INTERAMERICANA','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>51,'aseguradora' =>'INVERLAT','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>52,'aseguradora' =>'INVERMEXICO','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>53,'aseguradora' =>'RENAMEX','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>54,'aseguradora' =>'RESASEGURADOR PATRIA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>55,'aseguradora' =>'SEGUROS BBV PROBURSA S.A. C.V.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>56,'aseguradora' =>'SEGUROS M DE MEXICO S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>57,'aseguradora' =>'SEGUROS MONTERREY AETNA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>58,'aseguradora' =>'SKANDIA VIDA S.A. DE C.V.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>59,'aseguradora' =>'TOKIO MARINE S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>60,'aseguradora' =>'ASEMEX','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>61,'aseguradora' =>'INTERAMERICANA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>62,'aseguradora' =>'LIBERTY MEXICO SEG. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>63,'aseguradora' =>'PIONEER SEG. ESP. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>64,'aseguradora' =>'PREVISION OBRERA SOC. MUT. SEG','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>65,'aseguradora' =>'PROT. AGROPECUARIA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>66,'aseguradora' =>'REASEGURADORA ALIANZA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>67,'aseguradora' =>'GRUPO NACIONAL PROVINCIAL S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>68,'aseguradora' =>'LA COMERCIAL','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>69,'aseguradora' =>'LLOYD SEGUROS','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>70,'aseguradora' =>'MUTUALIDAD','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>71,'aseguradora' =>'OBRERA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>72,'aseguradora' =>'POTOSI','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>73,'aseguradora' =>'SEG. BANPAIS S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>74,'aseguradora' =>'SEGUROS GENESIS S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>75,'aseguradora' =>'SEGUROS MARGEN S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>76,'aseguradora' =>'SEGUROS RENAMEX S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>77,'aseguradora' =>'SIN INFORMACION','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>78,'aseguradora' =>'TORREON SOC. MUT. DE SEG.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>79,'aseguradora' =>'ANGLO MEX. DE SEG. S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>80,'aseguradora' =>'EL AGUILA S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>81,'aseguradora' =>'GENERAL DE SEGUROS S.A.','activo' => 1]);
        DB::table('aseguradoras')->insert(['id_aseguradora' =>82,'aseguradora' =>'HIDALGO S.A.','activo' => 1]);

        
    }
}
