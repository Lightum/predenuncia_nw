<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class medios_extorsion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medio_extorsiones')->insert(['id_mextorsion'=>1,'medio_extorsion'=>'PRESENCIAL','activo'=>1]);
        DB::table('medio_extorsiones')->insert(['id_mextorsion'=>2,'medio_extorsion'=>'TELEFÓNICA','activo'=>1]);
        DB::table('medio_extorsiones')->insert(['id_mextorsion'=>3,'medio_extorsion'=>'CORREO','activo'=>1]);
        DB::table('medio_extorsiones')->insert(['id_mextorsion'=>4,'medio_extorsion'=>'REDES SOCIALES','activo'=>1]);
    }
}
