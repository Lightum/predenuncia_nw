<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class t_intervinientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('intervinientes')->insert(['id_interviniente' => 1 ,'interviniente' =>'Victima','activo' => 1]);
        DB::table('intervinientes')->insert(['id_interviniente' => 2 ,'interviniente' =>'Ofendido','activo' => 1]);
        DB::table('intervinientes')->insert(['id_interviniente' => 3 ,'interviniente' =>'Probable','activo' => 1]);
        DB::table('intervinientes')->insert(['id_interviniente' => 4 ,'interviniente' =>'Representante','activo' => 1]);
    }
}
