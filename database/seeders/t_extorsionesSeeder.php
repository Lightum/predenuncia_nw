<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class t_extorsionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_extorsiones')->insert(['id_textorsion' => 1 ,'tipo_extorsion' =>'Presencial','activo' => 1]);
        DB::table('t_extorsiones')->insert(['id_textorsion' => 2 ,'tipo_extorsion' =>'Por teléfono','activo' => 1]);
        DB::table('t_extorsiones')->insert(['id_textorsion' => 3 ,'tipo_extorsion' =>'Por correo','activo' => 1]);
        DB::table('t_extorsiones')->insert(['id_textorsion' => 4 ,'tipo_extorsion' =>'Por redes sociales','activo' => 1]);
    }
}
