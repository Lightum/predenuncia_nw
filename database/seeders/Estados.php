<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class Estados extends Seeder
{
    public function run()
    {
        DB::table('estados')->insert(['id_estados' =>1,'estado' => 'Aguascalientes','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>2,'estado' => 'Baja California','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>3,'estado' => 'Baja California Sur','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>4,'estado' => 'Campeche','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>7,'estado' => 'Chiapas','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>8,'estado' => 'Chihuahua','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>9,'estado' => 'Ciudad de México','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>5,'estado' => 'Coahuila de Zaragoza','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>6,'estado' => 'Colima','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>10,'estado' => 'Durango','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>11,'estado' => 'Guanajuato','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>12,'estado' => 'Guerrero','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>13,'estado' => 'Hidalgo','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>14,'estado' => 'Jalisco','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>16,'estado' => 'Michoacán de Ocampo','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>17,'estado' => 'Morelos','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>15,'estado' => 'México','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>18,'estado' => 'Nayarit','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>19,'estado' => 'Nuevo León','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>20,'estado' => 'Oaxaca','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>21,'estado' => 'Puebla','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>22,'estado' => 'Querétaro','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>23,'estado' => 'Quintana Roo','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>24,'estado' => 'San Luis Potosí','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>25,'estado' => 'Sinaloa','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>26,'estado' => 'Sonora','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>27,'estado' => 'Tabasco','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>28,'estado' => 'Tamaulipas','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>29,'estado' => 'Tlaxcala','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>30,'estado' => 'Veracruz de Ignacio de la Llave','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>31,'estado' => 'Yucatán','activo'=>1]);
        DB::table('estados')->insert(['id_estados' =>32,'estado' => 'Zacatecas','activo'=>1]);


    }
}
