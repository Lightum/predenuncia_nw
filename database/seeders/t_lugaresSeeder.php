<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class t_lugaresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_lugares')->insert(['id_tlugares' => 1 ,'tipo_lugares' =>'Casa habitación','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 3 ,'tipo_lugares' =>'Centro Comercial','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 5 ,'tipo_lugares' =>'Centro Laboral','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 6 ,'tipo_lugares' =>'Centro Recreativo','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 7 ,'tipo_lugares' =>'Estacionamiento','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 8 ,'tipo_lugares' =>'Predio','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 4 ,'tipo_lugares' =>'Vialidad Federal','activo' => 1]);
        DB::table('t_lugares')->insert(['id_tlugares' => 2 ,'tipo_lugares' =>'Via publica','activo' => 1]);
    }
    //
}
