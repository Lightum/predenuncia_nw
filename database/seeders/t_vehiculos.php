<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class t_vehiculos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>1,'tipo_vehiculos'=>'AUTOMÓVIL/CAMIONETA','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>2,'tipo_vehiculos'=>'GO-KART/CARRERAS','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>3,'tipo_vehiculos'=>'REMOLQUE','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>4,'tipo_vehiculos'=>'VEHÍCULO ESPECIAL','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>5,'tipo_vehiculos'=>'AVIÓN','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>6,'tipo_vehiculos'=>'CASA RODANTE','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>7,'tipo_vehiculos'=>'LANCHA','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>8,'tipo_vehiculos'=>'MOTO ACUÁTICA','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>9,'tipo_vehiculos'=>'MONTACARGAS','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>10,'tipo_vehiculos'=>'BICICLETA','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>11,'tipo_vehiculos'=>'CAMIÓN','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>12,'tipo_vehiculos'=>'DE OTRO TIPO','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>13,'tipo_vehiculos'=>'MOTOCICLETA','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>14,'tipo_vehiculos'=>'BLINDADO','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>15,'tipo_vehiculos'=>'CUATRIMOTO','activo'=>1]);
        DB::table('tipo_vehiculos')->insert(['id_tvehiculos'=>16,'tipo_vehiculos'=>'MAQUINARIA','activo'=>1]);
    }
}
