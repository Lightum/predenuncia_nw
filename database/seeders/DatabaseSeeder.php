<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Localizaciones;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Estados::class);
        $this->call(t_extorsionesSeeder::class);
        $this->call(t_intervinientesSeeder::class);
        $this->call(t_lugaresSeeder::class);
        $this->call(t_delitosSeeder::class);
        $this->call(bienesSeeder::class);
        $this->call(marcas::class);
        $this->call(submarcas::class);
        $this->call(color::class);
        $this->call(aseguradorasSeeder::class);
        $this->call(t_vehiculos::class);
        $this->call(tipos_identificacionSeed::class);
        $this->call(medios_extorsion::class);
        $this->call(MunicipiosSeeder::class);
        $this->call(LocalidadesSeeder::class);
        $this->call(ColoniasSeeder::class);

    }
}
