<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tipos_identificacionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 1 ,'tipo_identificacion' =>'PASAPORTE','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 2 ,'tipo_identificacion' =>'CREDENCIAL ESCOLAR','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 3 ,'tipo_identificacion' =>'OTRO','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 4 ,'tipo_identificacion' =>'ACTA DE NACIMIENTO','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 5 ,'tipo_identificacion' =>'CARTILLA DE SERVICIO MILITAR','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 6 ,'tipo_identificacion' =>'LICENCIA PARA CONDUCIR','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 7 ,'tipo_identificacion' =>'CREDENCIAL DE ELECTOR (IFE O INE)','activo' => 1]);
        DB::table('tipo_identificacion')->insert(['id_tidentificacion' => 8 ,'tipo_identificacion' =>'CREDENCIAL LABORAL','activo' => 1]);
    }
}
