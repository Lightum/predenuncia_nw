<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Facades\DB;

class bienesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_bienes')->insert(['id_tbien' => 1 ,'bien' =>'No entregó bien','activo' => 1]);
        DB::table('tipo_bienes')->insert(['id_tbien' => 2 ,'bien' =>'No especificado','activo' => 1]);
        DB::table('tipo_bienes')->insert(['id_tbien' => 3 ,'bien' =>'Especie','activo' => 1]);
        DB::table('tipo_bienes')->insert(['id_tbien' => 4 ,'bien' =>'Dinero y especie','activo' => 1]);
        DB::table('tipo_bienes')->insert(['id_tbien' => 5 ,'bien' =>'Dinero','activo' => 1]);
    }
}
