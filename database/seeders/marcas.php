<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class marcas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert(['id_marca'=>1,'marca'=>'AUDI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>2,'marca'=>'MERCEDES BENZ','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>3,'marca'=>'JAGUAR','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>4,'marca'=>'MINI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>5,'marca'=>'LOTUS','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>6,'marca'=>'LAND ROVER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>7,'marca'=>'SMART','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>8,'marca'=>'STERLING','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>9,'marca'=>'SAAB','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>10,'marca'=>'HYUNDAI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>11,'marca'=>'FREIGHTLINER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>12,'marca'=>'MCLAREN AUTOMOTIVE','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>13,'marca'=>'CHANGAN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>14,'marca'=>'KIA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>15,'marca'=>'ALFA ROMEO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>16,'marca'=>'FAW','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>17,'marca'=>'PLYMOUTH','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>18,'marca'=>'MASERATI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>19,'marca'=>'DFSK','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>20,'marca'=>'ABARTH','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>21,'marca'=>'VOLKSWAGEN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>22,'marca'=>'LINCOLN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>23,'marca'=>'BMW','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>24,'marca'=>'SUZUKI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>25,'marca'=>'FERRARI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>26,'marca'=>'SUBARU','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>27,'marca'=>'ROVER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>28,'marca'=>'RAM','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>29,'marca'=>'PEUGEOT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>30,'marca'=>'FIAT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>31,'marca'=>'LEXUS','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>32,'marca'=>'MERCURY','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>33,'marca'=>'GMC','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>34,'marca'=>'HONDA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>35,'marca'=>'VÜHL','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>36,'marca'=>'GEO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>37,'marca'=>'MASTRETTA DESIGN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>38,'marca'=>'PIPER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>39,'marca'=>'CHEVROLET','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>40,'marca'=>'SATURN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>41,'marca'=>'SEAT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>42,'marca'=>'DINA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>43,'marca'=>'BUICK','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>44,'marca'=>'ISUZU','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>45,'marca'=>'RAMBLER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>46,'marca'=>'DE OTRA MARCA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>47,'marca'=>'LAMBORGHINI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>48,'marca'=>'ROLLS ROYCE','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>49,'marca'=>'HUMMER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>50,'marca'=>'FORD','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>51,'marca'=>'CHRYSLER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>52,'marca'=>'JEEP','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>53,'marca'=>'PONTIAC','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>54,'marca'=>'NISSAN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>55,'marca'=>'GIANT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>56,'marca'=>'MITSUBISHI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>57,'marca'=>'KENWORTH','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>58,'marca'=>'CITR','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>59,'marca'=>'MG','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>60,'marca'=>'TESLA MOTORS','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>61,'marca'=>'CADILLAC','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>62,'marca'=>'SRT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>63,'marca'=>'PORSCHE','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>64,'marca'=>'VOLVO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>65,'marca'=>'ACURA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>66,'marca'=>'SALEEN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>67,'marca'=>'INTERNATIONAL','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>68,'marca'=>'DODGE','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>69,'marca'=>'TOYOTA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>70,'marca'=>'MAZDA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>71,'marca'=>'RENAULT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>72,'marca'=>'ASTON MARTIN','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>73,'marca'=>'SHELBY','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>74,'marca'=>'BENTLEY','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>75,'marca'=>'INFINITI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>77,'marca'=>'APRILIA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>78,'marca'=>'BAJAJ','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>79,'marca'=>'BKM','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>80,'marca'=>'BMW','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>81,'marca'=>'CAGIVA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>82,'marca'=>'CAN-AM','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>83,'marca'=>'CARABELLA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>84,'marca'=>'DINAMO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>85,'marca'=>'DUCATI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>86,'marca'=>'GILERA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>87,'marca'=>'HARLEY-DAVISON','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>88,'marca'=>'HONDA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>89,'marca'=>'HUSQVARNA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>90,'marca'=>'HUSVARNA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>91,'marca'=>'ITALIKA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>92,'marca'=>'KAWASAKI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>93,'marca'=>'KTM','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>94,'marca'=>'LOW BLOW CHOPPER','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>95,'marca'=>'MAX','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>96,'marca'=>'MV AGUSTA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>97,'marca'=>'OTRA MARCA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>98,'marca'=>'PEUGEOT','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>99,'marca'=>'PIAGGIO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>100,'marca'=>'SUZUKI','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>101,'marca'=>'TRIUMPH','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>102,'marca'=>'VENTO','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>103,'marca'=>'VESPA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>104,'marca'=>'YAMAHA','activo'=>1]);
        DB::table('marcas')->insert(['id_marca'=>105,'marca'=>'ZANETTI','activo'=>1]);
    }
}