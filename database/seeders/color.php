<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Facades\DB;

class color extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('color')->insert(['id_color' => 1 ,'color' =>'VERDE','activo' => 1]);
        DB::table('color')->insert(['id_color' => 2 ,'color' =>'CAFÉ','activo' => 1]);
        DB::table('color')->insert(['id_color' => 3 ,'color' =>'NEGRO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 4 ,'color' =>'AMARILLO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 5 ,'color' =>'ROSA','activo' => 1]);
        DB::table('color')->insert(['id_color' => 6 ,'color' =>'BLANCO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 7 ,'color' =>'DORADO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 8 ,'color' =>'NARANJA','activo' => 1]);
        DB::table('color')->insert(['id_color' => 9 ,'color' =>'ROJO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 10 ,'color' =>'VINO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 11 ,'color' =>'MORADO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 12 ,'color' =>'AZUL','activo' => 1]);
        DB::table('color')->insert(['id_color' => 13 ,'color' =>'NO ESPECIFICADO','activo' => 1]);
        DB::table('color')->insert(['id_color' => 14 ,'color' =>'PLATA','activo' => 1]);
        DB::table('color')->insert(['id_color' => 15 ,'color' =>'BEIGE','activo' => 1]);
        DB::table('color')->insert(['id_color' => 16 ,'color' =>'GRIS','activo' => 1]);
    }
}
