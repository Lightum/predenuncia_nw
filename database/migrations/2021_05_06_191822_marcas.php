<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Marcas extends Migration
{

    public function up()
    {
        Schema::create('marcas', function (Blueprint $table){
            $table->id('id_marca');
            $table->string('marca');
            $table->boolean('activo');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('marcas');
    }
}
