<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Localidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localidades', function (Blueprint $table){
            $table->id('id_localidades');
            $table->string('nombre');
            $table->boolean('activo');
            $table->unsignedBigInteger('id_municipios');
            $table->foreign('id_municipios')->references('id_municipios')->on('municipios');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('localidades');
    }
}
