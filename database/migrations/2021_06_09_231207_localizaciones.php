<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Localizaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localizaciones', function (Blueprint $table) {//
            $table->id('id_localizacion');

            $table->unsignedBigInteger('id_estados');
            $table->foreign('id_estados')->references('id_estados')->on('estados');

            $table->unsignedBigInteger('id_municipios');
            $table->foreign('id_municipios')->references('id_municipios')->on('municipios');

            $table->unsignedBigInteger('id_colonias');
            $table->foreign('id_colonias')->references('id_colonias')->on('colonias');

            $table->string('localidad')->nullable();
            $table->string('calle');
            $table->string('codigo_postal');
            $table->string('num_int')->nullable();
            $table->string('num_ext');

            $table->string('modelo_id');
            $table->string('modelo_type');

            $table->boolean('activo');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('localizaciones');
    }
}
