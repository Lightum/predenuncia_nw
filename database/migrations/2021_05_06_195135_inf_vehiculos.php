<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InfVehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->id('id_vehiculos');

            $table->unsignedBigInteger('id_marca');
            $table->foreign('id_marca')->references('id_marca')->on('marcas');

            $table->unsignedBigInteger('id_submarca');
            $table->foreign('id_submarca')->references('id_submarca')->on('submarcas');

            $table->unsignedBigInteger('id_tvehiculos');
            $table->foreign('id_tvehiculos')->references('id_tvehiculos')->on('tipo_vehiculos');

            $table->unsignedBigInteger('id_color');
            $table->foreign('id_color')->references('id_color')->on('color');

            $table->string('placas');
            $table->string('num_serie');
            $table->string('motor');

            $table->unsignedBigInteger('id_aseguradora');
            $table->foreign('id_aseguradora')->references('id_aseguradora')->on('aseguradoras');

            $table->unsignedBigInteger('id_denuncias');
            $table->foreign('id_denuncias')->references('id_denuncias')->on('denuncias');

            $table->boolean('activo');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('vehiculos');
    }
}
