<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Colonias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colonias', function (Blueprint $table) {
            $table->id('id_colonias');
            $table->string('colonia');
            $table->integer('codigo_postal');
            $table->boolean('activo');
            $table->unsignedBigInteger('id_municipios');
            $table->foreign('id_municipios')->references('id_municipios')->on('municipios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('colonias');
    }
}
