<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Submarcas extends Migration
{
    
    public function up()
    {
        Schema::create('submarcas',function(Blueprint $table){
            $table->id('id_submarca');
            $table->string('submarca');
            $table->unsignedBigInteger('id_marca');
            $table->foreign('id_marca')->references('id_marca')->on('marcas');
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::drop('submarcas');
    }
}
