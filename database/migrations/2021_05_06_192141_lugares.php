<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lugares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('hechos', function (Blueprint $table) {
            $table->id('id_hechos');
            $table->longtext('hechos');
            $table->time('hora_hechos');
            $table->date('fecha_hechos');
            $table->boolean('violencia');
            $table->longtext('robo_objetos')->nullable();

            $table->unsignedBigInteger('id_tlugares');
            $table->foreign('id_tlugares')->references('id_tlugares')->on('t_lugares');
            $table->boolean('lugar_hallazgo');
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hechos');
    }
}
