<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Empresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id('id_empresa');
            $table->string('razon_social');
            $table->string('nom_representante');
            $table->string('app_representante');
            $table->string('apm_representante');
            $table->unsignedBigInteger('id_denuncias');
            $table->foreign('id_denuncias')->references('id_denuncias')->on('denuncias');
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('empresas');
    }
}
