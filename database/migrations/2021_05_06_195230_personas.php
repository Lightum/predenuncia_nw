<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Personas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {

            $table->id('id_personas');
            $table->string('nom_persona');
            $table->string('app_persona')->nullable();
            $table->string('apm_persona')->nullable();
            $table->string('sexo')->nullable();
            $table->integer('edad')->nullable();

            $table->string('telefono',10)->nullable();
            $table->string('correo')->nullable();
            
            $table->unsignedBigInteger('id_denuncias');
            $table->foreign('id_denuncias')->references('id_denuncias')->on('denuncias');

            $table->unsignedBigInteger('id_interviniente');
            $table->foreign('id_interviniente')->references('id_interviniente')->on('intervinientes');

            $table->boolean('tipo_personas')->nullable();
            $table->longtext('media_filiacion')->nullable();

            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personas'); //
    }
}
