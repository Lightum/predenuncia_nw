<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Extorsiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('extorsiones', function (Blueprint $table) {
            $table->id('id_extorsiones');
            $table->boolean('victima');
            $table->time('extorsion_hora');
            $table->date('extorsion_fecha');

            $table->unsignedBigInteger('id_textorsion');
            $table->foreign('id_textorsion')->references('id_textorsion')->on('t_extorsiones');

            $table->unsignedBigInteger('id_mextorsion');
            $table->foreign('id_mextorsion')->references('id_mextorsion')->on('medio_extorsiones');

            $table->string('medio_extorsion');

            $table->unsignedBigInteger('id_tidentificacion');
            $table->foreign('id_tidentificacion')->references('id_tidentificacion')->on('tipo_identificacion');

            $table->string('otra_identificacion')->nullable();
            $table->string('identificacion');
            $table->string('institucion_receptora');
            $table->string('otro_textorsion')->nullable();
            $table->string('otro_medio')->nullable();

            $table->unsignedBigInteger('id_tbien');
            $table->foreign('id_tbien')->references('id_tbien')->on('tipo_bienes');

            $table->string('bien_entregado')->nullable();
            $table->string('valor_bien')->nullable();
            $table->string('banco_cuenta')->nullable();
            $table->string('no_cuenta')->nullable();

            $table->unsignedBigInteger('id_hechos');
            $table->foreign('id_hechos')->references('id_hechos')->on('hechos');

            $table->unsignedBigInteger('id_denuncias');
            $table->foreign('id_denuncias')->references('id_denuncias')->on('denuncias');

            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('extorsiones');
    }
}
