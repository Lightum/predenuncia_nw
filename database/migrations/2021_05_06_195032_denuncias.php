<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Denuncias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias', function (Blueprint $table){
            $table->id('id_denuncias');

            $table->boolean('denuncia_066');
            $table->integer('procesado');
            $table->string('folio')->nullable();
            $table->date('fecha_denuncia');
            $table->longtext('foto_denuncia')->nullable();
            $table->longtext('video_denuncia')->nullable();

            $table->unsignedBigInteger('id_tdelitos');
            $table->foreign('id_tdelitos')->references('id_tdelitos')->on('tipo_delitos');

            $table->unsignedBigInteger('id_hechos');
            $table->foreign('id_hechos')->references('id_hechos')->on('hechos');

            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('denuncias');
    }
}
