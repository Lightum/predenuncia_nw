var cURL = 'http://dev.fiscaliaedomex.gob.mx:5683/qa/predenuncia';
var url_rout = 'http://dev.fiscaliaedomex.gob.mx:5683/qa/predenuncia';

//// combo de codigo postal
$(".id_colonia").change(function() {

    var id_colonia = $(".id_colonia").val();
    var uRL = window.location.hostname;
    console.log(id_colonia);
    console.log(uRL);

    url = uRL + "/codigo_postal/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_colonia": id_colonia,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $('.cp_denunciante').attr('readonly', true);
            $(".cp_denunciante").val(data);
        }
    });
});

$(".id_colonia1").change(function() {

    var id_colonia1 = $(".id_colonia1").val();
    console.log(id_colonia1);

    url = cURL + "/codigo_postal/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_colonia": id_colonia1,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $('#cp_hechos').attr('readonly', true);
            $("#cp_hechos").val(data);
        }
    });
});