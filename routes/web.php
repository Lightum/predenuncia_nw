<?php
use App\Http\Controllers\DenunciaPersonaDesaparecidaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SecuestroController;
use App\Http\Controllers\ExtorsionController;
use App\Http\Controllers\LesionesController;
use App\Http\Controllers\VehiculosController;
use App\Http\Controllers\DenunciasController;
use App\Http\Controllers\DelitosSexualesController;
use App\Http\Controllers\RoboTransporteController;
use App\Http\Controllers\RoboCasaController;
use App\Http\Controllers\TrataController;
use App\Http\Controllers\HomicidiosController;
use App\Http\Controllers\FlagranciaController;
use App\Http\Controllers\RoboViolenciaController;
use App\Http\Controllers\TorturaController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RecursosController;
use App\Http\Controllers\ViolenciaFamiliarController;
use App\Http\Controllers\ExtorsionRedes;
use App\Http\Controllers\LesionGeneroController;
use App\Http\Controllers\SustraccionHijoController;
use App\Http\Controllers\IncumplimientoAlimentariasController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//para ver los estilos
Route::get('/welcome', function () {
    return view('welcome');
});
//rutas del sistema
Route::get('/', function () {
    return view('principal');
})->name('principal');
Route::get('/denuncias_extorsion', function() {
	return view('denuncias_extorsion.principal');
})->name('denuncias_extorsion');
Route::get('extorsion_redes_sociales', [ExtorsionRedes::class, 'alta_extorsion'])->name('extorsion_redes_sociales');


Route::get('telefonica', [ExtorsionController::class, 'tel'])->name('telefonica');
Route::get('correo', [ExtorsionController::class, 'cor'])->name('correo');

Route::get('/finaliza_denuncia', function() {
	return view('listado_denuncias.finish_form');
})->name('finaliza_denuncia');
//rutas lista de denuncias

Route::group(['prefix' => 'lista_denuncias'], function() {
    Route::get('/', function() {
        return view('listado_denuncias.principal');
    })->name('lista_denuncias');

    Route::get('denuncia_tortura', function() {
        return view('listado_denuncias.denuncia_tortura');
    })->name('denuncia_tortura');

    // Route::get('denuncia_flagrancia', function() {
    //     return view('listado_denuncias.denuncia_flagrancia');
    // })->name('denuncia_flagrancia');

    // Route::get('denuncia_pdesaparecida', function() {
    //     return view('listado_denuncias.denuncia_pdesaparecida');
    // })->name('denuncia_pdesaparecida');

    Route::get('denuncia_secuestro', function() {
        return view('listado_denuncias.denuncia_secuestro');
    })->name('denuncia_secuestro');

    Route::get('denuncia_rtransporte', function() {
        return view('listado_denuncias.denuncia_rtransporte');
    })->name('denuncia_rtransporte');
    //Denuncia por robo de casa habitación
    Route::get('denuncia_rviolencia', function() {
        return view('listado_denuncias.denuncia_rviolencia');
    })->name('denuncia_rviolencia');

    Route::get('denuncia_dsexuales', function() {
        return view('listado_denuncias.denuncia_dsexuales');
    })->name('denuncia_dsexuales');


    Route::get('denuncia_otras', function() {
        return view('listado_denuncias.denuncia_otras');
    })->name('denuncia_otras');
});

Route::group(['prefix' => 'denuncias_robo'], function() {
    Route::get('/', function() {
        return view('denuncias_robos.principal');
    })->name('robo');
});

Route::group(['prefix' => 'denuncias_genero'], function() {
    Route::get('/', function() {
        return view('denuncias_genero.principal');
    })->name('genero');
});

Route::resource('secuestro', SecuestroController::class);
Route::get('denuncia_secuestro', [SecuestroController::class, 'alta_secuestro'])->name('denuncia_secuestro');

Route::resource('rtransporte', RoboTransporteController::class);
Route::get('denuncia_rtransporte', [RoboTransporteController::class, 'alta_rtransporte'])->name('denuncia_rtransporte');

Route::resource('pdesaparecida', DenunciaPersonaDesaparecidaController::class);
Route::get('denuncia_pdesaparecida', [DenunciaPersonaDesaparecidaController::class, 'create'])->name('denuncia_pdesaparecida');
Route::resource('delitos_sexuales', DelitosSexualesController::class);
Route::get('denuncia_dsexuales', [DelitosSexualesController::class, 'alta_dsexual'])->name('denuncia_dsexuales');
Route::resource('delitos_rviolencia', RoboViolenciaController::class);
Route::get('denuncia_rviolencia', [RoboViolenciaController::class, 'alta_rviolencia'])->name('denuncia_rviolencia');
Route::resource('delito_tortura', TorturaController::class);
Route::get('denuncia_tortura', [TorturaController::class, 'alta_dtortura'])->name('denuncia_tortura');

Route::post('combo_municipios',[DenunciasController::class, 'combo_municipios'])->name('combo_municipios');
//no la encuentra en el servidor online
Route::get('combo_coloniasGet',[DenunciasController::class, 'combo_colonias'])->name('combo_coloniasGet');

///rutas de lesiones
Route::resource('lesiones', LesionesController::class);
// Route::get('lesiones', [LesionesController::class, 'create'])->name('lesiones');
// Route::post('lesiones_store', [LesionesController::class, 'lesiones_store'])->name('lesiones_store');
//rutas de vehículos
Route::resource('vehiculos', VehiculosController::class);
Route::post('combo_submarca',[VehiculosController::class, 'combo_submarca'])->name('combo_submarca');
//rutas de trata
Route::resource('trata', TrataController::class);
//rutas de denuncia homicidio
Route::resource('homicidios', HomicidiosController::class);

//Rutas para la extorsion
Route::get('denuncias_extorsion', [ExtorsionController::class, 'alta_extorsion'])->name('denuncias_extorsion');
Route::get('combo',[ExtorsionController::class, 'combo'])->name('combo');
Route::post('guarda_extorsion', [ExtorsionController::class, 'guarda_extorsion'])->name('guarda_extorsion');

//rutas de Robo de casa
Route::get('denuncia_rcasa',[RoboCasaController::class,'alta_robo_casa'])->name('denuncia_rcasa');
Route::post('guarda_rcasa', [RoboCasaController::class, 'guarda_rcasa'])->name('guarda_rcasa');
// rutas de Robo en flagrancia
Route::resource('flagrancia', FlagranciaController::class);

///rutas de ViolenciaFamiliar
Route::resource('violencia_familiar', ViolenciaFamiliarController::class);
Route::get('violencia_familiar',[ViolenciaFamiliarController::class,'create'])->name('violencia_familiar');

///rutas de Lesiones de Genero
Route::resource('lesion_genero', LesionGeneroController::class);
Route::get('lesion_genero', [LesionGeneroController::class, 'lesiones'])->name('lesion_genero');

///rutas de Sustraccion de Hijo 
Route::resource('sustraccion_hijo', SustraccionHijoController::class);
Route::get('sustraccion_hijo', [SustraccionHijoController::class, 'sustraccion'])->name('sustraccion_hijo');

///rutas de Incumplimiento de Obligaciones Alimentarias
Route::resource('incum_alimen', IncumplimientoAlimentariasController::class);
Route::get('incum_alimen', [IncumplimientoAlimentariasController::class, 'incum'])->name('incum_alimen');


// rutas de login
Route::get('login', [LoginController::class,'login'])->name('login');
Route::post('validar', [LoginController::class,'validar'])->name('validar');
Route::get('inicio', [LoginController::class,'inicio'])->name('inicio');
//consultas de denuncias
Route::get('consulta_denuncias', [DenunciasController::class, 'index'])->name('consulta_denuncias');
Route::get('json_denuncias', [DenunciasController::class, 'json_denuncias'])->name('json_denuncias');
Route::get('pdf_denuncias/{id}', [DenunciasController::class, 'pdf_denuncias'])->name('pdf_denuncias');
/// get codigo postal
Route::post('codigo_postal',[RecursosController::class, 'codigo_postal'])->name('codigo_postal');
Route::post('combo_loc',[DenunciasController::class, 'combo_loc'])->name('combo_loc');

