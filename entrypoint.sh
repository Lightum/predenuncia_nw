#!/bin/sh

# Esperar a que MySQL esté disponible
echo "Esperando a que MySQL esté disponible..."
until nc -z -v -w30 mysql 3306; do
  echo "Esperando a que MySQL esté disponible..."
  sleep 1
done

# Ejecutar migraciones y seeders
echo "MySQL está disponible. Ejecutando migraciones y seeders..."
php artisan migrate:fresh --seed

# Ejecutar el comando original del contenedor (Apache)
apache2-foreground
