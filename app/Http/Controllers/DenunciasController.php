<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\GeneralRequest;
use App\Models\Persona;
use App\Models\Denuncia;
use App\Models\Vehiculos;
use App\Models\Hechos;
use App\Models\Empresa;
use App\Models\Submarcas;
use App\Models\Municipios;
use App\Models\Colonias;
use App\Models\Localidades;
use App\Models\Lugar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PDF;

class DenunciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administracion.consulta_denuncias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function json_denuncias()
    {
        $jsonData = datatables()->eloquent(Denuncia::orderBy('id_denuncias', 'DESC'))
        //->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        //->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        //->rawColumns(['Editar','Eliminar'])
        ->addColumn('btn','administracion.pdf_btn')
        ->rawColumns(['btn'])
        ->toJson();

        return $jsonData;
    }

    public function show_modelov(Request $request)
    {
        if(isset($request->texto)){
            $modelos = Submarcas::whereid_marca($request->texto)->get();
            return response()->json(
                [
                    'lista' => $modelos,
                    'success' => true
                ]
            );
        }else{
            return response()->json(
                [
                    'lista' => [],
                    'success' => false
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdf_denuncias(Request $request)
    {

        $id = $request->id;
        $denuncia = DB::table('denuncias AS d')
                            ->select('*')
                            ->where('d.id_denuncias', '=', $id)
                            ->first();

        if(isset($denuncia)){

            $denunciante = DB::table('personas AS den')
                                    ->select('*')
                                    ->where('den.id_denuncias', '=', $id)
                                    ->where('den.id_interviniente','=', 2)
                                    ->get();

            $empresa = DB::table('empresas AS emp')
            ->select('*')
            ->where('emp.id_denuncias', '=', $id)
            ->get();

            $hechos =  DB::table('hechos')
                                ->select('*')
                                ->where('id_hechos','=',$denuncia->id_hechos)
                                ->get();


            $vehiculos =  DB::table('vehiculos')
            ->select('*')
            ->where('id_denuncias','=',$denuncia->id_hechos)
            ->get();

            $lugar = Hechos::find($id)->direcciones()->get();

            $localidad = DB::table('localidades')
                ->select('nombre')
                ->where('id_localidades', $lugar[0]->localidad)->first();

            $lugar_persona = Persona::find($denunciante[0]->id_personas)->direcciones()->get();



            $presponsable = DB::table('personas AS den')
            ->select('*')
            ->where('den.id_denuncias', '=', $id)
            ->where('den.id_interviniente','=', 3)
            ->get();

            $extorsiones = DB::table('extorsiones')
            ->select('*')
            ->where('id_denuncias', '=', $id)
            ->get();

            $data = [
                'denuncia' => $denuncia,
                'denunciante' => $denunciante,
                'empresa' => $empresa,
                'hechos'=>$hechos,
                'vehiculos'=>$vehiculos,
                'lugar'=>$lugar,
                'lugar_persona'=>$lugar_persona,
                'localidad'=>$localidad,
                'extorsiones'=>$extorsiones,
                'presponsable'=>$presponsable
            ];

            $pdf = PDF::setOptions(['isRemoteEnabled' => false]);
            $pdf = PDF::loadView('administracion.pdf_denuncias', $data);
            return $pdf->download("$denuncia->folio.pdf");
        }else{
            echo "No existen denuncias";
        }
    }

    function combo_municipios(Request $request){
        $id_estado = $request->post('id_estado');
        $municipios = municipios::where('id_estados','=',$id_estado)->orderBy('municipio', 'asc')->get();
        return $municipios;
    }

    function combo_colonias(Request $request){
        $id_municipios = $request->get('id_municipios');
        $colonias = colonias::where('id_municipios','=',$id_municipios)->orderBy('colonia', 'asc')->get();
        return view ('listado_denuncias.combo')->with('colonias',$colonias);
    }

    function combo_loc(Request $request){

        $id_municipio = $request->post('id_municipio');
        $localidades = localidades::where('id_municipios','=',$id_municipio)->orderBy('nombre', 'asc')->get();
        return $localidades;

    }
}
