<?php

namespace App\Http\Controllers;

use App\Models\Lesiones;
use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Denuncia;
use App\Models\Lugar;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\GeneralRequest;
use App\Models\Colonias;
use App\Models\Lugares;
use App\Models\TLugar;
use App\Models\Intervinientes;
use App\Models\Hechos;
use App\Models\Empresa;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Http;


class SustraccionHijoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('lista_denuncias');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sustraccion()
    {

        $estados = DB::select('select* from estados');
        $id_estado = $estados[0]->id_estados;
        $interviniente = DB::select('select * from intervinientes');
        $tlugar = DB::select('select * from t_lugares');
        $municipios = DB::table('municipios')
            ->select('*')
            ->where('id_estados', '=', $id_estado)
            ->orderBy('municipio', 'asc')
            ->get();

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        return view('listado_denuncias.denuncia_sustraccion_hijo')->with('estados', $estados)
            ->with('municipios', $municipios)
            ->with('municipios_hechos', $municipios_hechos)
            ->with('interviniente', $interviniente)
            ->with('tlugar', $tlugar);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\$request->input\Lesiones$request->input  $request->input
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralRequest $request)
    {

        try { 
        $httpClient = new \GuzzleHttp\Client();
        $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

        //denunciante ----------------------------------------------------------------------------------
        $contentBody['isVictima'] =  'true';
        $contentBody['name'] = "Product Demo";

        if ($request->input('tipo_persona') == 0) {

            $contentBody['isPersonaMoral'] = true;
            $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
            $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
            $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
            $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

        }else{
            $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
            $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
            $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
        }


        $contentBody['id_sexo'] = $request->input('genero_denunciante');
        $contentBody['edad'] = $request->input('edad_denunciante');
        $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
        $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
        $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
        $contentBody['codigoPostal'] = $request->input('cp_denunciante');
        $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
        $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
        $contentBody['telefono'] = $request->input('telefono_denunciante');
        $contentBody['correoElectronico'] = $request->input('correo_denunciante');
        //hechos ----------------------------------------------------------------------------------
        $hora = explode(":", $request->input('hora_hechos'));
        $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
        $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
        $contentBody['hechos']  = trim(mb_strtoupper($request->input('descripcion'), 'utf-8'));

        if ($request->input('isHechosViolencia') == 0) {
            $contentBody['isHechosViolencia'] =  'false';
        }else{
            $contentBody['isHechosViolencia'] =  'true';
        }

        $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
        $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
        $contentBody['estado_id'] =  15;
        $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
        $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
        $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
        $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
        $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
        $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
        //probables responsables ----------------------------------------------------------------------------------
        $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
        $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
        $contentBody['mediaFiliacion'] =   trim(mb_strtoupper($request->input('media_filiacion'), 'utf-8')); //media_filiacion
        //objetos
        $contentBody['roboObjetos'] =  trim(mb_strtoupper($request->input('robo_objetos'), 'utf-8'));
        $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

        if($req->getBody()){

            $respuesta = json_decode($req->getBody(),true);
            $folio_pre = $respuesta['folio'];
        }

    } catch (\Exception $e) {
        DB::rollback();
        //return $e->getMessage();
        $folio_pre="LOCAL";
    }

        if($folio_pre!=NULL){
            ///Inicia el store
            DB::beginTransaction();

            $hechos = new Hechos;
            $denuncia = new Denuncia;
            $persona = new Persona;
            $localizacion = new Lugar;
            $localizacionh = new Lugar;
            $empresa = new Empresa;


            try { /// try hechos

                $hechos->hechos  = mb_strtoupper($request->input('descripcion'), 'utf-8');
                $hechos->fecha_hechos  = $request->input('fecha_hechos');
                $hechos->hora_hechos  = $request->input('hora_hechos');
                $hechos->violencia  = $request->input('violencia');
                $hechos->robo_objetos  = $request->input('robo_objetos');
                $hechos->id_tlugares  = $request->input('id_tipolugar');
                $hechos->lugar_hallazgo = $request->input('lugar_hallazgo');
                $hechos->activo  = 1;

                $hechos->save();
                $id_hechos = $hechos->id_hechos;
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }

            try { /// try denuncia
                $url = NULL;
                $url_video = NULL;

                if ($request->file('foto')) {
                    $imagenes = $request->file('foto')->store('public/imagenes');
                    $url = Storage::url($imagenes);
                }

                if ($request->file('video')) {
                    $videos = $request->file('video')->store('public/videos');
                    $url_video = Storage::url($videos);
                }

                $denuncia->denuncia_066  = $request->input('denuncia_066');
                $denuncia->procesado  = 0; //default
                $denuncia->folio  = 0; // se llena hasta que llegue a SIGI
                $denuncia->fecha_denuncia  = date('Y/m/d');
                $denuncia->foto_denuncia  = $url; /// ver como subir foto
                $denuncia->video_denuncia  = $url_video; /// ver como subir video
                $denuncia->id_tdelitos = 15; //violencia familiar
                $denuncia->id_hechos = $id_hechos;
                $denuncia->activo = 1;
                $denuncia->save();
                $id_denuncias =  $denuncia->id_denuncias;
                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncias, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $denuncia->folio=$folio;
                $denuncia->update();

            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }

            try {


                if ($request->input('tipo_persona') == 0) {

                    $empresa->razon_social = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                    $empresa->nom_representante = mb_strtoupper($request->input('nom_representante'), 'utf-8');
                    $empresa->app_representante = mb_strtoupper($request->input('app_representante'), 'utf-8');
                    $empresa->apm_representante = mb_strtoupper($request->input('apm_representante'), 'utf-8');
                    $empresa->id_denuncias = $id_denuncias;
                    $empresa->activo = 1;

                    $empresa->save();
                }

                    $persona->nom_persona = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                    $persona->tipo_personas = 1;
                    $persona->sexo = $request->input('genero_denunciante');
                    $persona->edad = $request->input('edad_denunciante');
                    $persona->telefono = $request->input('telefono_denunciante');
                    $persona->correo = $request->input('correo_denunciante');
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 2;
                    $persona->activo = 1;

                    $persona->save();

                //direccion denunciante
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];

                $persona->direcciones()->create($localizacion);

                if ($request->input('tipo_persona_p') == 1) {
                    //persona probable responsable
                    $persona_inc = new Persona;
                    $persona_inc->nom_persona = mb_strtoupper($request->input('nombre_probable'), 'utf-8');
                    $persona_inc->app_persona = mb_strtoupper($request->input('appat_probable'), 'utf-8');
                    $persona_inc->apm_persona = mb_strtoupper($request->input('apmat_probable'), 'utf-8');
                    $persona_inc->tipo_personas = 3;
                    $persona_inc->sexo = $request->input('genero_probable');
                    $persona_inc->edad = NULL;
                    $persona_inc->telefono = NULL;
                    $persona_inc->correo = NULL;
                    $persona_inc->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $persona_inc->id_denuncias = $id_denuncias;
                    $persona_inc->id_interviniente = 3;
                    $persona_inc->activo = 1;

                    $persona_inc->save();
                } else {
                    //persona inculpado
                    $persona_inc = new Persona;
                    $persona_inc->nom_persona = "QRR";
                    $persona_inc->app_persona =  "QRR";
                    $persona_inc->apm_persona = "QRR";
                    $persona_inc->tipo_personas = 3;
                    $persona_inc->sexo = NULL;
                    $persona_inc->edad = NULL;
                    $persona_inc->telefono = NULL;
                    $persona_inc->correo = NULL;
                    $persona_inc->media_filiacion = NULL;
                    $persona_inc->id_denuncias = $id_denuncias;
                    $persona_inc->id_interviniente = 3;
                    $persona_inc->activo = 1;

                    $persona_inc->save();
                }
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }

            try {

                // informacion de localizacion hechos
                $localizacionh = [
                    "id_estados" => 15,
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }
            DB::commit();
            Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
            return redirect()->route('finaliza_denuncia')
                            ->with('folio',$folio)
                            ->with('n_folio',$id_denuncias);

        }else{
            Alert::html('FGJEM', 'El servicio no está disponible por el momento', 'info');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lesiones  $lesiones
     * @return \Illuminate\Http\Response
     */
    public function show(Lesiones $lesiones)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lesiones  $lesiones
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesiones $lesiones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\$request->input  $request->input
     * @param  \App\Models\Lesiones  $lesiones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesiones $lesiones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesiones  $lesiones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesiones $lesiones)
    {
        //
    }



}
