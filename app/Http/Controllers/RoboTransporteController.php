<?php

namespace App\Http\Controllers;

use App\Models\Hechos;
use App\Models\Denuncia;
use App\Models\Empresa;
use App\Models\Persona;
use App\Models\Lugar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\GeneralRequest;
use RealRashid\SweetAlert\Facades\Alert;

class RoboTransporteController extends Controller
{

    public function alta_rtransporte(){
        $estados=DB::select('select* from estados');
        $municipios=DB::select('select * from municipios');
        $tlugar=DB::select('select * from t_lugares');

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        return view('listado_denuncias.denuncia_rtransporte')
        ->with('estados', $estados)
        ->with('municipios',$municipios)
        ->with('municipios_hechos',$municipios_hechos)
        ->with('tlugar',$tlugar);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralRequest $request)
    {

        try{
        $httpClient = new \GuzzleHttp\Client();
        $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

        //denunciante ----------------------------------------------------------------------------------
        $contentBody['isVictima'] =  'true';
        $contentBody['name'] = "Product Demo";

        if ($request->input('tipo_persona') == 0) {

            $contentBody['isPersonaMoral'] = true;
            $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
            $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
            $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
            $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

        }else{
            $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
            $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
            $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
        }


        $contentBody['id_sexo'] = $request->input('genero_denunciante');
        $contentBody['edad'] = $request->input('edad_denunciante');
        $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
        $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
        $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
        $contentBody['codigoPostal'] = $request->input('cp_denunciante');
        $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
        $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
        $contentBody['telefono'] = $request->input('telefono_denunciante');
        $contentBody['correoElectronico'] = $request->input('correo_denunciante');
        //hechos ----------------------------------------------------------------------------------
        $hora = explode(":", $request->input('hora_hechos'));
        $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
        $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
        $contentBody['hechos']  = trim(mb_strtoupper($request->input('descripcion'), 'utf-8'));

        if ($request->input('isHechosViolencia') == 0) {
            $contentBody['isHechosViolencia'] =  'false';
        }else{
            $contentBody['isHechosViolencia'] =  'true';
        }

        $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
        $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
        $contentBody['estado_id'] =  15;
        $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
        $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
        $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
        $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
        $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
        $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
        //probables responsables ----------------------------------------------------------------------------------
        if($request->input('tipo_persona_p') == 1){
            $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
        }else{
            $nombre_pr = "QRR QRR QRR";
        }
        $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
        $contentBody['mediaFiliacion'] =   trim(mb_strtoupper($request->input('media_filiacion'), 'utf-8')); //media_filiacion
        //objetos
        $contentBody['roboObjetos'] =  trim(mb_strtoupper($request->input('robo_objetos'), 'utf-8'));
        $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

        if($req->getBody()){

            $respuesta = json_decode($req->getBody(),true);
            $folio_pre = $respuesta['folio'];
        }

    } catch (\Exception $e) {
        DB::rollback();
        //return $e->getMessage();
        $folio_pre = "LOCAL";
    }

        if ($folio_pre!=NULL){

            $url = null;
            $url_video = null;

            if(request()->file('foto')!="")
            {
                $request->validate([
                    'foto'=> 'image|mimes:jpg,png,jpeg,gif,svg|max:200'
                ]);
                $imagenes = $request->file('foto')->store('public/imagenes');
                $url = Storage::url($imagenes);
            }
            // video
            if(request()->file('video')!="")
            {
                $request->validate([
                    'video'=> 'required|mimes:mp4,ogx,oga,ogv,ogg,webm|max:4096'
                ]);
                $videos = $request->file('video')->store('public/videos');
                $url_video = Storage::url($videos);
            }

            DB::beginTransaction();
            try {
                // HECHOS
                $hechos=new Hechos;
                $hechos->hechos = mb_strtoupper($request->descripcion, 'utf-8');
                $hechos->fecha_hechos = $request->fecha_hechos;
                $hechos->hora_hechos = $request->hora_hechos;
                $hechos->violencia = $request->violencia;
                $hechos->robo_objetos = $request->robo_objetos;
                $hechos->id_tlugares = $request->id_tipolugar;
                $hechos->lugar_hallazgo = $request->lugar_hallazgo;
                $hechos->activo = 1;
                $hechos->save();
                $id_hechos = $hechos->id_hechos;

                // informacion de localizaciones con hechos
                $localizacionh = [
                    "id_estados" => 15,
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);

                // DENUNCIAS
                $datosDenuncia = new Denuncia;
                $datosDenuncia->denuncia_066=$request->denuncia_066;
                $datosDenuncia->procesado=0;
                
                $datosDenuncia->fecha_denuncia = date('Y-m-d');
                $datosDenuncia->foto_denuncia = $url;
                $datosDenuncia->video_denuncia = $url_video;
                $datosDenuncia->id_tdelitos = "5"; // 5 = Robo a transporte publico
                $datosDenuncia->id_hechos = $id_hechos;
                $datosDenuncia->activo = 1;
                $datosDenuncia->save();
                $id_denuncia = $datosDenuncia->id_denuncias;
                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncia, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $datosDenuncia->folio=$folio;
                $datosDenuncia->update();
                // PERSONA MORAL - EMPRESA
                if($request->tipo_persona == 0){
                    $empresa = new Empresa();
                    $empresa->razon_social = mb_strtoupper($request->razon_social_denunciante, 'utf-8');
                    $empresa->nom_representante = mb_strtoupper($request->nom_representante, 'utf-8');
                    $empresa->app_representante = mb_strtoupper($request->app_representante, 'utf-8');
                    $empresa->apm_representante = mb_strtoupper($request->apm_representante, 'utf-8');
                    $empresa->id_denuncias = $id_denuncias;
                    $empresa->activo=1;
                    $empresa->save();
                }
                // PERSONA - DENUNCIANTE
                $denunciante = new Persona();
                $denunciante->nom_persona = mb_strtoupper($request->nombre_denunciante, 'utf-8');
                $denunciante->app_persona = mb_strtoupper($request->apellido_paterno, 'utf-8');
                $denunciante->apm_persona = mb_strtoupper($request->apellido_materno, 'utf-8');
                $denunciante->tipo_personas = 1; // 1 = persona Fisica
                $denunciante->sexo = $request->genero_denunciante;
                $denunciante->edad = $request->edad_denunciante;
                $denunciante->telefono = $request->telefono_denunciante;
                $denunciante->correo = $request->correo_denunciante;
                $denunciante->media_filiacion = "";
                $denunciante->id_denuncias = $id_denuncia;
                $denunciante->id_interviniente = $request->id_interviniente;
                $denunciante->activo=1;
                $denunciante->save();

                //tabla de localizaciones denunciante
                $localizacion = new Lugar;
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];
                $denunciante->direcciones()->create($localizacion);

                // PROBABLE RESPONSABLE
                if($request->tipo_persona_p == 1){
                    $probable = new Persona();
                    $probable->nom_persona = mb_strtoupper($request->nombre_probable, 'utf-8');
                    $probable->app_persona = mb_strtoupper($request->appat_probable, 'utf-8');
                    $probable->apm_persona = mb_strtoupper($request->apmat_probable, 'utf-8');
                    $probable->tipo_personas = 1; // 1 = Persona Fisica
                    $probable->sexo = $request->genero_probable;
                    $probable->edad = $request->edad_denunciante;
                    $probable->telefono = $request->telefono_denunciante;
                    $probable->correo = $request->correo_denunciante;
                    $probable->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $probable->id_denuncias = $id_denuncia;
                    $probable->id_interviniente = 3; // 3 = Probable
                    $probable->activo=1;
                    $probable->save();
                }else{
                    $probable_qrr = new Persona();
                    $probable_qrr->nom_persona = "QRR";
                    $probable_qrr->app_persona = "QRR";
                    $probable_qrr->apm_persona = "QRR" ;
                    $probable_qrr->tipo_personas = 1; // 1 = Persona Fisica
                    $probable_qrr->sexo = NULL;
                    $probable_qrr->edad = NULL;
                    $probable_qrr->telefono = NULL;
                    $probable_qrr->correo = NULL;
                    $probable_qrr->media_filiacion = "";
                    $probable_qrr->id_denuncias = $id_denuncia;
                    $probable_qrr->id_interviniente = 3; // 3 = Probable
                    $probable_qrr->activo = 1;
                    $probable_qrr->save();
                }


                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                if($url) Storage::delete($imagenes);
                if($url_video) Storage::delete($videos);
                return $e->getMessage();
            }
            Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
            return redirect()->route('finaliza_denuncia')
                                ->with('folio',$folio)
                            ->with('n_folio',$id_denuncia);
        }
        else{
            Alert::html('FGJEM', 'El servicio no está disponible por el momento', 'info');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RoboTransporte  $roboTransporte
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RoboTransporte  $roboTransporte
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RoboTransporte  $roboTransporte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RoboTransporte  $roboTransporte
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
