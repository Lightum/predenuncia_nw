<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GeneralRequest;
use App\Models\Hechos;
use App\Models\Lugar;
use App\Models\Persona;
use App\Models\Denuncia;
use App\Models\Empresa;
use App\Models\Colonias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
class TorturaController extends Controller
{
    public function index(){
        return redirect()->route('lista_denuncias');

    }
    public function alta_dtortura(){
        $estados=DB::select('select * from estados');
        $municipios=DB::select('select * from municipios');
        $interviniente=DB::select('select * from intervinientes');
        $tlugar=DB::select('select * from t_lugares');

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        return view('listado_denuncias.denuncia_tortura')
                    ->with('municipios', $municipios)
                    ->with('municipios_hechos', $municipios_hechos)
                    ->with('estados', $estados)
                    ->with('interviniente', $interviniente)
                    ->with('tlugar', $tlugar);
    }

    function combo(Request $request){

        $municipios = $request->get('id_municipio');
        $colonias = colonias::where('id_municipio','=',$municipios)->get();
        return view ('denuncias_extorsion.combo')->with('colonias',$colonias);
    }

    public function create($request){
        return redirect()->route('lista_denuncias');

    }


    public function store(GeneralRequest  $request){

    try {
        $httpClient = new \GuzzleHttp\Client();
        $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

        //denunciante ----------------------------------------------------------------------------------
        $contentBody['isVictima'] =  'true';
        $contentBody['name'] = "Product Demo";

        if ($request->input('tipo_persona') == 0) {

            $contentBody['isPersonaMoral'] = true;
            $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
            $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
            $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
            $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

        }else{
            $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
            $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
            $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
        }


        $contentBody['id_sexo'] = $request->input('genero_denunciante');
        $contentBody['edad'] = $request->input('edad_denunciante');
        $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
        $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
        $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
        $contentBody['codigoPostal'] = $request->input('cp_denunciante');
        $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
        $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
        $contentBody['telefono'] = $request->input('telefono_denunciante');
        $contentBody['correoElectronico'] = $request->input('correo_denunciante');
        //hechos ----------------------------------------------------------------------------------
        $hora = explode(":", $request->input('hora_hechos'));
        $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
        $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
        $contentBody['hechos']  = mb_strtoupper($request->input('descripcion'), 'utf-8');

        if ($request->input('isHechosViolencia') == 0) {
            $contentBody['isHechosViolencia'] =  'false';
        }else{
            $contentBody['isHechosViolencia'] =  'true';
        }

        $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
        $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
        $contentBody['estado_id'] =  15;
        $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
        $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
        $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
        $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
        $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
        $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
        //probables responsables ----------------------------------------------------------------------------------
        $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
        $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
        $contentBody['mediaFiliacion'] =   mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
        //objetos
        $contentBody['roboObjetos'] =  $request->input('robo_objetos');
        $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

        if($req->getBody()){

            $respuesta = json_decode($req->getBody(),true);
            $folio_pre = $respuesta['folio'];
        }

    } catch (\Exception $e) {
        DB::rollback();
        //return $e->getMessage();
        $folio_pre = "LOCAL";
    }


        if($folio_pre!=NULL){

            if($request->file('foto') != '' ){
                $request->validate([
                    'foto'=> 'image|max:200'
                ]);
                $imagenes = $request->file('foto')->store('public/imagenes');
                $url = Storage::url($imagenes);
            }
            if($request->file('video') != ''){
                $request->validate([
                    'video'=> 'max:4096'
                ]);
                $videos = $request->file('video')->store('public/videos');
                $url_video = Storage::url($videos);
            }


            DB::beginTransaction();
            try {
                $hechos = new Hechos;
                $hechos->hechos= mb_strtoupper($request->input('descripcion'), 'utf-8');
                $hechos->hora_hechos=$request->input('hora_hechos');
                $hechos->fecha_hechos=$request->input('fecha_hechos');
                $hechos->violencia=$request->input('violencia');
                $hechos->robo_objetos="";
                $hechos->id_tlugares=$request->input('id_tipolugar');
                $hechos->lugar_hallazgo = $request->input('lugar_hallazgo');
                $hechos->activo="1";
                $hechos->save();
                $id_hechos = $hechos->id_hechos;

                // informacion de localizaciones con hechos
                $localizacionh = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);

                $denunciaDsexual = new Denuncia;
                $denunciaDsexual->denuncia_066=$request->input('denuncia_066');
                $denunciaDsexual->procesado="0";
                $denunciaDsexual->folio =$folio_pre;
                $denunciaDsexual->fecha_denuncia=date('Y-m-d');;

                if($request->file('foto') != ''){
                    $denunciaDsexual->foto_denuncia = $url;
                }
                if($request->file('video') != ''){
                    $denunciaDsexual->video_denuncia = $url_video;
                }

                $denunciaDsexual->id_tdelitos = 13;
                $denunciaDsexual->id_hechos = $id_hechos;
                $denunciaDsexual->activo = 1;
                $denunciaDsexual->save();
                $id_denuncias = $denunciaDsexual->id_denuncias;
                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncias, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $denunciaDsexual->folio=$folio;
                $denunciaDsexual->update();

                if($request->input('tipo_persona') == 0){
                    //empresa
                    $empresa = new Empresa;
                    $empresa->razon_social = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                    $empresa->nom_representante = mb_strtoupper($request->input('nom_representante'), 'utf-8');
                    $empresa->app_representante = mb_strtoupper($request->input('app_representante'), 'utf-8');
                    $empresa->apm_representante = mb_strtoupper($request->input('apm_representante'), 'utf-8');
                    $empresa->id_denuncias = $id_denuncias;
                    $empresa->activo = "1";
                    $empresa->save();
                }
                else{
                    //denunciante
                    $persona = new Persona;
                    $persona->nom_persona = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                    $persona->sexo = $request->input('genero_denunciante');
                    $persona->edad = $request->input('edad_denunciante');
                    $persona->telefono = $request->input('telefono_denunciante');
                    $persona->correo=$request->input('correo_denunciante');
                    $persona->id_denuncias= $id_denuncias;
                    $persona->id_interviniente = 2; // 2=Ofendido
                    $persona->tipo_personas = 1;
                    // $persona->tipo_personas = $request->input('tipo_persona');
                    $persona->media_filiacion = "";
                    $persona->activo = "1";
                    $persona->save();


                    //tabla de localizaciones
                $localizacion = new Lugar;
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];
                $persona->direcciones()->create($localizacion);

        }

                if($request->input('tipo_persona_p') == 1){
                    // dd($request);
                    //Probable
                    $personaProbable = new Persona;
                    $personaProbable-> nom_persona = mb_strtoupper($request->input('nombre_probable'), 'utf-8');
                    $personaProbable-> app_persona = mb_strtoupper($request->input('appat_probable'), 'utf-8');
                    $personaProbable-> apm_persona = mb_strtoupper($request->input('apmat_probable'), 'utf-8');
                    $personaProbable-> sexo = $request->input('genero_probable');
                    $personaProbable-> edad ="0";
                    $personaProbable-> telefono ="S/D";
                    $personaProbable-> correo = "S/D";
                    $personaProbable-> id_denuncias = $id_denuncias;
                    $personaProbable-> id_interviniente = 3; //3=Probable
                    $personaProbable-> tipo_personas = 1;
                    $personaProbable->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $personaProbable-> activo = "1";
                    $personaProbable->save();

                }else{
                    $persona = new Persona;
                    $persona->nom_persona = "QRR";
                    $persona->app_persona =  "QRR";
                    $persona->apm_persona = "QRR";
                    $persona->tipo_personas = 1;
                    $persona->sexo = NULL;
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = "3"; //3=Probable
                    $persona->activo = 1;
                    $persona->save();
                }


            }
            catch(\Exception $e){
                DB::rollBack();
                return $e->getMessage();
            }
            DB::commit();
            Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
            return redirect()->route('finaliza_denuncia')
                                ->with('folio',$folio)
                            ->with('n_folio',$id_denuncias);
        }else{
            Alert::html('FGJEM', 'El servicio no está disponible por el momento', 'info');
        }

    }

}
