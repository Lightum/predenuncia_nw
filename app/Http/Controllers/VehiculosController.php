<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\GeneralRequest;
use App\Models\Persona;
use App\Models\Denuncia;
use App\Models\Vehiculos;
use App\Models\Hechos;
use App\Models\Empresa;
use App\Models\Submarcas;
use App\Models\Lugar;
use Denuncias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class VehiculosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados=DB::select('SELECT * FROM estados');
        $marcas_vehiculo=DB::select('SELECT * FROM marcas ORDER BY marca ASC');
        $t_vehiculos=DB::select('SELECT * FROM tipo_vehiculos');
        $color_vehiculo=DB::select('SELECT * FROM color');
        $tlugar=DB::select('SELECT * FROM t_lugares');
        $aseguradoras = DB::table('aseguradoras')->select('*')->get();


        //dd($marcas_vehiculo);
        $id_estado = $estados[0]->id_estados;
        $municipios = DB::table('municipios')
                            ->select('*')
                            ->where('id_estados', '=', $id_estado)
                            ->orderBy('municipio', 'asc')
                            ->get();

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        return view('denuncia_vehiculos.form_vehiculos')->with('estados', $estados)
                                                           ->with('marcas_vehiculo',$marcas_vehiculo)
                                                           ->with('t_vehiculos',$t_vehiculos)
                                                           ->with('color_vehiculo',$color_vehiculo)
                                                           ->with('aseguradoras',$aseguradoras)
                                                           ->with('municipios',$municipios)
                                                           ->with('municipios_hechos',$municipios_hechos)
                                                           ->with('tlugar',$tlugar);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralRequest $request)
    {
        try {
            $denuncias = new Denuncia;
            $vehiculos = new Vehiculos;
            $hechos = new Hechos;
            $localizacion = new Lugar;
            $empresa= new Empresa;

            $httpClient = new \GuzzleHttp\Client();
            $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

            //denunciante ----------------------------------------------------------------------------------
            $contentBody['isVictima'] =  'true';
            $contentBody['name'] = "Product Demo";

            if ($request->input('tipo_persona') == 0) {

                $contentBody['isPersonaMoral'] = true;
                $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
                $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
                $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

            }else{
                $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
            }


            $contentBody['id_sexo'] = $request->input('genero_denunciante');
            $contentBody['edad'] = $request->input('edad_denunciante');
            $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
            $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
            $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
            $contentBody['codigoPostal'] = $request->input('cp_denunciante');
            $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
            $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
            $contentBody['telefono'] = $request->input('telefono_denunciante');
            $contentBody['correoElectronico'] = $request->input('correo_denunciante');
            //hechos ----------------------------------------------------------------------------------
            $hora = explode(":", $request->input('hora_hechos'));
            $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
            $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
            $contentBody['hechos']  = trim(mb_strtoupper("NO. MOTOR: ".$request->input('num_motor')."\n".$request->input('descripcion'), 'utf-8'));

            if ($request->input('isHechosViolencia') == 0) {
                $contentBody['isHechosViolencia'] =  'false';
            }else{
                $contentBody['isHechosViolencia'] =  'true';
            }

            $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
            $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
            $contentBody['estado_id'] =  $request->input('id_estado');
            $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
            $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
            $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
            $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
            $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
            $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
            //probables responsables ----------------------------------------------------------------------------------
            $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
            $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
            $contentBody['mediaFiliacion'] =   trim(mb_strtoupper($request->input('media_filiacion'), 'utf-8')); //media_filiacion
            //objetos
            $contentBody['roboObjetos'] =  trim(mb_strtoupper($request->input('robo_objetos'), 'utf-8'));
            //vehiculos
            $contentBody['marca_vehiculo_id'] =  $request->input('marca_vehiculo_denunciante');
            $contentBody['tipo_vehiculo_id'] =  $request->input('tipo_vehiculo_denunciante');
            $contentBody['tipo_vehiculo_nombre'] =  $request->input('robo_objetos');
            $contentBody['modeloVehiculo'] =  $request->input('modelo_vehiculo_nombre');
            $contentBody['color_vehiculo_id'] =  $request->input('color_vehiculo_denunciante');
            $contentBody['color_vehiculo_nombre'] =  $request->input('color_vehiculo_denunciante');
            $contentBody['placasVehiculo'] =  $request->input('placas_vehiculo_denunciante');
            $contentBody['vehiculoNumeroSerie'] =  $request->input('num_serie_vehiculo_denunciante');

                if($request->input('aseguradora_vehiculo') == NULL or $request->input('aseguradora_vehiculo') == null){
                    $aseguradora_id = 77;

                }else{
                    $aseguradora_id = $request->input('aseguradora_vehiculo');
                }

            $contentBody['aseguradora_id'] = $aseguradora_id ;
            $contentBody['aseguradora_nombre'] =  $request->input('aseguradora_vehiculo');

            $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

            if($req->getBody()){
                $respuesta = json_decode($req->getBody(),true);
                $folio_pre = $respuesta['folio'];
            }

        } catch (\Exception $e) {
            DB::rollback();
            //return $e->getMessage();
            $folio_pre = "LOCAL";
        }

        if($folio_pre!=NULL){

            try{
                DB::beginTransaction();
                //hechos
                $hechos->hechos = mb_strtoupper($request->input('descripcion'), 'utf-8') ;
                $hechos->hora_hechos = $request->input('hora_hechos');
                $hechos->fecha_hechos = $request->input('fecha_hechos');
                $hechos->violencia = $request->input('violencia');
                $hechos->robo_objetos = $request->input('robo_objetos');
                $hechos->id_tlugares = $request->input('id_tipolugar');
                $hechos->lugar_hallazgo = $request->input('lugar_hallazgo');
                $hechos->activo = 1;
                $hechos->save();
                $id_hechos = $hechos->id_hechos;

                 //archivos
                $url=NULL;
                $url_video=NULL;

                if($request->file('foto')){
                    $imagenes = $request->file('foto')->store('public/imagenes');
                    $url = Storage::url($imagenes);
                }
                if($request->file('video')){
                    $videos = $request->file('video')->store('public/videos');
                    $url_video = Storage::url($videos);
                }

                //denuncia
                $denuncias->denuncia_066 = $request->input('denuncia_066');
                $denuncias->procesado = 0;
                $denuncias->folio=NULL;
                $denuncias->activo = 1;
                $denuncias->fecha_denuncia = date("Y-m-d");
                $denuncias->foto_denuncia = $url;
                $denuncias->video_denuncia = $url_video;
                $denuncias->id_tdelitos = 8;
                $denuncias->id_hechos = $id_hechos;
                $denuncias->save();
                $id_denuncias = $denuncias->id_denuncias;

                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncias, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $denuncias->folio=$folio;
                $denuncias->update();

                if($request->input('tipo_persona')==0){
                     //empresas
                    $empresa = new Empresa;
                    $empresa-> razon_social= mb_strtoupper(request('razon_social_denunciante'), 'utf-8');
                    $empresa-> nom_representante= mb_strtoupper(request('nom_representante'), 'utf-8');
                    $empresa-> app_representante= mb_strtoupper(request('app_representante'), 'utf-8');
                    $empresa-> apm_representante= mb_strtoupper(request('apm_representante'), 'utf-8');
                    $empresa->id_denuncias=$id_denuncias;
                    $empresa->activo=1;
                    $empresa->save();
                }
                //persona denunciante
                $persona = new Persona;
                $persona->nom_persona = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
                $persona->app_persona = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                $persona->apm_persona = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                $persona->tipo_personas = 1;
                $persona->sexo = $request->input('genero_denunciante');
                $persona->edad = $request->input('edad_denunciante');
                $persona->telefono = $request->input('telefono_denunciante');
                $persona->correo = $request->input('correo_denunciante');
                $persona->media_filiacion = NULL;
                $persona->id_denuncias = $id_denuncias;
                $persona->id_interviniente = 2;
                $persona->activo = 1;
                $persona->save();
                //tabla de localizaciones
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];
                $persona->direcciones()->create($localizacion);



                if($request->input('tipo_persona_p')==1){
                    //persona inculpado
                    $persona = new Persona;
                    $persona->nom_persona = mb_strtoupper($request->input('nombre_probable'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('appat_probable'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apmat_probable'), 'utf-8');
                    $persona->tipo_personas = 3;
                    $persona->sexo = $request->input('genero_probable');
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;
                    $persona->save();
                }else{
                    //persona inculpado
                    $persona = new Persona;
                    $persona->nom_persona = "QRR";
                    $persona->app_persona =  "QRR";
                    $persona->apm_persona = "QRR";
                    $persona->tipo_personas = 3;
                    $persona->sexo = NULL;
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;
                    $persona->save();
                }

                //vehiculos
                $vehiculos->id_marca = $request->input('marca_vehiculo_denunciante');
                $vehiculos->id_submarca = $request->input('modelo_vehiculo_denunciante');
                $vehiculos->id_tvehiculos = $request->input('tipo_vehiculo_denunciante');
                $vehiculos->id_color = $request->input('color_vehiculo_denunciante');
                $vehiculos->placas = $request->input('placas_vehiculo_denunciante');
                $vehiculos->num_serie = $request->input('num_serie_vehiculo_denunciante');
                $vehiculos->motor = $request->input('num_motor');
                $vehiculos->id_denuncias = $id_denuncias;
                $vehiculos->id_aseguradora = $aseguradora_id;
                $vehiculos->activo = 1;
                $vehiculos->save();


                // informacion de localizaciones con hechos
                $localizacionh = [
                    "id_estados" => 15,
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);

                DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return $e->getMessage();
                }
            }

        Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
        return redirect()->route('finaliza_denuncia')
                        ->with('folio',$folio)
                        ->with('n_folio',$id_denuncias);
    }

    function combo_submarca(Request $request){
        $id_submarca = $request->post('id_marca');
        $submarcas = submarcas::where('id_marca','=',$id_submarca)->orderBy('submarca', 'asc')->get();
        return $submarcas;
        //return view ('denuncia_vehiculos.combo_submarca')->with('submarcas',$submarcas);
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show_modelov(Request $request)
    {
        if(isset($request->texto)){
            $modelos = Submarcas::whereid_marca($request->texto)->get();
            return response()->json(
                [
                    'lista' => $modelos,
                    'success' => true
                ]
            );
        }else{
            return response()->json(
                [
                    'lista' => [],
                    'success' => false
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
