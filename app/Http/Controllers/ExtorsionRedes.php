<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Extorsion;
use App\Models\Lugares;
use App\Models\Colonias;
use App\Models\Denuncia;
use App\Models\Persona;
use App\Models\Intervinientes;
use App\Models\Lugar;
use App\Models\TLugar;
use App\Models\Hechos;
use App\Models\Empresa;
use App\Http\Requests\GeneralRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class ExtorsionRedes extends Controller
{
    public function alta_extorsion(){

        $bienes=DB::select('select * from tipo_bienes');
        $identificacion=DB::Select('select * from tipo_identificacion');
        $interviniente=DB::select('select * from intervinientes');
        $estados=DB::select('select* from estados');
        $municipios=DB::select('select * from municipios');

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        $tlugar=DB::select('select * from t_lugares');
        return view('denuncias_extorsion.extorsion_redes_sociales')
                ->with('municipios',$municipios)
                ->with('municipios_hechos',$municipios_hechos)
                ->with('estados',$estados)
                ->with('interviniente',$interviniente)
                ->with('tlugar',$tlugar)
                ->with('identificacion',$identificacion)
                ->with('bienes',$bienes);
    }
    function combo(Request $request){
        $municipios = $request->get('id_municipios');
        $colonias = colonias::where('id_municipios','=',$municipios)->orderBy('colonia', 'asc')->get();
        return view ('denuncias_extorsion.combo')->with('colonias',$colonias);
    }
    public function guarda_extorsion(GeneralRequest $request)
    {


        try{
            $httpClient = new \GuzzleHttp\Client();
            $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

            //denunciante ----------------------------------------------------------------------------------
            //$contentBody['isVictima'] =  'true';
            $contentBody['name'] = "Product Demo";

            if ($request->input('tipo_persona') == 0) {

                $contentBody['isPersonaMoral'] = true;
                $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
                $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
                $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

            }else{
                $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
            }


            $contentBody['id_sexo'] = $request->input('genero_denunciante');
            $contentBody['edad'] = $request->input('edad_denunciante');
            $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
            $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
            $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
            $contentBody['codigoPostal'] = $request->input('cp_denunciante');
            $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
            $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
            $contentBody['telefono'] = $request->input('telefono_denunciante');
            $contentBody['correoElectronico'] = $request->input('correo_denunciante');
            //hechos ----------------------------------------------------------------------------------
            $hora = explode(":", $request->input('hora_hechos'));
            $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
            $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
            $contentBody['hechos']  = trim(mb_strtoupper($request->input('descripcion'), 'utf-8'));

            if ($request->input('isHechosViolencia') == 0) {
                $contentBody['isHechosViolencia'] =  'false';
            }else{
                $contentBody['isHechosViolencia'] =  'true';
            }

            $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
            $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
            $contentBody['estado_id'] =  15;
            $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
            $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
            $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
            $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
            $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
            $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
            //probables responsables ----------------------------------------------------------------------------------
            if($request->input('tipo_persona_p') == 1){
                $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
            }else{
                $nombre_pr = "QRR QRR QRR";
            }
            $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
            $contentBody['mediaFiliacion'] =  trim(mb_strtoupper($request->input('media_filiacion'), 'utf-8')); //media_filiacion
            //extorsion
            if (request('victima') == 0){

                $is_victima = false;
            }else
            {
                $is_victima = true;
            }

            $contentBody['isVictima'] = $is_victima;
            $contentBody['roboObjetos'] =  trim(mb_strtoupper($request->input('robo_objetos'), 'utf-8'));
            $contentBody['extorsionHora'] = $hora[0].":".$hora[1];
            $contentBody['extorsionFecha'] = $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
            $contentBody['medio_extorsion_id'] = request('id_textorsion');
            $contentBody['tipo_identificacion_id'] = request('id_tidentificacion');;
            $contentBody['otraIdentificacion'] = null;
            $contentBody['otroMedio'] = null;
            $contentBody['institucionReceptora'] = "FGJEM";
            $contentBody['tipo_extorsion_id'] = 5;
            $contentBody['otroTipoExtorsion'] = null;
            $contentBody['entrego_bien_id'] = request('id_tbien');
            $contentBody['bienqEntrego'] = request('bien_entregado');
            $contentBody['valorAproximadoBien'] = request('valor_bien');
            $contentBody['bancoCuenta'] = request('banco_cuenta');
            $contentBody['numeroCuenta'] = request('no_cuenta');
            $contentBody['tipo_telefono_nombre'] = null;

            $medio_extorsion = "Presencial";

            if (request('medio_extorsion') != null){
                $medio_extorsion = request('medio_extorsion');
            }

            if (request('medio_extorsion1') != null){
                $medio_extorsion = request('medio_extorsion');
            }

            if (request('medio_extorsion2') != null){
                $medio_extorsion = request('medio_extorsion');
            }

            $contentBody['medio_extorsion_nombre'] = $medio_extorsion;
            $contentBody['tipo_identificacion_nombre'] = request('identificacion');
            $contentBody['tipo_extorsion_nombre'] = null;
            $contentBody['entrego_bien_nombre'] = null;
            $contentBody['extorsion'] = true;
            $contentBody['isExtorsion'] = true;

            $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

            if($req->getBody()){

                $respuesta = json_decode($req->getBody(),true);
                $folio_pre = $respuesta['folio'];
            }

        } catch (\Exception $e) {
            DB::rollback();
            //return $e->getMessage();
            $folio_pre = "LOCAL";
        }



            DB::beginTransaction();
            try {

                $url = NULL;
                $url_video = NULL;

                //tabla de hechos
                $hechos = new Hechos;
                $hechos->hechos = mb_strtoupper(request('descripcion'), 'utf-8');
                $hechos->fecha_hechos = request('fecha_hechos');
                $hechos->hora_hechos = request('hora_hechos');
                $hechos->violencia = request('violencia');
                $hechos->robo_objetos = request('robo_objetos');
                $hechos->id_tlugares = request('id_tipolugar');
                $hechos->lugar_hallazgo = $request->input('lugar_hallazgo');
                $hechos->activo = 1;

                $hechos->save();
                $id_hechos = $hechos->id_hechos;

                if ($request->file('foto')) {
                    $imagenes = $request->file('foto')->store('public/imagenes');
                    $url = Storage::url($imagenes);
                }

                if ($request->file('video')) {
                    $videos = $request->file('video')->store('public/videos');
                    $url_video = Storage::url($videos);
                }

                //tabla de denuncias
                $denuncias= new Denuncia;
                $denuncias->denuncia_066 = request('denuncia_066');
                $denuncias->procesado = 0;

                $denuncias->fecha_denuncia  = date('Y/m/d');
                $denuncias->foto_denuncia = $url;
                $denuncias->video_denuncia = $url_video;
                $denuncias->id_tdelitos = 1;
                $denuncias->id_hechos = $id_hechos;
                $denuncias->activo = 1;
                $denuncias->save();
                $id_denuncias = $denuncias->id_denuncias;
                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncias, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $denuncias->folio=$folio;
                $denuncias->update();

                //tabla de personas
                $personas= new Persona;
                $personas->nom_persona= mb_strtoupper(request('nombre_denunciante'), 'utf-8') ;
                $personas->app_persona= mb_strtoupper(request('apellido_paterno'), 'utf-8');
                $personas->apm_persona= mb_strtoupper(request('apellido_materno'), 'utf-8');
                $personas->tipo_personas=1;
                    if($personas->tipo_personas == 0 ){
                        $empresa = new Empresa;
                        $empresa->razon_social = mb_strtoupper(request('razon_social_denunciante'), 'utf-8');
                        $empresa->nom_representante = mb_strtoupper(request('nom_representante'), 'utf-8');
                        $empresa->app_representante = mb_strtoupper(request('app_representante'), 'utf-8');
                        $empresa->apm_representante = mb_strtoupper(request('apm_representante'), 'utf-8');
                        $empresa->id_denuncias = $id_denuncias;
                        $empresa->activo = 1;
                        $empresa->save();
                    }
                $personas->sexo = request('genero_denunciante');
                $personas->edad = request('edad_denunciante');
                $personas->telefono = request('telefono_denunciante');
                $personas->correo = request('correo_denunciante');
                $personas->media_filiacion = NULL ;
                $personas->id_interviniente = 2;
                $personas->id_denuncias = $id_denuncias;
                $personas->activo = 1;
                $personas->save();
                $id_personas=$personas->id_personas;

                //tabla de localizaciones
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];
                $personas->direcciones()->create($localizacion);

                //persona responsable
                if ($request->input('tipo_persona_p') == 1) {
                    //persona inculpado
                    $persona = new Persona;
                    $persona->nom_persona = mb_strtoupper($request->input('nombre_probable'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('appat_probable'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apmat_probable'), 'utf-8');
                    $persona->tipo_personas = 3;
                    $persona->sexo = $request->input('genero_probable');
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;

                    $persona->save();
                } else {
                    //persona inculpado
                    $persona = new Persona;
                    $persona->nom_persona = "QRR";
                    $persona->app_persona =  "QRR";
                    $persona->apm_persona = "QRR";
                    $persona->tipo_personas = 3;
                    $persona->sexo = NULL;
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;

                    $persona->save();
                }
                //tabla de Extorsiones
                $extorsion=new Extorsion;
                $extorsion->victima=request('victima');
                $extorsion->extorsion_hora=request('hora_hechos');
                $extorsion->extorsion_fecha=request('fecha_hechos');
                $extorsion->id_textorsion=request('id_textorsion');
                $extorsion->id_mextorsion=request('id_textorsion');

                    switch ($extorsion->id_textorsion) {
                        case 1:
                            $extorsion->medio_extorsion = "";
                            break;
                        case 2:
                            $extorsion->medio_extorsion = request('medio_extorsion');
                            break;
                        case 3:
                            $extorsion->medio_extorsion = request('medio_extorsion1');
                            break;
                        case 4:
                            $extorsion->medio_extorsion = request('medio_extorsion2');
                            break;
                    }

                $extorsion->id_tidentificacion = request('id_tidentificacion');
                $extorsion->otra_identificacion = null;
                $extorsion->identificacion = request('identificacion');
                $extorsion->institucion_receptora = "FGJEM";
                $extorsion->otro_textorsion = null;
                $extorsion->otro_medio= null;
                $extorsion->id_tbien=request('id_tbien');
                    if ($extorsion->id_tbien != 1) {
                        $extorsion->bien_entregado = request('bien_entregado');
                        $extorsion->valor_bien = request('valor_bien');
                        $extorsion->banco_cuenta = request('banco_cuenta');
                        $extorsion->no_cuenta = request('no_cuenta');
                    }
                    else {
                        $extorsion->bien_entregado = null;
                        $extorsion->valor_bien = null;
                        $extorsion->banco_cuenta = null;
                        $extorsion->no_cuenta = null;
                    }

                $extorsion->activo=1;
                $extorsion->id_hechos=$id_hechos;
                $extorsion->id_denuncias=$id_denuncias;
                $extorsion->save();
                $id_extorsion = $extorsion->id_extorsiones;

                // informacion de localizaciones con hechos
                $localizacionh = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }

            Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
            return redirect()->route('finaliza_denuncia')
                                ->with('folio',$folio)
                                ->with('n_folio',$id_denuncias);
    }
}
