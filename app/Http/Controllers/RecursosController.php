<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Colonias;
use Illuminate\Support\Facades\DB;

class RecursosController extends Controller
{

    public function codigo_postal(Request $request)
    {
        $id = $request->input('id_colonia');

        $cp = DB::table('colonias')->where('id_colonias', $id)->first();

        //return $user->email;

        return str_pad($cp->codigo_postal, 5, "0", STR_PAD_LEFT);

    }

}
