<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Usuarios;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function login(){

        return view('login.login');
        
    }

    public function validar(LoginRequest $request)
    {
        $this->validate($request,[
            'usuario' => 'required',
            'password' => 'required'
        ]);

        //$passwordEnc = Hash::make($request->password);
        //$2y$10$LSqRUbDeB2Gc1L5K9dh8ROoojYfc9D5puSeDBLQpNOoYnEznWMeZO

        $usuarios = DB::table('usuarios')
        ->where('activo', '=', 1)
        ->get();
        
        $cuantos = count($usuarios);

        if($cuantos == 1 and Hash::check($request->password, $usuarios[0]->password))
        {

            Session::put('session_usuario',$usuarios[0]->nombre);
            Session::put('session_tipo',$usuarios[0]->tipo_usuario);

            return redirect()->route('principal');
            echo "acceso correcto";

        }else{

            Session::flash('error', 'Es necesario loguearse antes de continuar');
           return redirect()->route('consulta_denuncias');
        }
    }

    public function inicio(){

        return view('administracion.principal');

    }
}
