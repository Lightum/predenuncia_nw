<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GeneralRequest;
use App\Models\Hechos;
use App\Models\Lugar;
use App\Models\Persona;
use App\Models\Denuncia;
use App\Models\Empresa;
use App\Models\Colonias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\TLugar;
use App\Models\Intervinientes;

class DenunciaPersonaDesaparecidaController extends Controller
{

    function combo(Request $request){

        $municipios = $request->get('id_municipio');
        $colonias = colonias::where('id_municipio','=',$municipios)->get();

        return view ('denuncias_extorsion.combo')->with('colonias',$colonias);
    }
    public function create(){

        $estados=DB::select('SELECT * FROM estados');
        $municipios=DB::select('SELECT * FROM municipios');
        $interviniente=DB::select('SELECT * FROM intervinientes');
        $tlugar=DB::select('SELECT * FROM t_lugares');

        $municipios_hechos = DB::table('municipios')
        ->select('*')
        ->where('id_estados', '=', 15)
        ->orderBy('municipio', 'asc')
        ->get();

        return view('listado_denuncias.denuncia_pdesaparecida')
                    ->with('municipios',$municipios)
                    ->with('municipios_hechos',$municipios_hechos)
                    ->with('estados', $estados)
                    ->with('interviniente',$interviniente)
                    ->with('tlugar',$tlugar);

    }
    public function store(GeneralRequest $request){
        $url = NULL;
        $url_video = NULL;


        try {
            $httpClient = new \GuzzleHttp\Client();
            $api = "https://sigi-api-qa.fiscaliaedomex.gob.mx/sigi-io/api/denuncia-expres/web/save";

            //denunciante ----------------------------------------------------------------------------------
            $contentBody['isVictima'] =  'true';
            $contentBody['name'] = "Product Demo";

            if ($request->input('tipo_persona') == 0) {

                $contentBody['isPersonaMoral'] = true;
                $contentBody['razonSocial'] = mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                $contentBody['representanteNombre'] =mb_strtoupper($request->input('nom_representante'), 'utf-8');
                $contentBody['representantePaterno'] =  mb_strtoupper($request->input('app_representante'), 'utf-8');
                $contentBody['representanteMaterno'] = mb_strtoupper($request->input('apm_representante'), 'utf-8');

            }else{
                $contentBody['materno'] = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                $contentBody['paterno'] = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                $contentBody['nombre'] = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
            }


            $contentBody['id_sexo'] = $request->input('genero_denunciante');
            $contentBody['edad'] = $request->input('edad_denunciante');
            $contentBody['id_estado_denunciante'] =  $request->input('id_estado');
            $contentBody['id_municipio_denunciante'] =  $request->input('municipio_den');
            $contentBody['id_colonia_denunciante'] =  $request->input('colonia_den');
            $contentBody['codigoPostal'] = $request->input('cp_denunciante');
            $contentBody['calleDenunciante'] =  mb_strtoupper($request->input('calle_denunciante'), 'utf-8');
            $contentBody['noExtDenunciante'] =  $request->input('num_ext_denunciante');
            $contentBody['telefono'] = $request->input('telefono_denunciante');
            $contentBody['correoElectronico'] = $request->input('correo_denunciante');
            //hechos ----------------------------------------------------------------------------------
            $hora = explode(":", $request->input('hora_hechos'));
            $contentBody['hechosHora'] =  $hora[0].":".$hora[1];
            $contentBody['hechosFecha'] =  $request->input('fecha_hechos')."T"."00:00:00.604+00:00";
            $contentBody['hechos']  = trim(mb_strtoupper($request->input('descripcion'), 'utf-8'));

            if ($request->input('violencia') == 0) {
                $contentBody['isHechosViolencia'] =  'false';
            }else{
                $contentBody['isHechosViolencia'] =  'true';
            }

            $contentBody['tipo_lugar_id'] =  $request->input('lugar_hallazgo');
            $contentBody['lugar_hechos_id'] = $request->input('id_tipolugar');
            $contentBody['estado_id'] =  15;
            $contentBody['id_municipio_hechos'] = $request->input('municipio_hechos');
            $contentBody['id_colonia_hechos'] = $request->input('colonia_hechos');
            $contentBody['id_localidad_hechos'] = $request->input('localidad_hechos');
            $contentBody['calleHechos'] =  mb_strtoupper($request->input('calle_hechos'), 'utf-8');
            $contentBody['hechosNumeroExt'] = $request->input('num_ext_hechos');
            $contentBody['hechosNumeroInt'] = $request->input('num_int_hechos');
            //probables responsables ----------------------------------------------------------------------------------
            if($request->input('tipo_persona_p') == 1){
                $nombre_pr = $request->input('nombre_probable')." ".$request->input('appat_probable')." ".$request->input('apmat_probable');
                $contentBody['mediaFiliacion'] =   trim(mb_strtoupper($request->input('media_filiacion'), 'utf-8')); //media_filiacion
            }else{
                $nombre_pr = "QRR QRR QRR";
            }
            $contentBody['probablesResponsables'] = mb_strtoupper($nombre_pr, 'utf-8');
            //objetos
            $contentBody['roboObjetos'] =  trim(mb_strtoupper($request->input('robo_objetos'), 'utf-8'));
            $req = $httpClient->post($api, ['verify' => false, 'body'=>json_encode($contentBody), 'auth' => ['DEN_EXP_USR', 'FGJ_D3N_3X5_U5R$'], 'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']]);

            if($req->getBody()){
                $respuesta = json_decode($req->getBody(),true);
                $folio_pre = $respuesta['folio'];
            }

        } catch (\Exception $e) {
            DB::rollback();
            //return $e->getMessage();
            $folio_pre = "LOCAL";
        }

        if($folio_pre!=NULL){

            DB::beginTransaction();
            try {

                if ($request->file('foto')) {
                    $imagenes = $request->file('foto')->store('public/imagenes');
                    $url = Storage::url($imagenes);
                }

                if ($request->file('video')) {
                    $videos = $request->file('video')->store('public/videos');
                    $url_video = Storage::url($videos);
                }

                //HECHOS
                $hechos = new Hechos;
                $hechos->hechos= mb_strtoupper($request->input('descripcion'), 'utf-8');
                $hechos->hora_hechos=$request->input('hora_hechos');
                $hechos->fecha_hechos=$request->input('fecha_hechos');
                $hechos->violencia=$request->input('violencia');
                $hechos->robo_objetos="PERSONA DESAPARECIDA";
                $hechos->id_tlugares=$request->input('id_tipolugar');
                $hechos->lugar_hallazgo = $request->input('lugar_hallazgo');
                $hechos->activo="1";

                $hechos->save();
                $id_hechos = $hechos->id_hechos;

                //DENUNCIAS
                $denuncias = new Denuncia;
                $denuncias->denuncia_066 = $request->input('denuncia_066');
                $denuncias->procesado = 0;
                $denuncias->fecha_denuncia= date('Y-m-d');
                $denuncias->foto_denuncia  = $url; /// ver como subir foto
                $denuncias->video_denuncia  = $url_video; /// ver como subir video
                $denuncias->id_tdelitos = 9;
                $denuncias->id_hechos = $id_hechos;
                $denuncias->activo="1";
                $denuncias->save();
                $id_denuncias = $denuncias->id_denuncias;
                if($folio_pre!="LOCAL"){
                    $folio = $folio_pre;
                }else{
                    $n_folio = str_pad($id_denuncias, 4, "0", STR_PAD_LEFT);
                    $folio = "LOCAL".$n_folio;
                }
                $denuncias->folio=$folio;
                $denuncias->update();

                //PERSONA
                if($request->input('tipo_persona') == 0) {
                //empresas
                    $empresas =  new Empresa;
                    $empresas->razon_social= mb_strtoupper($request->input('razon_social_denunciante'), 'utf-8');
                    $empresas->nom_representante= mb_strtoupper($request->input('nom_representante'), 'utf-8');
                    $empresas->app_representante= mb_strtoupper($request->input('app_representante'), 'utf-8');
                    $empresas->apm_representante= mb_strtoupper($request->input('apm_representante'), 'utf-8');
                    $empresas->id_denuncias=$id_denuncias;
                    $empresas->activo = 1;

                    $empresas->save();
                }

                    $persona = new Persona;
                    $persona->nom_persona = mb_strtoupper($request->input('nombre_denunciante'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('apellido_paterno'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apellido_materno'), 'utf-8');
                    $persona->sexo = $request->input('genero_denunciante');
                    $persona->edad = $request->input('edad_denunciante');
                    $persona->telefono = $request->input('telefono_denunciante');
                    $persona->correo = $request->input('correo_denunciante');
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente= 2;
                    $persona->tipo_personas= 1;
                    $persona->activo="1";

                    $persona->save();


                //tabla de localizaciones
                $localizacion = [
                    "id_estados" => $request->input('id_estado'),
                    "id_municipios" => $request->input('municipio_den'),
                    "id_colonias" => $request->input('colonia_den'),
                    "calle" => mb_strtoupper($request->input('calle_denunciante'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_denunciante'),
                    "num_ext" => $request->input('num_ext_denunciante'),
                    "num_int" => $request->input('num_int_denunciante'),
                    "activo" => 1,
                ];

                $persona->direcciones()->create($localizacion);

                if($request->input('tipo_persona_p') == 1){
                    //inculpado
                    $persona = new Persona;
                    $persona->nom_persona = mb_strtoupper($request->input('nombre_probable'), 'utf-8');
                    $persona->app_persona = mb_strtoupper($request->input('appat_probable'), 'utf-8');
                    $persona->apm_persona = mb_strtoupper($request->input('apmat_probable'), 'utf-8');
                    $persona->tipo_personas = 1;
                    $persona->sexo = $request->input('genero_probable');
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = mb_strtoupper($request->input('media_filiacion'), 'utf-8'); //media_filiacion
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;

                    $persona->save();
                }
                else{
                    //inculpado
                    $persona = new Persona;
                    $persona->nom_persona = "QRR";
                    $persona->app_persona =  "QRR";
                    $persona->apm_persona = "QRR";
                    $persona->tipo_personas = 1;
                    $persona->sexo = NULL;
                    $persona->edad = NULL;
                    $persona->telefono = NULL;
                    $persona->correo = NULL;
                    $persona->media_filiacion = NULL;
                    $persona->id_denuncias = $id_denuncias;
                    $persona->id_interviniente = 3;
                    $persona->activo = 1;

                    $persona->save();
                }

                    //persona desaparecida
                $personadesaparecida = new persona;
                $personadesaparecida->nom_persona= mb_strtoupper($request->input('nombre_desaparecido'), 'utf-8');
                $personadesaparecida->app_persona= mb_strtoupper($request->input('appat_per_des'), 'utf-8');
                $personadesaparecida->apm_persona= mb_strtoupper($request->input('apmat_per_des'), 'utf-8');
                $personadesaparecida->sexo=$request->input('genero_persona');
                $personadesaparecida->edad=$request->input('edad_per_des');
                $personadesaparecida->telefono = NULL;
                $personadesaparecida->correo = NULL;
                $personadesaparecida->media_filiacion = NULL;
                $personadesaparecida->id_denuncias= $id_denuncias;
                $personadesaparecida->id_interviniente= 1;
                $personadesaparecida->tipo_personas= 1;
                $personadesaparecida->activo="1";

                $personadesaparecida->save();


                // informacion de localizaciones con hechos
                $localizacionh = [
                    "id_estados" => 15,
                    "id_municipios" => $request->input('municipio_hechos'),
                    "id_colonias" => $request->input('colonia_hechos'),
                    "localidad" => $request->input('localidad_hechos'),
                    "calle" => mb_strtoupper($request->input('calle_hechos'), 'utf-8'),
                    "codigo_postal" => $request->input('cp_hechos'),
                    "num_ext" => $request->input('num_ext_hechos'),
                    "num_int" => $request->input('num_int_hechos'),
                    "activo" => 1,
                ];
                $hechos->direcciones()->create($localizacionh);

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }

            Alert::html('FGJEM', 'La presente predenuncia tendrá que ser ratificada por usted ante el Ministerio Público, para lo cual nos comunicaremos a la brevedad posible por teléfono o correo electrónico; se recomienda revisar sus datos para poderlo contactar y mencionarle la agencia a la que deberá acudir.', 'info');
            return redirect()->route('finaliza_denuncia')
                                ->with('folio',$folio)
                            ->with('n_folio',$id_denuncias);

        }
    }
}
