<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class RoboCasaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_denunciante'      => ['required', 'regex:/^[a-zA-Z\sñÑ.]+$/u', 'min:1', 'max:100'],
			'apellido_paterno'        => ['sometimes', 'regex:/^[a-zA-Z\sñÑ]+$/u', 'min:1', 'max:100'],
            'apellido_materno'        => ['nullable', 'regex:/^[a-zA-Z\sñÑ]+$/u', 'min:1', 'max:100'],
            'nombre_probable'         => ['regex:/^[a-zA-Z\sñÑ.]+$/u'],
            'appat_probable'          => ['regex:/^[a-zA-Z\sñÑ]+$/u'],
            'apmat_probable'          => ['regex:/^[a-zA-Z\sñÑ]+$/u'],
            'genero_denunciante'      => ['required'],
            'edad_denunciante'        => ['numeric', 'between:1,120'],
			'calle_denunciante'       => ['required', 'min:1', 'max:100'],
            'calle'                   => ['required', 'min:1', 'max:100'],
            'num_ext_denunciante'     => ['required', 'regex:/^[a-zA-Z\-0-9\s]+$/u'],
            'municipio_den'           => ['required'],
            'colonia_den'             => ['required'],
            'localidad_denunciante'   => ['required'],
            'cp_denunciante'          => ['required', 'digits:5'],
            'telefono_denunciante'    => ['required', 'digits:10'],
            'denuncia_066'            => ['required'],
            'calle_hechos'            => ['required', 'min:1', 'max:100'],
            'municipio_hechos'        => ['required'],
            'colonia_hechos'          => ['required'],
            'localidad_hechos'        => ['required'],
            'descripcion'             => ['required', 'min:150', 'max:4000'],
            'foto'                    => ['nullable','mimes:jpeg,png,jpg,gif,svg,png', 'max:15000'],
            'video'                   => ['nullable', 'mimes:mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv','max:40000'],
        ];
    }

    public function messages()
    {
        return [
            'nombre_denunciante.required'   => 'El :attribute es obligatorio.',
            'nombre_denunciante.regex'      => 'El :attribute debe contener solo letras',
            'nombre_denunciante.min'        => 'El :attribute debe contener mas de una letra.',
            'nombre_denunciante.max'        => 'El :attribute debe contener maximo 30 letras.',
            'apellido_paterno.regex'        => 'El :attribute es obligatorio.',
            'apellido_paterno.min'          => 'El :attribute debe contener mas de una letra.',
            'apellido_paterno.max'          => 'El :attribute debe contener maximo 30 letras.',
            'apellido_materno.min'          => 'El :attribute debe contener mas de una letra',
            'apellido_materno.max'          => 'El :attribute debe contener maximo 30 letras',
            'apellido_materno.regex'        => 'El :attribute debe contener solo letras',
            'nombre_probable.regex'         => 'El :attribute debe contener solo letras',
            'appat_probable.regex'          => 'El :attribute debe contener solo letras',
            'apmat_probable.regex'          => 'El :attribute debe contener solo letras',
            'genero_denunciante.required'   => 'El :attribute es obligatorio',
            'genero_persona.required'       => 'El :attribute es obligatorio',
            'edad_denunciante.numeric'      => 'El :attribute debe contener un numero',
            'edad_denunciante.between'      => 'La :attribute no puede ser mayor a 120',
            'calle_denunciante.required'    => 'El :attribute es obligatorio.',
            'calle_denunciante.min'         => 'El :attribute debe contener mas de una letra.',
            'calle_denunciante.max'         => 'El :attribute debe contener maximo 30 letras',
            'calle.required'                => 'El :attribute es obligatorio.',
            'calle.min'                     => 'El :attribute debe contener mas de una letra.',
            'calle.max'                     => 'El :attribute debe contener maximo 30 letras',
            'num_ext_denunciante.required'                => 'El :attribute es obligatorio',
            'municipio_den.required'        => 'El :attribute es obligatorio',
            'colonia_den.required'          => 'El :attribute es obligatorio',
            'localidad_denunciante.required'=> 'El :attribute es obligatorio',
            'cp_denunciante.required'       => 'El :attribute es obligatorio.',
            'cp_denunciante.digits'         => 'El :attribute debe contener 5 digitos',
            'telefono_denunciante.required' => 'El :attribute es obligatorio',
            'telefono_denunciante.digits'   => 'El :attribute debe contener 10 digitos',
            'denuncia_066'                  => 'El :attribute es obligatorio',
            'calle_hechos.required'         => 'La :attribute es obligatorio.',
            'calle_hechos.min'              => 'La :attribute debe contener mas de un caracter.',
            'calle_hechos.max'              => 'La :attribute debe contener maximo 100 caracteres',
            'descripcion.required'          => 'La :attribute es obligatorio.',
            'descripcion.min'               => 'La :attribute debe contener minimo 150  caracteres.',
            'descripcion.max'               => 'La :attribute debe contener máximo 4000 caracteres.',
            'municipio_hechos.required'     => 'El :attribite es obligatorio',
            'colonia_hechos.required'       => 'El :attribute es obligatorio',
            'localidad_hechos.required'     => 'Falta rellenar la localidad de los hechos',
            'foto'                          => 'La foto debe ser jpeg,png,jpg,gif,svg,png y no debe superar los 15 MB',
            'video'                         => 'La foto debe ser mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv y no debe superar los 40 MB'
        ];
    }

    public function attributes() {
        return [
            'nombre_denunciante'        => 'Nombre del denunciante',
            'apellido_paterno'          => 'Apellido paterno del denunciante',
            'apellido_materno'          => 'Apellido materno del denunciante',
            'genero_denunciante'        => 'Genero del denunciante',
            'edad_denunciante'          => 'Edad del denunciante',
            'nombre_probable'           => 'Nombre del probable responsable',
            'appat_probable'            => 'Apellido paterno del probable responsable',
            'apmat_probable'            => 'apellido materno del probable responsable',
            'nombre_desaparecido'       => 'Nombre de la persona desaparecida',
            'appat_per_des'             => 'Apellido paterno de la persona desaparecida',
            'apmat_per_des'             => 'Apellido materno de la persona desaparecida',
            'genero_denunciante'        => 'Genero del denunciante',
            'edad_denunciante'          => 'Edad del denunciante',
            'calle_denunciante'         => 'Calle del denunciante',
            'municipio_den'             => 'Municipio del denunciante',
            'colonia_den'               => 'Colonia del denunciante',
            'localidad_denunciante'     => 'Localidad del denunciante',
            'cp_denunciante'            => 'Codigo postal del denunciante',
            'telefono_denunciante'      => 'Telefono del denunciante',
            'correo_denunciante'        => 'Correo del denunciante',
            'calle'                     => 'La calle del domicilio afectado',
            'n_ext'                     => 'El numero exterior del domicilio afectado',
            'calle_hechos'              => 'Calle de los hechos',
            'descripcion'               => 'Descripción de los hechos',
            'municipio_hechos'          => 'Municipio de los hechos',
            'colonia_hechos'            => 'Colonia de los hechos',
            'localidad_hechos'          => 'localidad de los hechos',
            'fecha_hechos'              => 'Fecha de los hechos',
            'cp_hechos'                 => 'codigo postal de los hechos',
            'hora_hechos'               => 'hora de los hechos',
            'foto'                      => 'Foto adjunta en la denuncia',
            'video'                     => 'Video adjunto en la denuncia',
            'denuncia_066'              => 'Denuncia 066',
        ];
    }

    public function withValidator($validator)
    {
         $validator->after(function ($validator){
            if ($this->fecha_hechos > Carbon::now()->toDateString()){
            // if ($this->fecha_hechos > Carbon::now()){
               $validator->errors()->add('fecha invalida', 'la fecha de los hechos no puede ser mayor a la fecha actual');
             }

             if ($this->fecha_hechos == Carbon::now()->toDateString() && $this->hora_hechos > Carbon::now()->toTimeString()){
                 $validator->errors()->add('hora_invalida', 'la hora de los hechos no puede ser  mayor a la hora actual');
             }
         });
    }
}
