<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExtorsionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'nombre_denunciante'  => 'required|alpha|min:1|max:100',
            'apellido_paterno'  => 'sometimes|alpha|min:1|max:100',
            'apellido_materno'  => 'nullable|alpha|min:1|max:100',
            'genero_denunciante'  => 'required',
            'nom_representante' => 'required'
            'calle_denunciante'  => 'required|min:1|max:100',
            'num_ext_denunciante'     => ['required', 'regex:/^[a-zA-Z\-0-9\s]+$/u'],
            'cp_denunciante'  => 'required|min:1|max:5',
            'calle_hechos'  => 'required|min:1|max:100',
            'descripcion'  => 'required|string|min:150|max:4000',
            'telefono_denunciante'  => 'required|integer',
            'fecha_hechos' => 'required',
            'edad_denunciante' => 'required|integer',
            'correo_denunciante' => 'required|email',
            'foto'=> 'nullable|mimes:jpeg,png,jpg,gif,svg,png|max:3000',
            'video'=> 'nullable|mimes:mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv|max:10000',
            'municipio_den' => 'required',
            'colonia_den' => 'required',
            'denuncia_066' => 'required',
            'municipio_hechos' => 'required',
            'colonia_hechos' => 'required',
            'calle_hechos' => 'required|string',
            'cp_hechos' => 'required',
            'hora_hechos' => 'required',

         ];
    }

    public function messages()
    {
         return [
                'nombre_denunciante.required'   => 'El :attribute es obligatorio.',
                'nombre_denunciante.min'        => 'El :attribute debe contener mas de una letra.',
                'nombre_denunciante.max'        => 'El :attribute debe contener max 100 letras.',
                'nombre_denunciante.alpha'        => 'El :attribute debe contener Solo letras.',
                'apellido_paterno.required'   => 'El :attribute es obligatorio.',
                'apellido_paterno.min'        => 'El :attribute debe contener mas de una letra.',
                'apellido_paterno.max'        => 'El :attribute debe contener max 30 letras.',
                'apellido_paterno.alpha'        => 'El :attribute debe contener Solo letras.',
                'apellido_materno.alpha'        => 'El :attribute debe contener Solo letras.',
                'apellido_materno.min'        => 'El :attribute debe contener mas de una letra.',
                'nom_representante.regex'       => 'El :attribute debe contener solo letras',
                'nom_representante.min'         => 'El :attribute debe contener mas de una letra.',
                'nom_representante.max'         => 'El :attribute debe contener maximo 100 letras.',
                'genero_denunciante.required'   => 'El :attribute es obligatorio.',
                'calle_denunciante.required'    => 'La :attribute es obligatorio.',
                'calle_denunciante.min' => 'El :attribute debe contener mas de una letra.',
                'cp_denunciante.required'    => 'El :attribute es obligatorio.',
                'cp_denunciante.min' => 'El :attribute debe contener mas de un digito.',
                'calle_hechos.required'    => 'La :attribute es obligatorio.',
                'descripcion.required'    => 'La :attribute es obligatorio.',
                'descripcion.min' => 'La :attribute debe contener mas de un digito.',
                'descripcion.max' => 'La :attribute debe contener maximo 150 caracteres.',           
                'telefono_denunciante.required'   => 'El :attribute es obligatorio.',
                'fecha_hechos.required'   => 'La :attribute es obligatorio.',
                'edad_denunciante.required'   => 'El :attribute es obligatorio.',
                'correo_denunciante.required'   => 'El :attribute es obligatorio.',
                'foto.mimes' => 'mimes:jpeg,png,jpg,gif,svg,png',
                'video.mimes'=> 'mimes:mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv',
                'municipio_den.required'   => 'El :attribute es obligatorio.',
                'colonia_den.required'   => 'La :attribute es obligatorio.',
                'denuncia_066.required'   => 'La :attribute es obligatorio.',
                'municipio_hechos.required'   => 'La :attribute es obligatorio.',
                'colonia_hechos.required'   => 'La :attribute es obligatorio.',
                'calle_hechos.required'   => 'La :attribute es obligatorio.',
                'calle_hechos.string'   => 'La :attribute debe ser texto.',
                'cp_hechos.required'   => 'La :attribute es obligatorio.',
                'hora_hechos.required'   => 'La :attribute es obligatorio.',
        
                    
         ];
    }
    public function attributes()
    {
         return [
               'nombre_denunciante'  => 'Nombre del denunciante',
               'apellido_paterno'  => 'Apellido paterno del denunciante',
               'apellido_materno'  => 'Apellido materno del denunciante',
               'genero_denunciante'  => 'Genero del denunciante',
               'calle_denunciante'  => 'Calle del denunciante',
               'cp_denunciante'  => 'Codigo postal del denunciante',
               'calle_hechos'  => 'Calle de los hechos',
               'descripcion'  => 'Descripción de los hechos',
               'telefono_denunciante'  => 'Telefono del denunciante',
               'fecha_hechos' => 'Fecha de los hechos',
               'edad_denunciante' => 'Edad del denunciante',
               'correo_denunciante' => 'Correo del denunciante',
               'foto'=> 'Foto adjunta en la denuncia',
               'video'=> 'Video adjunto en la denuncia',
               'municipio_den' => 'Municipio del denunciante',
               'colonia_den' => 'Colonia del denunciante',
               'denuncia_066' => 'Denuncia 066',
               'colonia_hechos' => 'Colonia de los hechos',
               'calle_hechos' => 'Calle de los hechos',
               'cp_hechos' => 'codigo postal de los hechos',
               'hora_hechos' => 'required',
               'nom_representante'         => 'Nombre de representante',
               'nom_representante'         => 'Nombre de representante',
               'app_representante'         =>'Apellido Paterno del Representante',
               'apm_representante'         =>'Apellido Materno del Representante'
         ];
    }
}
