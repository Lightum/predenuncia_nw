<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use Carbon\Carbon;

class GeneralRequest extends FormRequest
{
/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules = array(
            'razon_social_denunciante'=> ['nullable', 'min:1', 'max:30'],
            'nom_representante'       => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'app_representante'       => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'apm_representante'       => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'nombre_denunciante'      => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'apellido_paterno'        => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'apellido_materno'        => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:100'],
            'nombre_probable'         => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u'],
            'appat_probable'          => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u'],
            'apmat_probable'          => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u'],
            'genero_denunciante'      => ['required'],
            'edad_denunciante'        => ['numeric', 'between:1,120'],
            'calle_denunciante'       => ['required', 'min:1', 'max:100'],
            'municipio_den'           => ['required'],
            'colonia_den'             => ['required'],
            'cp_denunciante'          => ['required', 'digits:5'],
            'num_ext_denunciante'     => ['required', 'regex:/^[a-zA-Z\-0-9\s]+$/u'],
            'telefono_denunciante'    => ['required', 'digits:10'],
            'denuncia_066'            => ['required'],
            'calle_hechos'            => ['required', 'min:1', 'max:100'],
            'municipio_hechos'        => ['required'],
            'colonia_hechos'          => ['required'],
            'descripcion'             => ['required', 'min:150', 'max:4000'],
            'foto'                    => ['nullable','mimes:jpeg,png,jpg,gif,svg,png', 'max:15000'],
            'video'                   => ['nullable', 'mimes:mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv','max:40000']
        );
        //datos de personas desaparecida
        if($this->has('nombre_desaparecido')){
            $rules_desaparecido = array(
                'nombre_desaparecido'     => ['regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30', 'required'],
                'appat_per_des'           => ['regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30', 'sometimes'],
                'apmat_per_des'           => ['regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30', 'nullable'],
                'edad_per_des'            => ['numeric','between:1,120']
            );

            $rules = array_merge($rules, $rules_desaparecido);
        }
        //datos de vehiculos
        if($this->has('marca_vehiculo_denunciante')){
            $rules_vehiculos = array(
                'marca_vehiculo_denunciante'     => 'required',
                'modelo_vehiculo_denunciante'    => 'required',
                'tipo_vehiculo_denunciante'      => 'required',
                'color_vehiculo_denunciante'     => 'required',
                'seguro'                         => 'required',
                'placas_vehiculo_denunciante'    => 'required',
                'numero_serie'                   => 'required',
                'num_motor'                      => 'required',
                'num_serie_vehiculo_denunciante' => 'required'
            );

            $rules = array_merge($rules, $rules_vehiculos);
        }

        //datos de Homicidio
        if($this->has('nombre_victima')){
            $rules_victima = array(
                'nombre_victima' => 'required',
                'app_victima' => 'required',
                'apm_victima' => 'required',
                'genero_victima' => 'required',
                'edad_victima' => 'required',
             );

            $rules = array_merge($rules, $rules_victima);
        }

         //datos de extorsion
         if($this->has('id_textorsion')){
            $rules_extorsion = array(
                'id_tidentificacion' => ['required'],
                'identificacion'     => ['required'],
                'victima'            => ['required'],
                'robo_objetos'       => ['required'],
            );

            $rules = array_merge($rules, $rules_extorsion);
        }

        if($this->has('nombre_sec')){
            $rules_secuestro = array(
                'nombre_sec'              => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30'],
			    'appat_per_sec'           => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30'],
                'apmat_per_sec'           => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:1', 'max:30'],
                'edad_per_sec'            => ['nullable', 'numeric', 'between:1,120'],
                'telefono_victima'        => ['nullable', 'digits:10'],
            );

            $rules = array_merge($rules, $rules_secuestro);
        }




        return $rules;
    }

    public function messages()
    {
        return [
            'razon_social_denunciante.min'  => 'El :attribute debe contener mas de una letra.',
            'razon_social_denunciante.max'  => 'El :attribute debe contener maximo 100 letras.',
            'nom_representante.regex'       => 'El :attribute debe contener solo letras',
            'nom_representante.min'         => 'El :attribute debe contener mas de una letra.',
            'nom_representante.max'         => 'El :attribute debe contener maximo 100 letras.',
            'app_representante.regex'       => 'El :attribute debe contener solo letras.',
            'app_representante.min'         => 'El :attribute debe contener mas de una letra.',
            'app_representante.max'         => 'El :attribute debe contener maximo 100 letras.',
            'apm_representante.regex'       => 'El :attribute debe contener solo letras',
            'apm_representante.min'         => 'El :attribute debe contener mas de una letra',
            'apm_representante.max'         => 'El :attribute debe contener maximo 100 letras',
            'nombre_denunciante.regex'      => 'El :attribute debe contener solo letras',
            'nombre_denunciante.min'        => 'El :attribute debe contener mas de una letra.',
            'nombre_denunciante.max'        => 'El :attribute debe contener maximo 100 letras.',
            'apellido_paterno.regex'        => 'El :attribute debe contener solo letras.',
            'apellido_paterno.min'          => 'El :attribute debe contener mas de una letra.',
            'apellido_paterno.max'          => 'El :attribute debe contener maximo 100 letras.',
            'apellido_materno.min'          => 'El :attribute debe contener mas de una letra',
            'apellido_materno.max'          => 'El :attribute debe contener maximo 100 letras',
            'apellido_materno.regex'        => 'El :attribute debe contener solo letras',
            'nombre_probable.regex'         => 'El :attribute debe contener solo letras',
            'appat_probable.regex'          => 'El :attribute debe contener solo letras',
            'apmat_probable.regex'          => 'El :attribute debe contener solo letras',
            'nombre_desaparecido.required'  => 'El :attribute es obligatorio.',
            'nombre_desaparecido.regex'     => 'El :attribute debe contener solo letras',
            'nombre_desaparecido.min'       => 'El :attribute debe contener mas de una letra.',
            'nombre_desaparecido.max'       => 'El :attribute debe contener maximo 30 letras.',
            'appat_per_des.regex'           => 'El :attribute es obligatorio.',
            'appat_per_des.min'             => 'El :attribute debe contener mas de una letra.',
            'appat_per_des.max'             => 'El :attribute debe contener maximo 30 letras.',
            'apmat_per_des.min'             => 'El :attribute debe contener mas de una letra',
            'apmat_per_des.max'             => 'El :attribute debe contener maximo 30 letras',
            'apmat_per_des.regex'           => 'El :attribute debe contener solo letras',
            'nombre_sec.required'           => 'El :attribute es obligatorio.',
            'nombre_sec.regex'              => 'El :attribute debe contener solo letras',
            'nombre_sec.min'                => 'El :attribute debe contener mas de una letra.',
            'nombre_sec.max'                => 'El :attribute debe contener maximo 30 letras.',
            'appat_per_sec.regex'           => 'El :attribute es obligatorio.',
            'appat_per_sec.min'             => 'El :attribute debe contener mas de una letra.',
            'appat_per_sec.max'             => 'El :attribute debe contener maximo 30 letras.',
            'apmat_per_sec.min'             => 'El :attribute debe contener mas de una letra',
            'apmat_per_sec.max'             => 'El :attribute debe contener maximo 30 letras',
            'apmat_per_sec.regex'           => 'El :attribute debe contener solo letras',
            'genero_denunciante.required'   => 'El :attribute es obligatorio',
            'genero_persona.required'       => 'El :attribute es obligatorio',
            'edad_denunciante.numeric'      => 'El :attribute debe contener un numero',
            'edad_denunciante.between'      => 'La :attribute no puede ser mayor a 120',
            'edad_per_des.numeric'          => 'El :attribute debe contener un numero',
            'edad_per_des.between'          => 'La edad de la persona desaparecida no puede ser mayor a 120',
            'edad_per_sec.numeric'          => 'El :attribute debe contener un numero',
            'edad_per_sec.between'          => 'La :attribute no puede ser mayor a 120',
            'num_ext_denunciante.regex'     => 'El :attribute no cumple con los requisitos',
            'calle_denunciante.required'    => 'La :attribute es obligatorio.',
            'calle_denunciante.min'         => 'La :attribute debe contener mas de una letra.',
            'calle_denunciante.max'         => 'La :attribute debe contener maximo 100 letras',
            'municipio_den.required'        => 'El :attribute es obligatorio',
            'colonia_den.required'          => 'El :attribute es obligatorio',
            'cp_denunciante.required'       => 'El :attribute es obligatorio.',
            'cp_denunciante.digits'         => 'El :attribute debe contener 5 digitos',
            'telefono_denunciante.required' => 'El :attribute es obligatorio',
            'telefono_denunciante.digits'   => 'El :attribute debe contener 10 digitos',
            'telefono_victima.digits'       => 'El :attribute debe contener 10 digitos',
            'denuncia_066'                  => 'El :attribute es obligatorio',
            'calle_hechos.required'         => 'La :attribute es obligatorio.',
            'calle_hechos.min'              => 'La :attribute debe contener mas de un caracter.',
            'calle_hechos.max'              => 'La :attribute debe contener maximo 100 caracteres',
            'descripcion.required'          => 'La :attribute es obligatorio.',
            'descripcion.min'               => 'La :attribute debe contener minimo 150  caracteres.',
            'descripcion.max'               => 'La :attribute debe contener máximo 4000 caracteres.',
            'municipio_hechos.required'     => 'El :attribute es obligatorio',
            'colonia_hechos.required'       => 'El :attribute es obligatorio',
            'foto'                          => 'La foto debe ser jpeg,png,jpg,gif,svg,png y no debe superar los 15 MB',
            'video'                         => 'La foto debe ser mp4,mpeg,webm,avi,3gp,amv,m4v,ogg,ogv,flv,mkv y no debe superar los 40 MB',
            'seguro'                        => 'Por favor, Especifique si cuenta con seguro'
        ];
    }

    public function attributes() {
        return [
            'nombre_denunciante'        => 'Nombre del denunciante',
            'apellido_paterno'          => 'Apellido paterno del denunciante',
            'apellido_materno'          => 'Apellido materno del denunciante',
            'genero_denunciante'        => 'Genero del denunciante',
            'edad_denunciante'          => 'Edad del denunciante',
            'nombre_probable'           => 'Nombre del probable responsable',
            'appat_probable'            => 'Apellido paterno del probable responsable',
            'apmat_probable'            => 'apellido materno del probable responsable',
            'nombre_desaparecido'       => 'Nombre de la persona desaparecida',
            'appat_per_des'             => 'Apellido paterno de la persona desaparecida',
            'apmat_per_des'             => 'Apellido materno de la persona desaparecida',
            'nombre_sec'                => 'Nombre de la persona secuestrada',
            'appat_per_sec'             => 'Apellido paterno de la persona secuestrada',
            'apmat_per_sec'             => 'Apellido materno de la persona secuestrada',
            'genero_denunciante'        => 'Genero del denunciante',
            'edad_denunciante'          => 'Edad del denunciante',
            'edad_per_sec'              => 'Edad de la persona secuestrada',
            'calle_denunciante'         => 'Calle del denunciante',
            'municipio_den'             => 'Municipio del denunciante',
            'num_ext_denunciante'       => 'El número exterior del denunciante es requerido',
            'colonia_den'               => 'Colonia del denunciante',
            'cp_denunciante'            => 'Codigo postal del denunciante',
            'telefono_denunciante'      => 'Telefono del denunciante',
            'telefono_victima'          => 'Telefono de la persona secuestrada',
            'correo_denunciante'        => 'Correo del denunciante',
            'calle_hechos'              => 'Calle de los hechos',
            'descripcion'               => 'Descripción de los hechos',
            'municipio_hechos'          => 'Municipio de los hechos',
            'colonia_hechos'            => 'Colonia de los hechos',
            'localidad_hechos'          => 'localidad de los hechos',
            'fecha_hechos'              => 'Fecha de los hechos',
            'cp_hechos'                 => 'codigo postal de los hechos',
            'hora_hechos'               => 'hora de los hechos',
            'foto'                      => 'Foto adjunta en la denuncia',
            'video'                     => 'Video adjunto en la denuncia',
            'denuncia_066'              => 'Denuncia 066',
            'nom_representante'         => 'Nombre de representante',
            'app_representante'         =>'Apellido Paterno del Representante',
            'apm_representante'         =>'Apellido Materno del Representante'
        ];
    }

    public function withValidator($validator)
    {
         $validator->after(function ($validator){
            if ($this->fecha_hechos > Carbon::now()->toDateString()){
            // if ($this->fecha_hechos > Carbon::now()){
               $validator->errors()->add('fecha invalida', 'la fecha de los hechos no puede ser mayor a la fecha actual');
             }

             if ($this->fecha_hechos == Carbon::now()->toDateString() && $this->hora_hechos > Carbon::now()->toTimeString()){
                 $validator->errors()->add('hora_invalida', 'la hora de los hechos no puede ser  mayor a la hora actual');
             }
         });
    }
}
