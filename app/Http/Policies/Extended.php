<?php 

namespace App\Http\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;
use Spatie\Csp\Value;
use Spatie\Csp\Keyword;

class Extended extends Basic
{
    public function configure()
    {
        parent::configure();
    
        $this->reportOnly();
        //$this->addDirective(Directive::SCRIPT, 'www.google.com');
        $this
            ->addDirective(Directive::STYLE, 'self font.googleapis.com self sha256-hash fonts.gstatic.com sweetalert.js.org gstatic.com')
            ->addDirective(Directive::SCRIPT, 'self font.googleapis.com self sha256-hash fonts.gstatic.com sweetalert.js.org gstatic.com')
            ->addDirective(Directive::DEFAULT, 'style-src fonts.gstatic.com font.googleapis.com')
            ->addNonceForDirective(Directive::SCRIPT)
            ->addNonceForDirective(Directive::STYLE)
            //->addDirective(Directive::STYLE,Keyword::UNSAFE_INLINE)
            ;
    
    }
}