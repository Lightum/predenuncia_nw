<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $table ='personas';
    protected $primaryKey = 'id_personas';
    public $timestamps = true;

    protected $fillable = [
    	'nom_persona',
        'app_persona',
        'apm_persona',
        'sexo',
        'edad',
        'telefono',
        'correo',
        'id_denuncias',
        'id_interviniente',
        'tipo_personas',
        'media_filiacion',
        'activo',
    ];
    //Relación polimorfica
    public function direcciones(){
        return $this->morphMany(Lugar::class,'modelo');
    }

}
