<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    use HasFactory;
    const DEFAULT_ROLE = 0;

    protected $table = 'localizaciones';
    protected $primaryKey = 'id_localizacion';
    public $timestamps = true;
    protected $fillable =['id_estados','id_municipios','id_colonias','localidad','codigo_postal','calle','num_ext','num_int','modelo_id','modelo_type','activo'];

    protected $attributes = [
        'num_int' => self::DEFAULT_ROLE,
    ];

    public function modelo(){
        return $this->morphto();
    }

}
