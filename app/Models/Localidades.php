<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Localidades extends Model
{
    use HasFactory;

    protected $primaryKey= 'id_localidades';
    protected $fillable = ['id_localidades','nombre','activo','id_municipios'];
}
