<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{
    use HasFactory;
    protected $table = 'vehiculos';
    protected $primaryKey = 'id_vehiculos';
    protected $fillable =['id_vehiculos',
                          'id_marca',
                          'id_submarca',
                          'id_tvehiculos',
                          'id_color',
                          'placas',
                          'num_serie',
                          'motor',
                          'id_aseguradora',
                          'id_denuncias',
                          'activo',
                          'created_at',
                          'updated_at'];
}
