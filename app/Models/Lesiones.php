<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesiones extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_denuncias';
    protected $fillable = ['hechos_denuncia', 'nom_representante','app_representante','folio'];
}
