<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $table = 'empresas';
    protected $primarykey = 'id_empresa';
    public $timestamps = true;
    

    protected $fillable = [
        'razon_social','nom_representante','app_representante','apm_representante','id_denuncias','activo'
    ];
    

}
