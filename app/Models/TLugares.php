<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TLugares extends Model
{
    use HasFactory;
    
    protected $table = 't_lugares';
    protected $primaryKey = 'id_tipolugar';
    protected $fillable =['tipo','activo'];
}
