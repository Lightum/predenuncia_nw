<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TVehiculos extends Model
{
    use HasFactory;
    protected $table = 'tipo_vehiculos';
    protected $primaryKey = 'id_tvehiculos';
    protected $fillable =['id_tvehiculos',
                          'tipo_vehiculos',
                          'activo'];
}
