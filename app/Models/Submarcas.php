<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submarcas extends Model
{
    use HasFactory;
    protected $table = 'submarcas';
    protected $primaryKey = 'id_submarca';
    protected $fillable =['id_marca',
                          'cve_submarca',
                          'descripcion',
                          'activo'];
}
