<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Textorsiones extends Model
{
    use HasFactory;
    protected $table = 't_extorsiones';
    protected $primaryKey = 'id_textorsion';
    protected $fillable = ['id_textorsion', 'tipo_extorsion','activo'];
}
