<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tbien extends Model
{
    use HasFactory;
    protected $table = 'tipo_bienes';
    protected $primaryKey = 'id_tbien';
    protected $fillable = ['id_tbien', 'bien','activo'];
}
