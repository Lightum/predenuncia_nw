<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    use HasFactory;
    protected $table ='denuncias';
    protected $primaryKey = 'id_denuncias';
    public $timestamps = true;
    
    protected $fillable = [
    	'denuncia_066', 'procesado', 'folio', 'fecha_denuncia', 'foto_denuncia', 'video_denuncia','id_tdelitos','id_hechos','activo',
    ];        
}
