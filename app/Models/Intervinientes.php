<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Intervinientes extends Model
{
    use HasFactory;

    
    protected $primaryKey = ['id_internivientes'];
    protected $fillable =['tipo','activo'];
}
