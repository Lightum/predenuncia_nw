<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aseguradoras extends Model
{
    use HasFactory;

    protected $table ='aseguradoras';
    protected $primaryKey = 'id_aseguradora';
    public $timestamps = true;

    protected $fillable = [
    	'id_aseguradora', 'aseguradora', 'activo',
    ];
}
