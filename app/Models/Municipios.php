<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
    use HasFactory;

    protected $primaryKey= 'id_municipios';
    protected $fillable = ['id_municipios','municipio','activo','id_estados'];
}
