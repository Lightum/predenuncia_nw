<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Extorsion extends Model
{
    use HasFactory;
    protected $table = 'extorsiones';
    protected $primaryKey = 'id_extorsion';
    protected $fillable = ['id_extorsion', 'nombre_victima','app_victima','apm_victima','tipo_telefono', 'fecha_ext','medio_ext','identificacion',
                           'id_tbienes','bien_entregado','valor_bien','banco_cuenta','no_cuenta','id_textorsion','id_lugarhechos','descripcion_hechos'];
}
