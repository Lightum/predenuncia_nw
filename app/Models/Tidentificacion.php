<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tidentificacion extends Model
{
    use HasFactory;
    protected $table = 'tipo_identificacion';
    protected $primaryKey = 'id_tidentificacion';
    protected $fillable = ['id_tidentificacion', 'tipo_identificacion','activo'];
}
