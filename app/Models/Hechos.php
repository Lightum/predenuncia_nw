<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hechos extends Model
{
    use HasFactory;
    protected $table = 'hechos';
    protected $primaryKey = 'id_hechos';
    protected $fillable =['hechos','hora_hechos','fecha_hechos','violencia','id_tlugares','lugar_hallazgo','robo_objetos','activo'];

    public function direcciones(){
        return $this->morphMany('App\Models\Lugar','modelo');
    }
}
