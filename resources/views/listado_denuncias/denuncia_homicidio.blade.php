@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE HOMICIDIO
@endsection
@section('campos')
    @section('inicio_form')
    <form id="form_advanced_validation" name="form" method="POST" action="{{route('homicidios.store')}}" enctype="multipart/form-data">
        @csrf
    @endsection
    @section('campos_intermedios')
    <div class="secciones_den"><h3>Datos de la víctima</h3></div>
    <div class="separacion_den"></div>
    <fieldset>
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="nombre_victima"
                            id="nombre_victima" placeholder="Nombre" required>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="app_victima"
                            id="app_victima" placeholder="Apellido Paterno" required>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="apm_victima"
                            id="apm_victima" placeholder="Apellido Materno" required>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <p class="card-inside-title">Género</p>
                        <input type="radio" name="genero_victima" id="masculino_victima"
                            class="with-gap radio-col-brown" value="1" required>
                        <label for="masculino_victima">Masculino</label>
                        <input type="radio" name="genero_victima" id="femenino_victima" class="with-gap radio-col-brown"
                            value="2" required>
                        <label for="femenino_victima">Femenino</label>
                        <input type="radio" name="genero_victima" id="noespecificado_victima" class="with-gap radio-col-brown"
                            value="3" required>
                        <label for="noespecificado_victima">LGBTTTIQ+</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="number" class="form-control" name="edad_victima" id="edad_victima" required>
                        <label class="form-label">Edad</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    @endsection
    @section('fin_form')
    </form>
    @endsection
@endsection
