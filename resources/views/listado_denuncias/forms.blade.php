@extends('layouts.app')
@section('content')
<div class="block-header">
    <h2>@yield('titulo')</h2>
</div>
@if ($errors->any())
    <div class="col-md-12">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif


<!-- Advanced Form With Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('layouts.flash-message')
        <div class="card">
            <div class="header">
                <h2>No podrá denunciar a través de esta plataforma si los hechos ocurrieron fuera del Estado de México.</h2>                                  
                <br>
                <h2>CAPTURE SUS DATOS POR FAVOR</h2>
            </div>
            <div class="body">
            @yield('inicio_form')
                <h3>Denuncia Ciudadana</h3>
                <fieldset>
                <br>
                <div class="secciones_den"><h3>Datos del denunciante</h3></div>
                <div class="separacion_den"></div>
                <div class="row clearfix">
                <div class="col-sm-12">
                   <input type="hidden" class="form-control" name="id_interviniente" id="id_interviniente" value="2" hidden="hidden">
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-float">
                            <p class="card-inside-title">Tipo de persona:</p>
                            <input type="radio" name="tipo_persona" id="fisica" class="with-gap radio-col-brown" value="1" checked required {{old('tipo_persona') == '1' ? 'checked' : ''}}>
                            <label for="fisica">Fisica</label>
                            <input type="radio" name="tipo_persona" id="moral" class="with-gap radio-col-brown" value="0" required {{old('tipo_persona') == '0' ? 'checked' : ''}}>
                            <label for="moral">Moral</label>
                        </div>
                        <div class="form-group form-float razon_social_denunciante">
                            <div class="form-line">
                                <input type="text" class="form-control" name="razon_social_denunciante"
                                    id="razon_social_denunciante" value="{{ old('razon_social_denunciante') }}" require>
                                <label class="form-label">Razón Social</label>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                    <div class="form-group form-float nom_representante">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nom_representante" id="nom_representante" value="{{ old('nom_representante') }}" pattern="[a-zA-ZñÑ .]{1,30}" title="El nombre del representante legal debe contener solo letras" require>
                            <label class="form-label">Nombre del Representante:</label>
                        </div>
                    </div>
                    <div class="form-group form-float app_representante">
                        <div class="form-line">
                            <input type="text" class="form-control" id="app_representante" name="app_representante" value="{{ old('app_representante') }}" pattern="[a-zA-ZñÑ ]{1,30}" title="El apellido paterno del representante debe contener solo letras" require>
                            <label class="form-label">Apellido Paterno del Representante:</label>
                        </div>
                    </div>
                    <div class="form-group form-float apm_representante">
                        <div class="form-line">
                            <input type="text" class="form-control" id="apm_representante" name="apm_representante" value="{{ old('apm_representante') }}" pattern="[a-zA-ZñÑ ]{1,30}" title="El apellido materno del representante legal debe contener solo letras" >
                            <label class="form-label">Apellido Materno del representante:</label>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float p_fisica">
                            <div class="form-line">
                                <input type="text" class="form-control" name="nombre_denunciante" id="nombre_denunciante" value="{{ old('nombre_denunciante') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El nombre debe contener solo letras" require>
                                <label class="form-label">Nombre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float p_fisica">
                            <div class="form-line">
                                <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno"  value="{{ old('apellido_paterno') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El apellido paterno debe contener solo letras" require>
                                <label class="form-label">Apellido Paterno</label>
                            </div>
                            <small>Llenar al menos un apellido</small>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float p_fisica">
                            <div class="form-line">
                                <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" value="{{ old('apellido_materno') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El apellido materno debe contener solo letras"  require>
                                <label class="form-label">Apellido Materno</label>
                            </div>
                            <small>Llenar al menos un apellido</small>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <p class="card-inside-title">Género *</p>
                                <input type="radio" name="genero_denunciante" id="masculino"
                                    class="with-gap radio-col-brown" {{old('genero_denunciante') == '1' ? 'checked="checked"' : ''}} value="1" required>
                                <label for="masculino">Masculino</label>
                                <input type="radio" name="genero_denunciante" id="femenino" class="with-gap radio-col-brown"
                                       {{old('genero_denunciante') == '2' ? 'checked="checked"' : ''}} value="2" required>
                                <label for="femenino">Femenino</label>
                                <input type="radio" name="genero_denunciante" id="noespecificado" class="with-gap radio-col-brown"
                                 {{old('genero_denunciante') == '3' ? 'checked="checked"' : ''}} value="3" required>
                                <label for="noespecificado">LGBTTTIQ+</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control" min="0" max="120"  pattern="([0-9]{3})" name="edad_denunciante" id="edad_denunciante" value="{{ old('edad_denunciante') }}" required>
                                <label class="form-label">Edad *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="correo_denunciante"
                                    id="correo_denunciante" value="{{ old('correo_denunciante') }}" required>
                                <label class="form-label">E-mail *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="tel" class="form-control" name="telefono_denunciante"
                                    id="telefono_denunciante" value="{{ old('telefono_denunciante') }}" pattern="[0-9]{10}" title="El número telefónico debe ser de 10 digitos"  maxlength="10" required>
                                <label class="form-label">Teléfono *</label>
                            </div>
                        </div>
                    </div>
                </div>

                    <!-- Direccion del Denunciante-->
                <div class="secciones_den"><h3>Dirección del Denunciante</h3></div>
                <div class="separacion_den"></div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Estado *</label>
                            <div class="form-line">
                                <select class="form-control show-tick estado" name="id_estado" id="id_estado" required>
                                    <option value="">Estado</option>
                                        @forelse($estados as $est)
                                            <option value="{{$est->id_estados}}" {{old('id_estado') == $est->id_estados ? "selected" : ""}}>{{$est->estado}}</option>
                                        @empty
                                            <p>Sin Estados</p>
                                        @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <label class="form-label">Municipio *</label>
                            <div class="form-line">
                                <select class="form-control show-tick municipio" name="municipio_den" id="id_municipio" required>
                                    <option value="">Seleccione</option>
                                </select>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <label class="form-label">Colonia *</label>
                            <div class="form-line">
                                <select class="form-control id_colonia" name="colonia_den" id="colonia_den" required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="calle_denunciante" id="calle_denunciante" value="{{ old('calle_denunciante') }}"  required>
                                <label class="form-label">Calle *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" class="form-control cp_denunciante" name="cp_denunciante" id="cp_denunciante" value="{{ old('cp_denunciante') }}" pattern="[0-9]{5}" title="El código postal debe ser de 5 digitos" required>
                                <label class="form-label">Código Postal *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="num_ext_denunciante" id="num_ext_denunciante" value="{{ old('num_ext_denunciante') }}"  required>
                                <label class="form-label">No. Exterior *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="num_int_denunciante" id="num_int_denunciante" value="{{ old('num_int_denunciante') }}">
                                <label class="form-label">No. Interior</label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Probable Responsable -->
                <div class="secciones_den"><h3>Probable Responsable</h3></div>
                <div class="separacion_den"></div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <p class="card-inside-title">¿Conoces al probable responsable? *</p>
                                <input type="radio" name="tipo_persona_p" id="fisica_p" class="with-gap radio-col-brown"  {{old('tipo_persona_p') == '1' ? 'checked' : '' }} value="1" required>
                                <label for="fisica_p">Si</label>
                                <input type="radio" name="tipo_persona_p" id="moral_p" class="with-gap radio-col-brown" {{old('tipo_persona_p') == '0' ? 'checked' : ''}} value="0" required>
                                <label for="moral_p">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float conoces_probable" >
                                <input type="hidden" class="form-control" name="conoces_probable" id="conoces_probable">
                                <label class="form-label">Quien Resulte Responsable</label>
                            </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float nombre_probable" >
                            <div class="form-line">
                                <input type="text" class="form-control" name="nombre_probable" id="nombre_probable" value="{{ old('nombre_probable') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El nombre del probable responsable debe contener solo letras">
                                <label class="form-label">Nombre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float appat_probable">
                            <div class="form-line">
                                <input type="text" class="form-control" name="appat_probable" id="appat_probable" value="{{ old('appat_probable') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El apellido paterno del probable responsable debe contener solo letras">
                                <label class="form-label">Apellido Paterno</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float apmat_probable">
                            <div class="form-line">
                                <input type="text" class="form-control" name="apmat_probable" id="apmat_probable" value="{{ old('apmat_probable') }}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+{1,30}" title="El apellido materno del probable responsable debe contener solo letras">
                                <label class="form-label">Apellido Materno</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group form-float genero_probable">
                            <div class="form-line">
                                <p class="card-inside-title">Género *</p>
                                <input type="radio" name="genero_probable" id="masculino_probable"
                                    class="with-gap radio-col-brown" value="1" {{old('genero_probable') == '1' ? 'checked="checked"' : ''}} required>
                                <label for="masculino_probable">Masculino</label>
                                <input type="radio" name="genero_probable" id="femenino_probable" class="with-gap radio-col-brown"
                                     value="0" {{old('genero_probable') == '0' ? 'checked="checked"' : ''}} required>
                                <label for="femenino_probable">Femenino</label>
                                <input type="radio" name="genero_probable" id="noespecificado_probable" class="with-gap radio-col-brown"
                                      value="2" {{old('genero_probable') == '2' ? 'checked="checked"' : '' }} required>
                                <label for="noespecificado_probable">LGBTTTIQ+</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float media_filiacion">
                            <div class="form-line">
                                <textarea class="form-control " name="media_filiacion" id="filiacion_probable" style="height: 200px" maxlength="4000">{{ old('media_filiacion') }}</textarea>
                                <label class="form-label">Media Filiacion</label>
                            </div>
                        </div>
                    </div>
                </div>

                @yield('campos_intermedios')
                    <div class="secciones_den"><h3>Información de los hechos</h3></div>
                    <div class="separacion_den"></div>
                    <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <p class="card-inside-title">¿Ya realizó su reporte al 089? *</p>
                                <input type="radio" name="denuncia_066" id="denuncia_066_si" class="with-gap radio-col-brown" {{old('denuncia_066') == '1' ? 'checked' : ''}} value="1" required>
                                <label for="denuncia_066_si">Sí</label>
                                <input type="radio" name="denuncia_066" id="denuncia_066_no" class="with-gap radio-col-brown" {{old('denuncia_066') == '0' ? 'checked' : ''}} value="0" required>
                                <label for="denuncia_066_no">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <label class="form-label">Lugar donde ocurrieron los hechos</label>
                            <div class="form-line">
                                <select class="form-control show-tick" id="id_tipolugar" name="id_tipolugar" required>
                                    <option value="">Lugar donde ocurrieron los hechos *</option>
                                    @foreach($tlugar as $tl)
                                    <option value="{{$tl->id_tlugares}}" {{old('id_tipolugar') == $tl->id_tlugares ? "selected" : ""}}>{{$tl->tipo_lugares}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group form-float">
                            <label class="form-label">Tipo de lugar *</label>
                            <div class="form-line">
                                <select class="form-control show-tick" name="lugar_hallazgo" id="lugar_hallazgo" required>
                                    <option value="">Tipo de Lugar</option>
                                    <option value="1"  {{old('lugar_hallazgo') == 1 ? 'selected' : ''}}>Lugar de Hallazgo</option>
                                    <option value="2"  {{old('lugar_hallazgo') == 2 ? 'selected' : ''}}>Lugar de los Hechos</option>
                                </select>
                            </div>
                        </div>
                    </div>


                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <p class="card-inside-title">¿Ocurrió con violencia? *</p>
                        <input type="radio" name="violencia" id="si" class="with-gap radio-col-brown"
                            {{old('violencia') == '1' ? 'checked' : ''}} value="1" required>
                        <label for="si">Si</label>
                        <input type="radio" name="violencia" id="no" class="with-gap radio-col-brown"
                            {{old('violencia') == '0' ? 'checked' : ''}} value="0" required>
                        <label for="no">No</label>
                    </div>
                </div>
            </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                        <label class="form-label">Municipio *</label>
                        <div class="form-line">
                            <select class="form-control show-tick municipio" name="municipio_hechos" id="id_municipio1" required>
                                <option value="">Municipio</option>
                                @forelse($municipios_hechos as $mun)
                                    <option value="{{$mun->id_municipios}}" {{old('municipio_hechos') == $mun->id_municipios ? "selected" : ""}}>{{$mun->municipio}}</option>
                                @empty
                                    Sin Municipios
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                            <label class="form-label">Colonia *</label>
                            <div class="form-line">
                                <select class="form-control id_colonia1" {{ old('colonia_hechos') }} name="colonia_hechos" id="colonia_hechos" required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                            <label class="form-label">Localidad </label>
                            <div class="form-line">
                                <select class="form-control" name="localidad_hechos" id="localidad_hechos" required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="calle_hechos" id="calle_hechos" value="{{ old('calle_hechos') }}" required>
                                <label class="form-label">Calle *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" class="form-control" name="cp_hechos" id="cp_hechos" value="{{ old('cp_hechos') }}" pattern="[0-9]{5}" title="El codigo postal del lugar de los hechos debe contener 5 digitos" required>
                                <label class="form-label">Código Postal *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="num_ext_hechos" id="num_ext_hechos" value="{{ old('num_ext_hechos') }}" required>
                                <label class="form-label">No. Exterior *</label>
                            </div>
                        </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="num_int_hechos" id="num_int_hechos" value="{{ old('num_int_hechos') }}">
                            <label class="form-label">No. Interior</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group date">
                        <div class="form-line">
                            <label class="form-label">Fecha de los hechos *</label>
                            <input type="date" class="form-control" name="fecha_hechos" id="fecha_hechos" value="{{ old('fecha_hechos') }}"
                                required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group date">
                        <div class="form-line">
                            <label class="form-label">Hora de los hechos *</label>
                            <input type="time" class="form-control" name="hora_hechos" id="hora_hechos" value="{{ old('hora_hechos') }}"
                                required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" name="descripcion" id="descripcion" style="height: 200px" minlength="150" maxlength="4000" required>{{ old('descripcion') }}</textarea>
                            <label class="form-label">Descripción de los hechos *</label>
                            <span id="characters">
                                <div class="right-side-count">4000 </div>
                                <div class="right-side">caracteres restantes</div>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                        <label class="form-label">Foto de los hechos</label>
                        <div class="form-line">
                            <input type="file" value="{{ old('foto') }}" class="form-control" id="foto" name="foto" accept="image/png, image/jpeg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float">
                        <label class="form-label">Video de los hechos</label>
                        <div class="form-line">
                            <input type="file" class="form-control" id="video" name="video" accept="video/mp4,video/mpeg,video/webm,video/avi,video/3gp,video/amv,video/m4v,video/ogg,video/ogv,video/flv,video/mkv">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 align-right">
                    <button class="btn btn-primary btn-lg waves-effect" style="font-size: 18px; border-radius:5px;" id="btn_enviar" type="submit">Enviar</button>
                </div>
                </fieldset>
                @yield('campos_finales')
                @yield('fin_form')
            </div>
        </div>
    </div>
</div>
<!-- #END# Advanced Form Example With Validation -->
</div>
@section('scripts')
<script src="{{asset('js/ComboEstadoMunicipio.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/ComboLocal.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/validateForms.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/valForms.js')}}" nonce="{{ csp_nonce() }}"></script>

<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {
        $('#fisica').on('change', function () {

            if ($(this).is(':checked')) {
                var tpersona = localStorage.setItem("tpersona", 1);
                $(".razon_social_denunciante").hide();
                $(".nom_representante").hide();
                $(".app_representante").hide();
                $(".apm_representante").hide();
                $(".p_fisica").show();
                document.getElementById("moral").checked = false;
            }
        });
        $('#moral').on('change', function () {

            if ($(this).is(':checked')) {
                var tpersona = localStorage.setItem("tpersona", 2);
                $(".razon_social_denunciante").show();
                $(".nom_representante").show();
                $(".app_representante").show();
                $(".apm_representante").show();
                $(".p_fisica").hide();
                document.getElementById("fisica").checked = false;
            }
        });
    });
</script>
<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {
        $(".conoces_probable").hide();
        $('#fisica_p').on('change', function () {


            if ($(this).is(':checked')) {
                var p = localStorage.setItem("probable", 1);
                $(".conoces_probable").hide();
                $(".nombre_probable").show();
                $(".appat_probable").show();
                $(".apmat_probable").show();
                $(".genero_probable").show();
                $(".media_filiacion").show();
            }
        });
        $('#moral_p').on('change', function () {

            if ($(this).is(':checked')) {
                var p = localStorage.setItem("probable", 0);
                $(".conoces_probable").show();
                $(".nombre_probable").hide();
                $(".appat_probable").hide();
                $(".apmat_probable").hide();
                $(".genero_probable").hide();
                $(".media_filiacion").hide();
            }
        });
    });
</script>

<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {


        if ($('#seguro_si').is(':checked')) {
            $("#aseguradora_vehiculo").show();
        }else{
            $("#aseguradora_vehiculo").hide();
        }

        $('#seguro_no').on('change', function () {
            if ($(this).is(':checked')) {
                $("#aseguradora_vehiculo").hide();
            }
        });

        $('#seguro_si').on('change', function () {

            if ($(this).is(':checked')) {
                $("#aseguradora_vehiculo").show();
                $("#aseguradora_vehiculo").val("");

            }
        });
    });
</script>



<script type = "text/javascript">



    function init(){
        if (typeof(Storage) !== 'undefined') {

            let p = localStorage.getItem('probable');
            let tpersona = localStorage.getItem('tpersona');
            let textorsion = localStorage.getItem('textorsion');
            console.log("tpersona: "+tpersona);
            if (p == 1){
                $(".conoces_probable").hide();
                $(".nombre_probable").show();
                $(".appat_probable").show();
                $(".apmat_probable").show();
                $(".genero_probable").show();
                $(".media_filiacion").show();
            }

            if(p == 0){
                $(".conoces_probable").show();
                $(".nombre_probable").hide();
                $(".appat_probable").hide();
                $(".apmat_probable").hide();
                $(".genero_probable").hide();
                $(".media_filiacion").hide();
            }

            if(tpersona == 1 || tpersona == null)
            {
                $(".razon_social_denunciante").hide();
                $(".nom_representante").hide();
                $(".app_representante").hide();
                $(".apm_representante").hide();
                $(".p_fisica").show();
                document.getElementById("fisica").checked = true;
                document.getElementById("moral").checked = false;

            }

            if (tpersona == 2){
                $(".razon_social_denunciante").show();
                $(".nom_representante").show();
                $(".app_representante").show();
                $(".apm_representante").show();
                $(".p_fisica").hide();
                document.getElementById("moral").checked = true;
                document.getElementById("fisica").checked = false;
            }

            if(textorsion == 1 ){
                $(".telefono").hide();
                $(".email").hide();
                $(".redes").hide();
            }
            if (textorsion == 2){
                $(".telefono").show();
                $(".email").hide();
                $(".redes").hide();
            }
            if(textorsion == 3){
                $(".email").show();
                $(".telefono").hide();
                $(".redes").hide();
            }
            if (textorsion == 4 ){
                $(".redes").show();
                $(".email").hide();
                $(".telefono").hide();
            }

        } else {
            console.log("No soporta local storage");
        }
    }

    window.onload = init;

</script>
@endsection
@endsection
