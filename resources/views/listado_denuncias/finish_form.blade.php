@extends('layouts.app')
@section('content')
<div class="block-header">
    <h2>@yield('titulo')</h2>
</div>
@if ($errors->any())
    <div class="col-md-12">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Guarde el siguiente folio
                </h2>
            </div>
            <div class="body">
                <p>Su predenuncia se ha generado correctamente, por favor guarde el siguiente Folio: <div id="folio_static"></div></p>
                <p>- <a href="{{route('pdf_denuncias',json_encode(session('n_folio')))}}" target="_blank">PDF</a> Descargue su comprobante.</p>
            </div>
        </div>
    </div>
</div>
<!-- #END# Advanced Form Example With Validation -->
</div>
@section('scripts')
<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function(){
        var a = {!! json_encode(session('folio')) !!};
        var folio_pw = localStorage.getItem('folio_pw');
        if(a !== null){
            $("#folio_static").text(a);
            console.log(a);
            localStorage.setItem('folio_pw',a);
        }else{
            $("#folio_static").text(folio_pw);
        }

        //Elimina las variables temporales al finalizar el formulario
        localStorage.removeItem("modelo_vehiculo");
        localStorage.removeItem("marca_vehiculo");
        localStorage.removeItem("id_municipio");
        localStorage.removeItem("id_municipio1");
        localStorage.removeItem("colonia_den");
        localStorage.removeItem("colonia_hechos");
        localStorage.removeItem("probable");
        localStorage.removeItem("tpersona");
    });
</script>
@endsection
@endsection
