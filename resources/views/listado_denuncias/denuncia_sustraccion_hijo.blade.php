@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE SUSTRACCIÓN DE HIJO
@endsection

@section('campos')
    @section('inicio_form')
    <form id="form_advanced_validation" action="{{route('sustraccion_hijo.store')}}" method="POST" enctype="multipart/form-data">
        @csrf

    @section('fin_form')
    </form>
    @endsection


@endsection
