@extends('layouts.app')
    @section('content')
<div class="container-fluid">
        <div class="block-header">
            <h2>DENUNCIAS</h2>
        </div>

        {{-- <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/anonimato.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Persona desaparecida</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_pdesaparecida')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/secuestro.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Secuestro</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_secuestro')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/homicidio.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Homicidio</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('homicidios.index')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/soborno.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Trata de personas</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('trata.index')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/atado.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Tortura</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_tortura')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div> --}}

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card1">
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/anonimato.png')}}">
                                    <div class="caption">
                                        <div class="content-area">
                                            <h3>Persona desaparecida</h3>
                                            <p></p>
                                        </div>
                                    </div>
                                    <div class="sep_listado"></div>
                                     <p>
                                        <a href="{{route('pdesaparecida.create')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                               
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/secuestro.png')}}">
                                    <div class="caption">
                                        <h3>Secuestro</h3>
                        <p></p>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('denuncia_secuestro')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/homicidio.png')}}">
                                    <div class="caption">
                                        <h3>Homicidio</h3>
                                        <p></p>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('homicidios.index')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/soborno.png')}}">
                                    <div class="caption">
                                        <h3>Trata de personas</h3>
                                        <p></p>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('trata.index')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card1">
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/atado.png')}}">
                                    <div class="caption">
                                        <div class="content-area">
                                            <h3>Tortura</h3>
                                            <p></p>
                                        </div>
                                    </div>
                                    <div class="sep_listado"></div>
                                     <p>
                                        <a href="{{route('denuncia_tortura')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                </div>
            </div>
        </div>

    </div>
    @stop






