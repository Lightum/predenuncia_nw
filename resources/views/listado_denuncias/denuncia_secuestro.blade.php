@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE SECUESTRO
@endsection
@section('campos')
    @section('inicio_form')
    <form id="form_advanced_validation" method="POST" action="{{route('secuestro.store')}}" enctype="multipart/form-data">
    @csrf
    @endsection
    @section('campos_intermedios')
         <!-- Persona Secuestrada-->
         <fieldset>
         <div class="secciones_den"><h3>Datos Persona Secuestrada</h3></div>
         <div class="separacion_den"></div>
         <div class="row clearfix">
             <div class="col-sm-12">
                 <div class="form-group form-float">
                     <div class="form-line">
                         <input type="text" class="form-control" name="nombre_sec" id="nombre_sec" value="{{ old('nombre_sec') }}" pattern="[a-zA-ZñŃ .]{1,30}" title="El nombre de la persona secuestrada debe contener solo letras" required>
                         <label class="form-label">Nombre</label>
                     </div>
                 </div>
                 <div class="form-group form-float">
                     <div class="form-line">
                         <input type="text" class="form-control" name="appat_per_sec" id="appat_per_sec" value="{{ old('appat_per_sec') }}" pattern="[a-zA-ZñŃ ]{1,30}" title="El apellido paterno de la persona secuestrada debe contener solo letras" required="">
                         <label class="form-label">Apellido Paterno</label>
                     </div>
                 </div>
                 <div class="form-group form-float">
                     <div class="form-line">
                         <input type="text" class="form-control" name="apmat_per_sec" id="apmat_per_sec" value=" {{ old('apmat_per_sec') }}" pattern="[a-zA-ZñŃ ]{1,30}" title="El apellido materno de la persona secuestrada debe contener solo letras" required="">
                         <label class="form-label">Apellido Materno*</label>
                     </div>
                 </div>
                 <div class="form-group form-float">
                    <div class="form-line">
                        <p class="card-inside-title">Género</p>
                        <input type="radio" name="genero_persona" id="persona_masculino"
                            class="with-gap radio-col-brown" {{old('genero_persona') == '1' ? 'checked' : '' }} value="1" >
                        <label for="persona_masculino">Masculino</label>
                        <input type="radio" name="genero_persona" id="persona_femenino" class="with-gap radio-col-brown"
                             {{old('genero_persona') == '0' ? 'checked' : ''}} value="0">
                        <label for="persona_femenino">Femenino</label>
                        <input type="radio" name="genero_persona" id="persona_noespecificado" class="with-gap radio-col-brown"
                             {{old('genero_persona') == '2' ? 'checked' : ''}} value="2">
                        <label for="persona_noespecificado">LGBTTTIQ+</label>
                    </div>
                </div>
                 <div class="form-group form-float">
                     <div class="form-line">
                         <input type="number" class="form-control" min="0" max="120" name="edad_per_sec" id="edad_per_sec" value="{{ old('edad_per_sec') }}"  require>
                         <label class="form-label">Edad.</label>
                     </div>
                 </div>
                  <div class="form-group form-float">
                     <div class="form-line">
                         <input type="tel" class="form-control" name="telefono_victima" id="telefono_victima" value="{{ old('telefono_victima') }}"  pattern="[0-9]{10}" title="El número telefónico debe ser de 10 digitos"  maxlength="10">
                         <label class="form-label">Teléfono</label>
                     </div>
                 </div>

             </div>
         </div>
         </fieldset>
    @endsection
    @section('fin_form')
    </form>
    @endsection
@endsection
