@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE PERSONA DESAPARECIDA
@endsection
@section('campos')
    @section('inicio_form')

    <form id="form_advanced_validation" action="{{route('pdesaparecida.store')}}"  method="POST" name="form"  enctype="multipart/form-data">
    @csrf
    @endsection
    @section('campos_intermedios')
        <!-- Persona Desaparecida-->
        <fieldset>
        <div class="secciones_den"><h3>Datos Persona Desaparecida</h3></div>
        <div class="separacion_den"></div>
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="nombre_desaparecido" id="nombre_desaparecido" value="{{ old('nombre_desaparecido') }}" pattern="[a-zA-ZñÑ .]{1,30}" title="El nombre de la persona desaparecida debe contener solo letras" require>
                        <label class="form-label">Nombre*</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="appat_per_des" id="appat_per_des" value="{{ old('appat_per_des') }}" pattern="[a-zA-ZñÑ ]{1,30}" title="El apellido paterno de la persona desaparecida debe contener solo letras" require>
                        <label class="form-label">Apellido Paterno*</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="apmat_per_des" id="apmat_per_des" value="{{ old('apmat_per_des') }}" pattern="[a-zA-ZñÑ ]{1,30}" title="El apellido materno de la persona desaparecida debe contener solo letras" require>
                        <label class="form-label">Apellido Materno*</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <p class="card-inside-title">Género</p>
                        <input type="radio" name="genero_persona" id="persona_masculino"
                            class="with-gap radio-col-brown" value="1" {{ old('gereno_persona') == '1' ? 'checked' : '' }}>
                        <label for="persona_masculino">Masculino</label>
                        <input type="radio" name="genero_persona" id="persona_femenino" class="with-gap radio-col-brown"
                            value="0" {{ old('genero_persona') == '0' ? 'checked' : '' }} >
                        <label for="persona_femenino">Femenino</label>
                        <input type="radio" name="genero_persona" id="persona_noespecificado" class="with-gap radio-col-brown"
                            value="2"  {{ old('genero_persona') == '2' ? 'checked' : '' }}>
                        <label for="persona_noespecificado">LGBTTTIQ+</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="edad_per_des" id="edad_per_des" min="0" max="120"  pattern="([0-9]{3})" value="{{ old('edad_per_des') }}" required>
                        <label class="form-label">Edad*</label>
                    </div>
                </div>
            </div>
        </div>
        </fieldset>
    @endsection
    @section('fin_form')
    </form>
    @endsection
@endsection
