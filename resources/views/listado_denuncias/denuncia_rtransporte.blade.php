@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE ROBO A TRANSPORTE PÚBLICO
@endsection
@section('campos')
    @section('inicio_form')
    <form id="form_advanced_validation" method="POST" action="{{route('rtransporte.store')}}" enctype="multipart/form-data">
    @csrf
    @endsection
    @section('fin_form')
    </form>
    @endsection
@endsection
