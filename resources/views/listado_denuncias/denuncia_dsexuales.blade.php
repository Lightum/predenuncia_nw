@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE DELITOS SEXUALES
@endsection

@section('campos')
    @section('inicio_form')
    <form id="form_advanced_validation" action="{{route('delitos_sexuales.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @endsection
    @section('fin_form')
    </form>
    @endsection

@endsection
