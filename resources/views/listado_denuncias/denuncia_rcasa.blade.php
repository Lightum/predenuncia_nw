@extends('listado_denuncias.forms')

@section('titulo')
DENUNCIA DE ROBO A CASA HABITACIÓN
@endsection

@section('campos')
    @section('inicio_form')
        <form id="form_advanced_validation" method="POST" action="{{route('guarda_rcasa')}}" enctype="multipart/form-data">
            @csrf
    @endsection
        @section('campos_intermedios')
            <div class="secciones_den"><h3>Robo de Objetos</h3></div>
            <div class="separacion_den"></div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <br>
                    <br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control " name="robo_objetos" id="robo_objetos" style="height: 200px" maxlength="4000" required>{{ old('robo_objetos') }}</textarea>
                            <label class="form-label">Robo de Objetos</label>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        @endsection
    @section('fin_form')
    </form>
    @endsection
@endsection
