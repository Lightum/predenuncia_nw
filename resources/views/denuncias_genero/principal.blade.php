@extends('layouts.app')
    @section('content')
    {{-- <div class="col-xs-10 col-sm-2 col-md-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="{{asset('images/agresion-sexual.png')}}" alt="AdminBSB - Profile Image">
                </div>
                <div class="content-area">
                    <h3>Delitos sexuales</h3>
                    <p></p>
                </div>
            </div>
            <div class="profile-footer">
                <a href="{{route('denuncia_dsexuales')}}" style"text-decoration: none"><button
                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
            </div>
        </div>
    </div>
    <div class="col-xs-10 col-sm-2 col-md-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="{{asset('images/lesionado.png')}}" alt="AdminBSB - Profile Image">
                </div>
                <div class="content-area">
                    <h3>Lesiones</h3>
                    <p></p>
                </div>
            </div>
            <div class="profile-footer">
                <a href="{{route('lesiones.create')}}" style"text-decoration: none"><button
                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
            </div>
        </div>
    </div> --}}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card1">
                <div class="body">
                    <div class="row">                                   
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/agresion-sexual.png')}}">
                                <div class="caption">
                                    <div class="content-area">
                                        <h3>Delitos sexuales</h3>
                                    </div>
                                </div>
                                <div class="sep_listado"></div>
                                 <p>
                                    <a href="{{route('denuncia_dsexuales')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                           
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/lesionado.png')}}">
                                <div class="caption">
                                    <h3>Lesiones</h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('lesiones.create')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/alimento.jpg')}}">
                                <div class="caption">
                                    <h3>Incum.de Obligaciones Alimentarias </h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('incum_alimen')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/violenciafamiliar.png')}}">
                                <div class="caption">
                                    <h3>Violencia Familiar</h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('violencia_familiar')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card1">
                <div class="body">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/hijos.png')}}">
                                <div class="caption">
                                    <h3>Sustraccion de Hijo</h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('sustraccion_hijo')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/lesiongenero.jpg')}}">
                                <div class="caption">
                                    <h3>Lesiones en Razón de Genero</h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('lesion_genero')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="{{asset('images/documentos.png')}}">
                                <div class="caption">
                                    <h3>Otros</h3>
                                </div>
                                <div class="sep_listado"></div>
                                <p>
                                    <a href="{{route('lista_denuncias')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </p>
                            </div>
                        </div>
    
            </div>
        </div>
    </div>

</div>
    @endsection
