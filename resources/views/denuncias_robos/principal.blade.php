@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>ROBOS</h2>
        </div>

        {{-- <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/casa.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Robo a casa habitación</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_rcasa')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/flagrancia.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>En flagrancia</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('flagrancia.create')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/carterista.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Robo con violencia</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_rviolencia')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-2 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/autobu.png')}}" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Transporte público</h3>
                        <p></p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('denuncia_rtransporte')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div> --}}

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card1">
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/casa.png')}}">
                                    <div class="caption">
                                        <div class="content-area">
                                            <h3>Casa habitación</h3>
                                        </div>
                                    </div>
                                    <div class="sep_listado"></div>
                                     <p>
                                        <a href="{{route('denuncia_rcasa')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                </div>
                               
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/flagrancia.png')}}">
                                    <div class="caption">
                                        <h3>En flagrancia</h3>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('flagrancia.create')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/carterista.png')}}">
                                    <div class="caption">
                                        <h3>Con violencia</h3>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('denuncia_rviolencia')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/autobu.png')}}">
                                    <div class="caption">
                                        <h3>Transporte público</h3>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('denuncia_rtransporte')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection