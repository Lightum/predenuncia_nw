@extends('listado_denuncias.forms')
@section('titulo')
    DENUNCIA DE EXTORSION PERSONAL
@endsection
@section('campos')
    @section('inicio_form')
        <form id="form_advanced_validation" name="form" method="POST" action="{{route('guarda_extorsion')}}" enctype="multipart/form-data">
            @csrf
            @section('campos_intermedios')
            <fieldset>
                <div class="secciones_den"><h3>Detalle Extorsión</h3></div>
                <div class="separacion_den"></div>
                <div class="row clearfix">
                    <div class="col-sm-3" hidden>
                        <div class="demo-switch-title">Presencial</div>
                            <div class="switch">
                                <label><input type="checkbox" value="1" name="id_textorsion" id="presencial" placeholder="Selecciona tipo de extorsion" title="Selecciona tipo de extorsion"  required><span class="lever switch-col-brown" ></span></label>
                            </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div id="mensaje_switches"></div>
                            <input type="hidden" value="1" name="id_interviniente" id="id_interviniente">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <select class="form-control show-tick" id="id_tidentificacion" name="id_tidentificacion" value="{{old('id_tidentificacion')}}" required>
                                <option value="" selected>Tipo de identificacion</option>
                                @foreach($identificacion as $ti)
                                    <option value="{{$ti->id_tidentificacion}}" {{old('id_tidentificacion') == $ti->id_tidentificacion ? 'selected' : ''}} >{{$ti->tipo_identificacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" id="identificacion" name="identificacion"  value="{{old('identificacion')}}" required>
                            <label class="form-label">Identificación:</label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-2">
                        <div class="form-group form-float">
                                <p class="card-inside-title">¿Es usted la victima?:</p>
                                <input type="radio" name="victima" id="SI" class="with-gap radio-col-brown" value="1"
                                {{old('victima') == '1' ? 'checked="checked"' : ''}} required>
                                <label for="SI">Si</label>
                                <input type="radio" name="victima" id="NO" class="with-gap radio-col-brown" value="0"
                                {{old('victima') == '0' ? 'checked="checked"' : ''}}  required>
                                <label for="NO">No</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group form-float">
                        <p class="card-inside-title">Tipo de bien entregado:</p>
                        <select class="form-control show-tick" id="id_tbien" name="id_tbien" required="required">
                                <option value="" selected>Tipo de bien</option>
                                    @foreach($bienes as $b)

                                        <option value="{{$b->id_tbien}}" {{old('id_tbien') == $b->id_tbien ? 'selected' : ''}}>{{$b->bien}}</option>
                                    @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="bien_entregado" id="bien_entregado" value="{{old('bien_entregado')}}">
                                <label class="form-label">Bien Entregado:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="valor_bien" id="valor_bien" value="{{old('valor_bien')}}">
                                <label class="form-label">Valor Aproximado del bien:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="banco_cuenta" id="banco_cuenta" value="{{old('banco_cuenta')}}">
                                <label class="form-label">Banco de la cuenta:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="no_cuenta" id="no_cuenta" value="{{old('no_cuenta')}}">
                                <label class="form-label">Número de cuenta:</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-3">
                        <div class="form-group form-float">
                            <p class="card-inside-title">¿Robo de Objetos?:</p>
                            <input type="radio" name="robo_objetos" id="Si" class="with-gap radio-col-brown" value="1"
                            {{old('robo_objetos') == '1' ? 'checked="checked"' : ''}} required>
                            <label for="Si">Si</label>
                            <input type="radio" name="robo_objetos" id="No" class="with-gap radio-col-brown" value="0"
                            {{old('robo_objetos') == '0' ? 'checked="checked"' : ''}} required>
                            <label for="No">No</label>
                        </div>
                    </div>
                </div>
            </fieldset>
            @endsection
    @section('fin_form')
        </form>
    @endsection

@section('scripts')
<script src="{{asset('js/ComboEstadoMunicipio.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/ComboLocal.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/validateForms.js')}}" nonce="{{ csp_nonce() }}"></script>
<script src="{{asset('js/valForms.js')}}" nonce="{{ csp_nonce() }}"></script>

<script type="text/javascript" nonce="{{ csp_nonce() }}">

    $(document).ready(function () {
        //PRESENCIAL
        var presencial = localStorage.getItem('presencial');
        if(presencial==1){
            $(".telefono").hide();
            $(".email").hide();
            $(".redes").hide();
            document.getElementById("presencial").checked = true;
            document.getElementById("tel").checked = false;
            document.getElementById("url").checked = false;
            document.getElementById("correo").checked = false;
        }
        $('#presencial').on('change', function () {
            if ($(this).is(':checked')) {
                localStorage.setItem('presencial',1);
                localStorage.setItem('tel',0);
                localStorage.setItem('url',0);
                localStorage.setItem('correo',0);
                $(".telefono").hide();
                $(".email").hide();
                $(".redes").hide();
                document.getElementById("tel").checked = false;
                document.getElementById("url").checked = false;
                document.getElementById("correo").checked = false;

            }

        });

    });

    </script>

<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {
        // $(".razon_social_denunciante").hide();
        // $(".nom_representante").hide();
        // $(".app_representante").hide();
        // $(".apm_representante").hide();
        // $(".p_fisica").show();

        $('#fisica').on('change', function () {
            var tpersona = localStorage.setItem("tpersona", 1);
            if ($(this).is(':checked')) {
                $(".razon_social_denunciante").hide();
                $(".nom_representante").hide();
                $(".app_representante").hide();
                $(".apm_representante").hide();
                $(".p_fisica").show();
                document.getElementById("moral").checked = false;
            }
        });
        $('#moral').on('change', function () {
            var tpersona = localStorage.setItem("tpersona", 2);
            if ($(this).is(':checked')) {
                $(".razon_social_denunciante").show();
                $(".nom_representante").show();
                $(".app_representante").show();
                $(".apm_representante").show();
                $(".p_fisica").hide();
                document.getElementById("fisica").checked = false;
            }
        });
    });
</script>
<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {
        $(".conoces_probable").hide();
        $('#fisica_p').on('change', function () {


            if ($(this).is(':checked')) {
                var p = localStorage.setItem("probable", 1);
                $(".conoces_probable").hide();
                $(".nombre_probable").show();
                $(".appat_probable").show();
                $(".apmat_probable").show();
                $(".genero_probable").show();
                $(".media_filiacion").show();
            }
        });
        $('#moral_p').on('change', function () {

            if ($(this).is(':checked')) {
                var p = localStorage.setItem("probable", 0);
                $(".conoces_probable").show();
                $(".nombre_probable").hide();
                $(".appat_probable").hide();
                $(".apmat_probable").hide();
                $(".genero_probable").hide();
                $(".media_filiacion").hide();
            }
        });
    });
</script>

<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function () {
        $("#aseguradora_vehiculo").show();

        $('#seguro_no').on('change', function () {
            if ($(this).is(':checked')) {

                $("#aseguradora_vehiculo").val(77);
                $("#aseguradora_vehiculo").hide();

            }
        });

        $('#seguro_si').on('change', function () {

            if ($(this).is(':checked')) {
                $("#aseguradora_vehiculo").show();
                $("#aseguradora_vehiculo").val("");

            }
        });
    });
</script>



<script type = "text/javascript">



    function init(){
        if (typeof(Storage) !== 'undefined') {

            let p = localStorage.getItem('probable');
            let tpersona = localStorage.getItem('tpersona');
            console.log("tpersona: "+tpersona);
            if (p == 1){
                $(".conoces_probable").hide();
                $(".nombre_probable").show();
                $(".appat_probable").show();
                $(".apmat_probable").show();
                $(".genero_probable").show();
                $(".media_filiacion").show();
            }

            if(p == 0){
                $(".conoces_probable").show();
                $(".nombre_probable").hide();
                $(".appat_probable").hide();
                $(".apmat_probable").hide();
                $(".genero_probable").hide();
                $(".media_filiacion").hide();
            }

            if(tpersona == 1 || tpersona == null)
            {
                $(".razon_social_denunciante").hide();
                $(".nom_representante").hide();
                $(".app_representante").hide();
                $(".apm_representante").hide();
                $(".p_fisica").show();
                document.getElementById("fisica").checked = true;
                document.getElementById("moral").checked = false;

            }

            if (tpersona == 2){
                $(".razon_social_denunciante").show();
                $(".nom_representante").show();
                $(".app_representante").show();
                $(".apm_representante").show();
                $(".p_fisica").hide();
                document.getElementById("moral").checked = true;
                document.getElementById("fisica").checked = false;
            }

        } else {
            console.log("No soporta local storage");
        }
    }

    window.onload = init;

</script>
<script nonce="{{ csp_nonce() }}">
    $('#btn_enviar').on('click', function () {
        console.log("click en el boton");
        var bandera = 0;
        var presencial_chk = document.getElementById('presencial').checked;
        var tel_chk = document.getElementById('tel').checked;
        var correo_chk = document.getElementById('correo').checked;
        var url_chk = document.getElementById('url').checked;

        if(presencial_chk){
            bandera = 1
        }
        if(tel_chk){
            bandera = 1
        }
        if(correo_chk){
            bandera = 1
        }
        if(url_chk){
            bandera = 1
        }

        if(bandera==0){
                document.getElementById("mensaje_switches").innerHTML = '<div class="alert alert-warning" role="alert">Por favor, seleccione el tipo de extorsión</div>';
        }else{
            document.getElementById("mensaje_switches").innerHTML = '';
        }


    });
</script>

@endsection
