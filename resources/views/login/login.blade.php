@extends('layouts.app')

@section('content')

<div class="row clearfix">
    <body class="login-page">
        <div class="login-box">
            <div class="card">
                <div class="body">
                    <form action="{{route('validar')}}" method="POST">
                        @csrf
                        <div class="msg">Inicio de session</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name="usuario" placeholder="usuario" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required autofocus>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">INICIO DE SESIÓN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</div>
@if ($errors->any())
<div class="col-md-12">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

@stop
