@extends('listado_denuncias.forms')
@section('titulo')
DENUNCIA DE ROBO DE VEHÍCULOS
@endsection
@section('campos')
    @section('inicio_form')
        <form id="form_advanced_validation" name="form" method="POST" action="{{route('vehiculos.store')}}" enctype="multipart/form-data">
            @csrf
    @endsection
    @section('campos_intermedios')
        <div class="secciones_den"><h3>Datos del Vehículo</h3></div>
        <div class="separacion_den"></div>
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p class="card-inside-title">¿Cuenta con seguro contra robo? *</p>
                        <input type="radio" name="seguro" id="seguro_si" class="with-gap radio-col-brown"  {{old('seguro') == '1' ? 'checked' : ''}} value="1" required>
                        <label for="seguro_si">Sí</label>
                        <input type="radio" name="seguro" id="seguro_no" class="with-gap radio-col-brown"  {{old('seguro') == '0' ? 'checked' : ''}} value="0" required>
                        <label for="seguro_no">No</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Marca Vehículo *</label>
                    <div class="form-line">
                        <select class="form-control show-tick" name="marca_vehiculo_denunciante" id="marca_vehiculo_denunciante" required>
                            <option value="">Seleccione</option>
                            @forelse($marcas_vehiculo as $marca)
                            <option value="{{$marca->id_marca}}" {{old('marca_vehiculo_denunciante') == $marca->id_marca ? "selected" : ""}}>{{$marca->marca}}</option>
                            @empty
                                Sin marcas de vehiculos
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Modelo Vehículo *</label>
                    <div class="form-line">
                        <select class="form-control show-tick" {{ old('modelo_vehiculo_denunciante') }} name="modelo_vehiculo_denunciante" id="modelo_vehiculo_denunciante" required></select>
                        <input type="hidden" name="modelo_vehiculo_nombre" id="modelo_vehiculo_nombre" value="{{ old('modelo_vehiculo_nombre') }}">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Tipo Vehículo *</label>
                    <div class="form-line">
                        <select class="form-control show-tick" name="tipo_vehiculo_denunciante" id="tipo_vehiculo_denunciante" required>
                            <option value="">Seleccione</option>
                            @forelse($t_vehiculos as $t_vehiculo)
                            <option value="{{$t_vehiculo->id_tvehiculos}}" {{old('tipo_vehiculo_denunciante') == $t_vehiculo->id_tvehiculos ? "selected" : ""}}>{{$t_vehiculo->tipo_vehiculos}}</option>
                            @empty
                                Sin tipos de vehículo
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Color Vehículo *</label>
                    <div class="form-line">
                        <select class="form-control show-tick" name="color_vehiculo_denunciante" id="color_vehiculo_denunciante" required>
                            <option value="">Seleccione</option>
                            @forelse($color_vehiculo as $color)
                            <option value="{{$color->id_color}}" {{old('color_vehiculo_denunciante') == $color->id_color ? "selected" : ""}}>{{$color->color}}</option>
                            @empty
                                Sin tipos de vehículo
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Aseguradora</label>
                    <div class="form-line">
                        <select class="form-control show-tick" name="aseguradora_vehiculo" id="aseguradora_vehiculo" required>
                            <option value="">Seleccione</option>
                            @forelse($aseguradoras as $aseguradora)
                            <option value="{{$aseguradora->id_aseguradora}}" {{old('aseguradora_vehiculo') == $aseguradora->id_aseguradora ? "selected" : ""}}>{{$aseguradora->aseguradora}}</option>
                            @empty
                                Sin aseguradoras en DB
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-float">
                    <label class="form-label">Tipo de placa *</label>
                    <div class="form-line">
                        <select class="form-control show-tick" name="tipo_placa" id="tipo_placa">
                            <option value="">Seleccione</option>
                            <option value="0" {{ old('tipo_placa') == 0 ? 'selected' : ''}} >Servicio Particular</option>
                            <option value="1" {{ old('tipo_placa') == 1 ? 'selected' : ''}} >Servicio Público Estatal</option>
                            <option value="2" {{ old('tipo_placa') == 2 ? 'selected' : ''}} >Servicio Público Federal</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="placas_vehiculo_denunciante" id="placas_vehiculo_denunciante" value="{{ old('placas_vehiculo_denunciante') }}" required>
                        <label class="form-label">Placas *</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="numero_serie" id="numero_serie" value="{{ old('numero_serie') }}" required>
                        <label class="form-label">Número de serie *</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="num_motor" id="num_motor" value="{{ old('num_motor') }}" required>
                        <label class="form-label">No. Motor *</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="num_serie_vehiculo_denunciante" id="num_serie_vehiculo_denunciante" value="{{ old('num_serie_vehiculo_denunciante') }}" required>
                        <label class="form-label">No. Serie Vehículo *</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
        </div>
            <div class="secciones_den"><h3>Robo de Objetos</h3></div>
            <div class="separacion_den"></div>
            <br>
            <br>
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea class="form-control " name="robo_objetos" id="robo_objetos" style="height: 200px" maxlength="4000" required>{{ old('robo_objetos') }}</textarea>
                    <label class="form-label">Robo de Objetos</label>
                </div>
            </div>
            <br>
    @endsection
    @section('fin_form')
        </form>

<script type="text/javascript" nonce="{{ csp_nonce() }}">
    $(document).ready(function(){
            // var modelo_vehiculo = $("#modelo_vehiculo_denunciante option:selected").text();
            // console.log(modelo_vehiculo);
            // if(modelo_vehiculo !== null){
            //     $("#modelo_vehiculo_nombre").val(modelo_vehiculo);
            // }
            $("#modelo_vehiculo_denunciante").change(function() {
                var modelo_vehiculo = $("#modelo_vehiculo_denunciante option:selected").text();
                $("#modelo_vehiculo_nombre").val(modelo_vehiculo);
            });
        });
    </script>
<!--Combos para codigo postal-->
<script src="{{asset('js/valForms.js')}}" nonce="{{ csp_nonce() }}"></script>
<!--Combos para vehiculos -->
<script type="text/javascript" nonce="{{ csp_nonce() }}">
    let url = "{{route('combo_submarca')}}";
  </script>
<script src="{{asset('js/combos_vehiculos.js')}}" nonce="{{ csp_nonce() }}"></script>

<!--Fin combos para vehiculos -->
    @endsection
@endsection
