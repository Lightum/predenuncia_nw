@extends('layouts.app')
    @section('content')
@section('titulo')
DENUNCIA DE ROBO DE VEHÍCULOS
@endsection
<div class="card table-responsive">
<div class="container">
    <div class="row">
        <div class="col-md-12">
           <br>
           <br>
           <table id="denuncias" class="table table-striped table-bordered">
               <thead>
                   <tr>
                       <th>Tipo</th>
                       <th>Titulo</th>
                       <th>Descripción</th>
                       <th width="70px">Fecha</th>
                       <th width="150px">Acciones</th>
                   </tr>
               </thead>
           </table>
        </div>
    </div>
</div>
</div>
@section('scripts')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js" defer></script>
<script>
$(document).ready(function() {
    $('#denuncias').DataTable({
        "serverSide": true,
        "ajax": "{{ url('json_denuncias') }}",
        "columns": [
            {data: 'id_denuncias'},
            {data: 'folio'},
            {data: 'fecha_denuncia'},
            {data: 'id_tdelitos'},
            {data: 'btn'}
        ],
        "aoColumnDefs": [ {
        "aTargets": [ 0 ],
        "mData": "download_link",
        // "mRender": function ( data, type, full ) {
        //     if(data === 0){
        //         return 'Comunicado';
        //     }else{
        //         return 'Entrevista';
        //     }
        // }
        } ],
        "language": {
            "info": "_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "lengthMenu": 'Mostrar <select >'+
                        '<option value="10">10</option>'+
                        '<option value="30">30</option>'+
                        '<option value="-1">Todos</option>'+
                        '</select> registros',
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "emptyTable": "No hay datos",
            "zeroRecords": "No hay coincidencias",
            "infoEmpty": "",
            "infoFiltered": ""
        }
    });
});
</script>
@endsection
@endsection
