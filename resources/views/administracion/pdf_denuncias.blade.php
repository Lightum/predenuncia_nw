<style>
    .encabezado_img{
        margin-bottom: 30px;
    }

   .img_right{
        margin-left: 90%;
        margin-top:-115px;
    }

    .img_left{
        margin-right: 70%;
    }

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        font-family: sans-serif;
        padding: 3px;
        width: auto;
    }

    h3{
        text-align: center;
        font-family: sans-serif;
    }
    p{
        font-family: sans-serif;
    }
    .tabla_right{
        width: auto;
        border: #000;
        margin-right: 20%;
        margin-left: auto;
        height: auto;
        clear: both;
        font-size: 12;
    }

    .tabla_left{
        width: 550px;
        margin-right: auto;
        margin-left: 0%;
        height: auto;
        clear: both;
        font-size: 12;
    }

    .tabla_center{
        width: 550px;
        margin-right: auto;
        margin-left: auto;
        height: auto;
        clear: both;
        font-size: 12;
    }

    .tabla_heads {
      border-width: 1px;
      border: #000;
      padding-left: 3px;
      padding-right: 3px;
      text-align: left;
      width: auto;
    }

    .tabla_info {
      border: #000;
      padding-left: 20px;
      padding-right: 20px;
    }

    .firma {
      text-align: justify;
      margin-right: 30%;
      font-size: 9px;
    }

    .firmante {
      text-align: center;
      margin-right: 30%;
    }
    .qr_image{
        width: 120;
        margin-right: auto;
        margin-left: 75%;
        height: auto;
    }

    .tsp{
        width: 120;
        margin-right: auto;
        margin-left: 550px;
        height: auto;
        font-size: 8px;
        margin-top: -228px;
        float: right;
    }
</style>

<html>
    <body>
        <div class="encabezado_img">
            <div class="img_left">EDOMEX</div>
            <div class="img_right">FGJEM</div>
        {{-- <img class="img_left" src="{{asset('images/edomex.png')}}" width="150px">
        <img class="img_right" src="{{asset('images/logo_fgjem.png')}}" width="75px"> --}}
        </div>
        <p style="float:right"><b>Folio: {{$denuncia->folio}}</b></p>
        <br>
        <br>
        <br>
        <h3>Datos del denunciante</h3>
    <table class="tabla_center">
        @if (isset($empresa[0]->razon_social))
        <tr>
                <th class="tabla_heads">Razón Social</th>
                <td>{{($empresa[0]->razon_social)}}</td>
        </tr>
        @endif
        <tr>
            @if (isset($empresa[0]->nom_representante))
                <th class="tabla_heads">Nombre del Representante</th>
                <td>{{($empresa[0]->nom_representante)}}</td>
            @else
                <th class="tabla_heads">Nombre del Denunciante</th>
                <td>{{($denunciante[0]->nom_persona)}}</td>
            @endif
        </tr>
        <tr>
            @if (isset($empresa[0]->app_representante))
                <th class="tabla_heads">Primer Apellido Representante</th>
                <td>{{($empresa[0]->app_representante)}}</td>
            @else
                <th class="tabla_heads">Primer Apellido Denunciante</th>
                <td>{{($denunciante[0]->app_persona)}}</td>
            @endif
        </tr>
        <tr>
            @if (isset($empresa[0]->apm_representante))
                <th class="tabla_heads">Segundo Apellido representante</th>
                <td>{{($empresa[0]->apm_representante)}}</td>
            @else
                <th class="tabla_heads">Segundo Apellido Denunciante</th>
                <td>{{($denunciante[0]->apm_persona)}}</td>
            @endif
        </tr>
        <tr>
            <th class="tabla_heads">Sexo</th>
            @if ($denunciante[0]->sexo == 1)
                <td>Masculino</td>
            @elseif ($denunciante[0]->sexo == 2)
                <td>Femenino</td>
            @else
                <td>No Especificado</td>
            @endif
        </tr>
        <tr>
            <th class="tabla_heads">Edad</th>
            <td>{{($denunciante[0]->edad)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Estado</th>
            <td>
                @php
                    $query = App\Models\Estados::where(['id_estados' => $lugar_persona[0]->id_estados])->first();
                    echo $query->estado;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Municipio</th>
            <td>
                @php
                    $query = App\Models\Municipios::where(['id_municipios' => $lugar_persona[0]->id_municipios])->first();
                    echo $query->municipio;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Colonia</th>
            <td>
                @php
                    $query = App\Models\Colonias::where(['id_colonias' => $lugar_persona[0]->id_colonias])->first();
                    echo $query->colonia;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Calle</th>
            <td>{{($lugar_persona[0]->calle)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número Exterior</th>
            <td>{{($lugar_persona[0]->num_ext)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número Interior</th>
            <td>{{($lugar_persona[0]->num_int)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Teléfono</th>
            <td>{{($denunciante[0]->telefono)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Correo electrónico</th>
            <td>{{($denunciante[0]->correo)}}</td>
        </tr>
    </table>
    <br>
    <br>
    <h3>Hechos Denuncia</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Descripción de los hechos</th>
        </tr>
        <tr>
            <td>{{($hechos[0]->hechos)}}</td>
        </tr>
    </table>
    <br>
    <br>
    <h3>Lugar de los hechos</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Con violencia</th>
            @if ($hechos[0]->violencia == 0)
                <td>NO</td>
            @else
                <td>SÍ</td>
            @endif
        </tr>
        <tr>
            <th class="tabla_heads">Hora</th>
            <td>{{($hechos[0]->hora_hechos)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Fecha</th>
            <td>{{date('d-m-Y', strtotime($hechos[0]->fecha_hechos))}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Tipo de lugar</th>
            <td>
                @if($hechos[0]->lugar_hallazgo==1)
                    Lugar de Hallazgo
                @else
                    Lugar de los Hechos
                @endif
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Lugar donde ocurrieron los hechos</th>
            <td>
            @php
                $query = App\Models\TLugares::where(['id_tlugares' => $hechos[0]->id_tlugares])->first();
                echo $query->tipo_lugares;
            @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Estado</th>
            <td>
            @php
                $query = App\Models\Estados::where(['id_estados' => $lugar[0]->id_estados])->first();
                echo $query->estado;
            @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Código Postal</th>
            <td>{{($lugar[0]->codigo_postal)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Municipio</th>
            <td>
                @php
                $query = App\Models\Municipios::where(['id_municipios' => $lugar[0]->id_municipios])->first();
                echo $query->municipio;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Colonia</th>
            <td>
                @php
                $query = App\Models\Colonias::where(['id_colonias' => $lugar[0]->id_colonias])->first();
                echo $query->colonia;
                @endphp
            </td>
        </tr>
        @if(isset($localidad->nombre))
        <tr>
            <th class="tabla_heads">Localidad</th>
            <td>{{($localidad->nombre)}}</td>
        </tr>
        @endif
        <tr>
            <th class="tabla_heads">Calle</th>
            <td>{{($lugar[0]->calle)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número Exterior</th>
            <td>{{($lugar[0]->num_ext)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número Interior</th>
            <td>{{($lugar[0]->num_int)}}</td>
        </tr>
    </table>
    <br>
    <br>
    <h3>Probables Responsables</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Descripción</th>
            <td>
                @if($presponsable[0]->nom_persona=="QRR" && $presponsable[0]->app_persona="QRR" && $presponsable[0]->apm_persona=="QRR")
                    QRR
                @else
                    {{($presponsable[0]->nom_persona)}} {{($presponsable[0]->app_persona)}} {{($presponsable[0]->apm_persona)}}
                @endif
            </td>
        </tr>
    </table>
    <br>
    <br>
    @if (isset($presponsable[0]->media_filiacion) && $presponsable[0]->media_filiacion!="")
    <h3>Media Filiación del(os) Probable(s) Responsable(s)</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Descripción</th>
        </tr>
        <tr>
            <td>{{$presponsable[0]->media_filiacion}}</td>
        </tr>
    </table>
    @endif
    <br>
    <br>
    <br>
    @if (isset($vehiculos[0]->id_tvehiculos))
    <h3>Capturar solo en caso de Robo de Vehiculo</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Tipo Vehículo</th>
            <td>
                @php
                $query = App\Models\TVehiculos::where(['id_tvehiculos' => $vehiculos[0]->id_tvehiculos])->first();
                echo $query->tipo_vehiculos;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Marca</th>
            <td>
                @php
                $query = App\Models\Marcas::where(['id_marca' => $vehiculos[0]->id_marca])->first();
                echo $query->marca;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Modelo</th>
            <td>
                @php
                $query = App\Models\Submarcas::where(['id_submarca' => $vehiculos[0]->id_submarca])->first();
                echo $query->submarca;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Color</th>
            <td>
                @php
                $query = App\Models\Colores::where(['id_color' => $vehiculos[0]->id_color])->first();
                echo $query->color;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Placas</th>
            <td>{{($vehiculos[0]->placas)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número de Serie</th>
            <td>{{($vehiculos[0]->num_serie)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Aseguradora</th>
            <td>
                @php
                $query = App\Models\Aseguradoras::where(['id_aseguradora' => $vehiculos[0]->id_aseguradora])->first();
                echo $query->aseguradora;
                @endphp
            </td>
        </tr>
    </table>
    @endif

    {{-- {{dd($extorsiones)}} --}}
    @if (isset($extorsiones[0]->id_extorsiones))
    <h3>Detalle Extorsión</h3>
    <table class="tabla_center">
        <tr>
            <th class="tabla_heads">Tipo de Extorsión</th>
            <td>
                @php
                $query = App\Models\Textorsiones::where(['id_textorsion' => $extorsiones[0]->id_textorsion])->first();
                echo $query->tipo_extorsion;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Tipo Identificación</th>
            <td>
                @php
                $query = App\Models\Tidentificacion::where(['id_tidentificacion' => $extorsiones[0]->id_tidentificacion])->first();
                echo $query->tipo_identificacion;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Identificación</th>
            <td>{{$extorsiones[0]->identificacion}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">¿Es usted la víctima?</th>
            @if ($extorsiones[0]->victima == 0)
                <td>No</td>
            @else
                <td>Sí</td>
            @endif
        </tr>
        <tr>
            <th class="tabla_heads">Tipo de bien entregado</th>
            <td>
                @php
                $query = App\Models\Tbien::where(['id_tbien' => $extorsiones[0]->id_tbien])->first();
                echo $query->bien;
                @endphp
            </td>
        </tr>
        <tr>
            <th class="tabla_heads">Bien Entregado</th>
            <td>{{($extorsiones[0]->bien_entregado)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Valor del Bien</th>
            <td>{{($extorsiones[0]->valor_bien)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Banco de la Cuenta</th>
            <td>{{($extorsiones[0]->banco_cuenta)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Número de cuenta</th>
            <td>{{($extorsiones[0]->no_cuenta)}}</td>
        </tr>
        <tr>
            <th class="tabla_heads">Medio de Extorsión</th>
            <td>{{($extorsiones[0]->medio_extorsion)}}</td>
        </tr>
    </table>
    @endif

    </body>
</html>
