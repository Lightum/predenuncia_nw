@extends('layouts.app')
    @section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>DENUNCIAS</h2>
        </div>
        {{-- <!-- =============== CARDS DE DENUNCIAS ============ -->

        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/phone.png')}}" class="img-fluid" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                       <div class="content-area">
                        <h3>Extorsiones</h3>
                        <p>Denuncias por Extorsión</p>
                    </div>
                    </div>
                </div>
                <div class="profile-footer">

                    <a href="{{route('denuncias_extorsion')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/flagrancia.png')}}" class="img-fluid" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Robos</h3>
                        <p>Denuncias por Robo</p>
                    </div>
                </div>
                <div class="profile-footer">

                    <a href="{{route('robo')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/genero.png')}}" class="img-fluid" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Género</h3>
                        <p>Denuncias por Violencia de género</p>
                    </div>
                </div>
                <div class="profile-footer">

                    <a href="{{route('genero')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-sm-3 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="demo-google-material-icon">
                        <div class="image-area">
                            <img src="{{asset('images/recoleccion.png')}}" class="img-fluid" alt="AdminBSB - Profile Image">
                        </div>
                    </div>
                    <div class="content-area">
                        <h3>Robo de Vehículos</h3>
                        <p>Denuncias por Robo de Vehículo</p>
                    </div>
                </div>
                <div class="profile-footer">
                    <a href="{{route('vehiculos.index')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        <img src="{{asset('images/documentos.png')}}" class="img-fluid" alt="AdminBSB - Profile Image">
                    </div>
                    <div class="content-area">
                        <h3>Otros Delitos</h3>
                        <p>Listado de denuncias</p>
                    </div>
                </div>
                <div class="profile-footer">

                    <a href="{{route('lista_denuncias')}}" style"text-decoration: none"><button
                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </div>
            </div>
        </div>

        <!-- =============== CARDS DE DENUNCIAS ============ --> --}}
        <div class="row clearfix">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card1">
           <div class="body">
            <div class="row">
             <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
                <img src="{{asset('images/recoleccion.png')}}">
                <div class="caption">
                    <h3>Robo de Vehículos</h3>
                    <p id="texto">Denuncias Robo de Vehículo</p>
                </div>
                <div class="sep_listado"></div>
                <p>
                    <a href="{{route('vehiculos.index')}}" style"text-decoration: none"><button
                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                </p>
            </div>
        </div>

           <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
              <img src="{{asset('images/genero.jpg')}}">
               <div class="caption">
                   <h3>Género</h3>
                    <p>Denuncias Violencia de género</p>
                </div>
                <div class="sep_listado"></div>
                  <p>
                    <a href="{{route('genero')}}" style"text-decoration: none"><button
                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                             </p>
                         </div>
                     </div>

                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/phone.png')}}">
                                    <div class="caption">
                                        <div class="content-area">
                                            <h3>Extorsiones</h3>
                                            <p>Denuncias por Extorsión</p>
                                        </div>
                                    </div>
                                    <div class="sep_listado"></div>
                                     <p>
                                    <a href="{{route('denuncias_extorsion')}}" style"text-decoration: none"><button
                                        class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                               
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/flagrancia.png')}}">
                                    <div class="caption">
                                        <h3>Robos</h3>
                                        <p>Denuncias por Robo</p>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('robo')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!------separador-->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="{{asset('images/documentos.png')}}">
                                    <div class="caption">
                                        <h3>Otros Delitos</h3>
                                        <p>Listado de denuncias</p>
                                    </div>
                                    <div class="sep_listado"></div>
                                    <p>
                                        <a href="{{route('lista_denuncias')}}" style"text-decoration: none"><button
                                            class="btn btn-primary btn-lg waves-effect btn-block">SELECCIONAR</button></a>
                                    </p>
                                </div>
                            </div>                         

                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
@section('scripts')
<script nonce="{{ csp_nonce() }}" type="text/javascript">
    window.addEventListener("resize", cambiarTexto);

    function cambiarTexto() {    
        var width = document.documentElement.clientWidth;
        
        if (width < 800){
            document.getElementById("texto").innerHTML = "Robo de Vehículo";
        }else{
            document.getElementById("texto").innerHTML = "Denuncias Robo de Vehículo";
        }
    }
</script>
@endsection
