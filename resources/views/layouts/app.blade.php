<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>FGJEM | Pre-denuncia Web</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('images/logo_fgjem.png')}}" type="image/x-icon">
    <script src="{{asset('jquery/jquery-3.3.1.min.js')}}" nonce="{{ csp_nonce() }}"></script>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" nonce="{{ csp_nonce() }}" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet" nonce="{{ csp_nonce() }}" >
    <!-- Bootstrap Core Css -->
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" nonce="{{ csp_nonce() }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" nonce="{{ csp_nonce() }}"  />

    <!-- Animation Css -->
    <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" nonce="{{ csp_nonce() }}"  />

    <!-- Light Gallery Plugin Css -->
    <link href="{{asset('plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet" nonce="{{ csp_nonce() }}" >

    <!-- Custom Css -->
    <link href="{{asset('css/style.css')}}" nonce="{{ csp_nonce() }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet" nonce="{{ csp_nonce() }}"  />
    <!-- Bootstrap Select Css -->
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" nonce="{{ csp_nonce() }}" rel="stylesheet" />

    <!-- Dropzone Css-->
    <link href="{{asset('dropzone/dropzone.css')}}" rel="stylesheet" nonce="{{ csp_nonce() }}" >
    
    <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet"  nonce="{{ csp_nonce() }}"/>

    <meta http-equiv="Content-Security-Policy-Report-Only: policy ">

    <STYLE nonce="{{ csp_nonce() }}">
        A {
            text-decoration: none;
        }

    </STYLE>


</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Por favor espere</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->

    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{route('principal')}}">Pre-denuncia Web</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{asset('images/logo_fgjem.png')}}" width="135" height="130" alt="User" />
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENU DE NAVEGACIÓN</li>
                    <li class="active">
                        <a href="{{route('principal')}}">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span>Extorsiones</span>
                        </a>
                        <ul class="ml-menu">
                        <li>
                            <a href="{{route('denuncias_extorsion')}}">Presencial</a>
                        </li>
                        <li>
                            <a href="{{route('telefonica')}}">Telefónica</a>
                        </li>
                        <li>
                            <a href="{{route('correo')}}">Correo</a>
                        </li>
                        <li>
                            <a href="{{route('extorsion_redes_sociales')}}">Redes Sociales</a>
                        </li>
                    </ul>
                    </li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>Robos</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{route('vehiculos.index')}}">Robo de vehiculos</a>
                        </li>
                        <li>
                            <a href="{{route('denuncia_rcasa')}}">Robo a casa hab.</a>
                        </li>
                        <li>
                            <a href="{{route('flagrancia.create')}}">Robo en flagrancia</a>
                        </li>
                        <li>
                            <a href="{{route('denuncia_rviolencia')}}">Robo con violencia</a>
                        </li>
                        <li>
                            <a href="{{route('denuncia_rtransporte')}}">Transporte público</a>
                        </li>
                    </ul>

                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>Género</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{route('denuncia_dsexuales')}}">Delitos Sexuales</a>
                        </li>
                        <li>
                            <a href="{{route('incum_alimen')}}">Incumplimiento de Obligaciones Alimentarias</a>
                        </li>
                        <li>
                            <a href="{{route('violencia_familiar')}}">Violencia Familiar</a>
                        </li>
                        <li>
                            <a href="{{route('sustraccion_hijo')}}">Sustracción de Hijo</a>
                        </li>
                        <li>
                            <a href="{{route('lesion_genero')}}">Lesiones en Razón de Genero </a>
                        </li>
                        <li>
                            <a href="{{route('lista_denuncias')}}">Otros</a>
                        </li>
                    </ul>

                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>Denuncias</span>
                    </a>
                    <ul class ="ml-menu">
                        <li>
                        <a href="{{route('denuncia_pdesaparecida')}}">Persona desaparecida</a>
                        </li>
                        <li>
                            <a href="{{route('denuncia_secuestro')}}">Secuestro</a>
                        </li>
                        <li>
                            <a href="{{route('homicidios.index')}}">Homicidio</a>
                        </li>
                        <li>
                            <a href="{{route('denuncia_tortura')}}">Tortura</a>
                        </li>
                        <li>
                            <a href="{{route('trata.index')}}">Trata de personas</a>
                        </li>
                    </ul>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->

            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->

        <!-- #END# Right Sidebar -->
        <!-- CONTENIDO :D -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <!--@include('/layouts/flash-message')-->
            @yield('content')
            @yield('scripts')
            @yield('script')
        </div>
    </section>
    </div>

    <!-- Jquery Core Js -->
    <script  src="{{asset('plugins/jquery/jquery.min.js')}}" nonce="{{ csp_nonce() }}"></script>

    <!-- Bootstrap Core Js -->
    <script nonce="{{ csp_nonce() }}" src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->


    <!-- Slimscroll Plugin Js -->
    <script nonce="{{ csp_nonce() }}" src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script  src="{{asset('plugins/jquery-validation/jquery.validate.js')}}" nonce="{{ csp_nonce() }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script nonce="{{ csp_nonce() }}" src="{{asset('plugins/jquery-steps/jquery.steps.js')}}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script   src="{{asset('plugins/sweetalert/sweetalert.min.js')}}" nonce="{{ csp_nonce() }}"></script>
    @include('sweetalert::alert')
    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('plugins/node-waves/waves.js')}}" nonce="{{ csp_nonce() }}" ></script>

    <!-- Custom Js -->
    <script src="{{asset('js/admin.js')}}" nonce="{{ csp_nonce() }}" ></script>
    <script nonce="{{ csp_nonce() }}" src="{{asset('js/pages/forms/form-validation.js')}}"></script>

    <!-- Demo Js -->
    {{-- <script src="{{asset('js/demo.js')}}" nonce="{{ csp_nonce() }}" ></script> --}}

    <script nonce="{{ csp_nonce() }}" src="{{asset('js/pages/forms/advanced-form-elements.js')}}"></script>

    <!-- Dropzone Js-->
    <script src="{{asset('dropzone/dropzone.js')}}" nonce="{{ csp_nonce() }}" ></script>
</body>

</html>